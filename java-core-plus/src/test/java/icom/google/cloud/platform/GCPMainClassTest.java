package icom.google.cloud.platform;

import icom.google.cloud.platform.api.DefaultJob;
import org.junit.jupiter.api.Test;

class GCPMainClassTest {
    DefaultJob job = new DefaultJob();

    @Test
    void exampleTableCreatingRequest() {
        GCPMainClass.exampleTableCreatingRequest("locations.some_not_existing_table_path");
    }

    @Test
    void exampleGetRequest() {
        exampleTableCreatingRequest();
        job.getDataFromTable(
                "select " +
                        "* " +
                        "from " +
                        "locations.some_not_existing_table_path");
        exampleDeleteTableFromRemote();
    }

    @Test
    void exampleDeleteTableFromRemote() {
        job.getDataFromTable(
                "drop " +
                        "table " +
                        "locations.some_not_existing_table_path");
    }
}