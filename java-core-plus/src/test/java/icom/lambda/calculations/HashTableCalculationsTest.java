package icom.lambda.calculations;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class HashTableCalculationsTest {
    LambdaCalculator testFunction = null;
    HashTableCalculations calculations = null;

    @BeforeEach
    void setUp() {
        try {
            calculations = mock(HashTableCalculations.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addOperation() {
        HashTableCalculations tempHashTable = new HashTableCalculations();
        tempHashTable.addOperation("==", (a, b)-> a++ + b++);
        HashTableCalculations mockClass = mock(tempHashTable.getClass());
        LambdaCalculator returnedMock = tempHashTable.getOperationBySymbol("==");
        when(calculations.getOperationBySymbol("==")).thenReturn(returnedMock);
        testFunction = calculations.getOperationBySymbol("==");
        Assertions.assertEquals(returnedMock, testFunction);
    }

    @Test
    void getOperationBySymbol() {
        LambdaCalculator returnedMock = new HashTableCalculations().getOperationBySymbol("+");

        when(calculations.getOperationBySymbol("+")).thenReturn(returnedMock);
        LambdaCalculator tested = calculations.getOperationBySymbol("+");
        Assertions.assertEquals(returnedMock, tested);

        when(calculations.getOperationBySymbol("-")).thenReturn(returnedMock);
        Assertions.assertEquals(returnedMock, tested);

        when(calculations.getOperationBySymbol("*")).thenReturn(returnedMock);
        Assertions.assertEquals(returnedMock, tested);

        when(calculations.getOperationBySymbol("/")).thenReturn(returnedMock);
        Assertions.assertEquals(returnedMock, tested);
    }
}