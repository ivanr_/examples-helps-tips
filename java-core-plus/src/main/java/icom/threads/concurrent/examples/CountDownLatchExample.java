package icom.threads.concurrent.examples;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchExample {
    static CountDownLatch countDownLatch = new CountDownLatch(4);

    public static void main(String[] args) {
        CDLThread t1 = new CDLThread("CDL Thread1");
        t1.setDaemon(true);
        t1.start();
        CDLThread t2 = new CDLThread("CDL Thread2");
        t2.setDaemon(true);
        t2.start();
        CDLThread t3 = new CDLThread("CDL Thread3");
        t3.setDaemon(true);
        t3.start();

        try {
            countDownLatch.await();
            System.out.println("trying to await in main thread");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("countdown is 0");
    }
}

class CDLThread extends Thread implements Runnable {
    private String threadName;

    public CDLThread(String threadName) {
        this.threadName = threadName;
    }

    @Override
    public void run() {
        int countIterations = (int) CountDownLatchExample.countDownLatch.getCount();
        for (int count = 0; count < countIterations; count++) {
            System.out.printf("Thread %s processing... | has %d steps to finish\n", threadName, (countIterations-count));
            CountDownLatchExample.countDownLatch.countDown();
            System.out.printf("CountDownLatch (thread %s ) is %d\n", threadName, CountDownLatchExample.countDownLatch.getCount());
        }
    }
}
