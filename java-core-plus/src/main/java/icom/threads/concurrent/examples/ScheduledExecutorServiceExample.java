package icom.threads.concurrent.examples;

import java.util.concurrent.*;

public class ScheduledExecutorServiceExample {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("time WITH hack: " + (System.currentTimeMillis() - System.currentTimeMillis() / 100_000 * 100_000) + " ms");
        CountDownLatch lock = new CountDownLatch(4);

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(2);
        ScheduledFuture<?> future = executor.scheduleAtFixedRate(() -> {
            lock.countDown();
            System.out.println("Hello World from " + Thread.currentThread().getName() + ", lock count: " + lock.getCount());
            System.out.println("time WITH hack: " + (System.currentTimeMillis() - System.currentTimeMillis() / 100_000 * 100_000) + " ms");
        }, 500, 100, TimeUnit.MILLISECONDS);

        lock.await();
        System.out.println("time WITH hack: " + (System.currentTimeMillis() - System.currentTimeMillis() / 100_000 * 100_000) + " ms");
        System.out.println("main thread with lock count: " + lock.getCount());
        future.cancel(true);
        executor.shutdown();
    }
}
