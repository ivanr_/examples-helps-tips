package icom.threads.concurrent.examples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class ForksExample {
    public static void main(String[] args) {
        MassiveIntHolder mas = new MassiveIntHolder(null);
        System.out.println("Sum from all elements: " + new ForkJoinPool().invoke(new CountMassiveSumToEveryListElem(mas)));

        // system out of all elements from int arrays
        for (Iterator it = mas.getMasInt().iterator(); it.hasNext(); ) {
            int[] tempForOut = (int[]) it.next();
//            Arrays.stream(tempForOut).peek(value -> System.out.println(value)).toArray();
        }
        System.out.println("breakpoint");
    }
}

class MassiveIntHolder {
    private List<int[]> masInt;
    int[] a;

    public MassiveIntHolder(List<int[]> masInt) {
        if (masInt == null) {
            initHolder();
        }
        else {
            this.masInt = masInt;
        }
    }

    private void initHolder() {
        int arraylistCapasity = 6;
        masInt = new ArrayList<>(arraylistCapasity);
        for (int count = 0; count < arraylistCapasity; count++) {
            masInt.add(getA());
        }
    }

    private int[] getA() {
        return Arrays.stream(new int[5]).map(operand -> (int) Math.round(Math.random() * 10)).toArray();
    }

    public List<int[]> getMasInt() {
        return masInt;
    }
}

class CountMassiveSumToEveryListElem extends RecursiveTask {
    private final MassiveIntHolder massiveIntHolder;
    private int[] nextMassive = null;
    private int result = 0;

    CountMassiveSumToEveryListElem(MassiveIntHolder massiveIntHolder) {
        this.massiveIntHolder = massiveIntHolder;
    }

    private CountMassiveSumToEveryListElem(int[] nextMassive) {
        massiveIntHolder = null;
        this.nextMassive = nextMassive;
        count();
    }

    @Override
    protected Object compute() {
        List<CountMassiveSumToEveryListElem> subTaskForCount = new ArrayList<>();
        int resultFromTasks = 0;
        if (massiveIntHolder != null) {
            int countHolders = 0;
            while (countHolders < massiveIntHolder.getMasInt().size()) {
                CountMassiveSumToEveryListElem task = new CountMassiveSumToEveryListElem(massiveIntHolder.getMasInt().get(countHolders));
                task.fork();
                subTaskForCount.add(task);
                countHolders++;
            }
            for (CountMassiveSumToEveryListElem task : subTaskForCount) {
                try {
                    resultFromTasks += (int) task.join();

                } catch (ClassCastException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("Sum of current massive is " + result);
            return result;
        }
        return resultFromTasks;
    }

    private void count() {
        int result = 0;
        if (massiveIntHolder == null & nextMassive != null) {
            result = Arrays.stream(nextMassive).sum();
        } else {
            if (massiveIntHolder == null & nextMassive == null) throw new IllegalArgumentException();
        }
        this.result = result;
    }
}