package icom.threads.concurrent.examples;

public class WaiterNotifier {
    public static void main(String[] args) throws InterruptedException {
        DataContainer dataContainer = new DataContainer("My text", 3000, 2000);
        Waiter waiter = new Waiter(dataContainer);
        Notifier notifier = new Notifier(dataContainer);
        new Thread(waiter, "waiter").start();
        new Thread(notifier, "notifier").start();
        Thread.currentThread().join(5000);
        System.out.println(dataContainer.getData());
    }
}

class DataContainer {
    private String data = "default description message";
    int waitTimeDelay = 1500;
    private int notifyTimeDelay = 2500;

    public DataContainer() {
    }

    public DataContainer(String data) {
        this.data = data;
    }

    public DataContainer(String data, int waitTimeDelay, int notifyTimeDelay) {
        this.data = data;
        this.waitTimeDelay = waitTimeDelay;
        this.notifyTimeDelay = notifyTimeDelay;
    }

    public String appendData(String appendMessage) {
        String result = new String( new StringBuilder().append(data).append(" ").append(appendMessage));
        data = result;
        return result;
    }

    public String getData() {
        return data;
    }
}

class Waiter implements Runnable {
    DataContainer dataContainer;

    public Waiter(DataContainer dataContainer) {
        this.dataContainer = dataContainer;
    }

    @Override
    public void run() {
        System.out.println(this.getClass().getName() + " started to work at " + System.currentTimeMillis());
        synchronized (dataContainer) {
            dataContainer.appendData("some text from " + this.getClass().getName() + " class");
            try {
                dataContainer.wait(dataContainer.waitTimeDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(this.getClass().getName() + " finished work at " + System.currentTimeMillis());
    }
}

class Notifier implements Runnable {
    DataContainer dataContainer;

    public Notifier(DataContainer dataContainer) {
        this.dataContainer = dataContainer;
    }

    @Override
    public void run() {
        System.out.println(this.getClass().getName() + " started to work at " + System.currentTimeMillis());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (dataContainer) {
            dataContainer.appendData("some text from " + this.getClass().getName() + " class");
            try {
                dataContainer.wait(dataContainer.waitTimeDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(this.getClass().getName() + " finished work at " + System.currentTimeMillis());
    }
}