package icom.threads.concurrent.examples;

import java.util.concurrent.Semaphore;

public class SemaphoreExample {
    static Semaphore semaphore = new Semaphore(3);
    static int commonAvailableCredits = 0;

    public static void main(String[] args) {
        Thread thread1 = new SemaphoreThread("Thread1");
        thread1.start();
        Thread thread2 = new SemaphoreThread("Thread2");
        thread2.start();
        Thread thread3 = new SemaphoreThread("Thread3");
        thread3.start();
        Thread thread4 = new SemaphoreThread("Thread4");
        thread4.start();
        Thread thread5 = new SemaphoreThread("Thread5");
        thread5.start();
    }
}

class SemaphoreThread extends Thread{
    public SemaphoreThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        try {
            System.out.println(getName() + " trying to get permittion, avaliable permits: " + SemaphoreExample.semaphore.availablePermits());
            SemaphoreExample.semaphore.acquire();
            System.out.println(getName() + " got acquire");
            for (int count = 0; count < 5; count++) {
                System.out.println("Credits changed by " + getName() + " to " + ++SemaphoreExample.commonAvailableCredits);
                System.out.println("Avaliable permittions " + SemaphoreExample.semaphore.availablePermits());
                Thread.sleep(1000);
            }
            System.out.println(getName() + " releasing lock");
            SemaphoreExample.semaphore.release();
            System.out.println("Avaliable permittions after release: " + SemaphoreExample.semaphore.availablePermits());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}