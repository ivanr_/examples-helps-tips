package icom.threads.concurrent.examples;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierExample {
    static CyclicBarrier cyclicBarrier = new CyclicBarrier(2, () -> System.out.println("This is the meeting of all wanted participators."));

    public static void main(String[] args) throws InterruptedException {
        CyclBarThread t1 = new CyclBarThread("First participator");
        CyclBarThread t2 = new CyclBarThread("Second participator");
        CyclBarThread t3 = new CyclBarThread("Third participator");
        t1.setDaemon(true);
        t2.setDaemon(true);
        t3.setDaemon(true);
        t1.start();
        t2.start();
        t3.start();
        int waitCounter = 0;
        while (!cyclicBarrier.isBroken() & waitCounter != 10) {
            System.out.println(
                    "Main thread is waiting for braking ("
                            + cyclicBarrier.getNumberWaiting()
                            + " parties is awaiting)..."
                            + (10 - waitCounter));
            Thread.sleep(1000);
            waitCounter++;
        }
        System.out.println("Main thread done");
    }
}

class CyclBarThread extends Thread {

    CyclBarThread(String name) {
        super(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " processing...");
        try {
            CyclicBarrierExample.cyclicBarrier.await();
            if (CyclicBarrierExample.cyclicBarrier.isBroken()) System.out.println("Barrier is broken");
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
        System.out.println(getName() + " is join to party. ");
        System.out.println("Waiting number is " + CyclicBarrierExample.cyclicBarrier.getNumberWaiting());

    }
}