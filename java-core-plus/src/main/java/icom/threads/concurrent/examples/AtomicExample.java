package icom.threads.concurrent.examples;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicExample {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(10);
        Thread incrementInt = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("AtomicInteger before increment: " + atomicInteger);
                atomicInteger.getAndIncrement();
                System.out.println("AtomicInteger after getAndIncrement: " + atomicInteger);
                atomicInteger.incrementAndGet();
                System.out.println("AtomicInteger after incrementAndGet: " + atomicInteger);
            }
        });
        int addToAtomic = 10;
        Thread addInt = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("AtomicInteger before adding: " + atomicInteger);
                atomicInteger.addAndGet(addToAtomic);
                System.out.println("AtomicInteger after addAndGet: " + atomicInteger);
                atomicInteger.getAndAdd(addToAtomic);
                System.out.println("AtomicInteger after getAndAdd: " + atomicInteger);
            }
        });

        incrementInt.start();
        addInt.start();
    }
}
