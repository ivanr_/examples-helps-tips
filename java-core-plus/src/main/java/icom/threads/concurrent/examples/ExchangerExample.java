package icom.threads.concurrent.examples;

import java.util.concurrent.Exchanger;

public class ExchangerExample {
    public static void main(String[] args) {
        Exchanger exchanger = new Exchanger();
        ExchangerThread t1 = new ExchangerThread("Thread 1", "some thread ONE data", exchanger);
        ExchangerThread t2 = new ExchangerThread("Thread 2", "some thread TWO data", exchanger);
        t1.start();
        t2.start();
    }
}

class ExchangerThread extends Thread {
    private String threadData;
    private Exchanger<String> exchanger;

    public ExchangerThread(String name, String threadData, Exchanger exchanger) {
        super(name);
        this.threadData = threadData;
        this.exchanger = exchanger;
    }

    @Override
    public void run() {
        System.out.println(getName() + " processing...");
        try {
            threadData = exchanger.exchange(threadData);
            System.out.println(getName() + ":" + threadData);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
