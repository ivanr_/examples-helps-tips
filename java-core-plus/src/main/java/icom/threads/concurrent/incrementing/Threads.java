package icom.threads.concurrent.incrementing;

public class Threads {
    public static volatile int counter = 0;

    public static void main(String[] args) throws NoSuchMethodException {
        runThreadFromAnotherClass();
        runThreadThrowMethod();
        runAffableThread();

        Thread myCounter = new Thread(() -> {
            while (counter < 50) {
                System.out.println("Counter is " + ++counter + " incremented by OUTSIDE thread");
            }
            System.out.println("Outside counter thread done");
        });
        myCounter.start();
        while (counter < 50) {
            System.out.println("Counter is " + ++counter + " incremented by MAIN thread");
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("main thread done");
    }

    private static void runThreadFromAnotherClass() {
        ChildRunnable childRunnable = new ChildRunnable();
        Thread myThread = new Thread(childRunnable);
        myThread.start();
    }

    private static void runThreadThrowMethod() {
        Thread myThread = new Thread(() -> System.out.println("Thread running with thrown method"));
        myThread.start();
    }

    private static void runAffableThread() {
        AffableThread affableThread = new AffableThread();
        affableThread.start();
    }
}

class ChildRunnable implements Runnable {

    @Override
    public void run() {
        System.out.println("Second thread from another class running");
    }
}

class AffableThread extends Thread {
    public void run() {
        System.out.println("HA-HA it's NOT affable thread!!!!");
    }
}