package icom.threads.concurrent.checkstate;

public class Concurrent {

    public static void main(String[] args) throws InterruptedException {
        //check NEW state
        System.out.println("first thread state: " + ThreadHolder.firstThr.getState());
        ThreadHolder.firstThr.start();
        //checking RUNNABLE state
        Thread.sleep(2000);
        // check TIMED_WAITING state
        System.out.println("first thread state: " + ThreadHolder.firstThr.getState());
        LockDataHolder.instance.setLockedThread(true);
        //checking TERMINATED state
        Thread.sleep(2000);
        System.out.println("first thread state: " + ThreadHolder.firstThr.getState());
        // check impossibility of restarting thread - waiting for Illegal Thread State Exception
        try {
            ThreadHolder.firstThr.start();
        } catch (Exception e) {
            System.out.println("Catched exception: " + e.fillInStackTrace());
        }
//        block state checking
        ThreadHolder.secondThr.start();
        InterClassData.incrementCounter();
        System.out.println("second thread state is " + ThreadHolder.secondThr.getState() + ", time:" + System.currentTimeMillis());
//        checking for blocking for non static method --------> NOT GOT
        InterClassData counterHolder = new InterClassData();
        Thread thirdThread = new Thread(() -> {
            Thread.currentThread().setName("ThirdThread");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                InterClassData.incrementCounter();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thirdThread.start();
        counterHolder.incrementNotStaticCounter();
        System.out.println("Third thread state: " + thirdThread.getState());
//        checking for variable blocking inside of the method
        Thread fourthThread = new Thread(() -> {
            Thread.currentThread().setName("FourthThread");
            try {
                Thread.sleep(1000);
                counterHolder.incrementCounterSyncStep();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " thread DONE");
        });
        fourthThread.start();
        counterHolder.incrementCounterSyncStep();
        System.out.println("Main thread DONE");
    }
}

class LockDataHolder {
    private boolean isLockedThread = false;
    public static final LockDataHolder instance = new LockDataHolder();

    private LockDataHolder() {
    }

    public boolean getLockedThread() {
        return isLockedThread;
    }

    public void setLockedThread(boolean lockedThread) {
        this.isLockedThread = lockedThread;
        if (isLockedThread) System.out.println("Locker is OFF");
        if (!isLockedThread) System.out.println("Locker is ON");
    }
}

class InterClassData {
    private static volatile Integer counter = 0;
    private volatile Integer counterNotStatic = 0;

    public static synchronized void incrementCounter() throws InterruptedException {
        counter++;
        System.out.println("Incremented by thread: " + Thread.currentThread().getName() + ". Value: " + counter + ", time:" + System.currentTimeMillis());
        Thread.sleep(5000);
    }

    public synchronized void incrementNotStaticCounter() throws InterruptedException {
        this.counterNotStatic++;
        System.out.println("Not static method, incremented by thread: " + Thread.currentThread().getName() + ". Value: " + counterNotStatic + ", time:" + System.currentTimeMillis());
        Thread.sleep(5000);
    }

    public void incrementCounterSyncStep() throws InterruptedException {
        int whileCounter = 0;
        while (whileCounter<10) {
            System.out.println("Thread name/state: " + Thread.currentThread().getName() + "/" + Thread.currentThread().getState());
            Thread.sleep(500);
            whileCounter++;
        }
        synchronized (counterNotStatic) {
            Thread.sleep(5000);
            counterNotStatic++;
            System.out.println("Modified by thread/state: " + Thread.currentThread().getName() + "/" + Thread.currentThread().getState());
            Thread.sleep(5000);
        }
        System.out.println("Thread name/state: " + Thread.currentThread().getName() + "/" + Thread.currentThread().getState());
        Thread.sleep(500);
    }
}

class ThreadHolder {
    public static final ThreadHolder instance = new ThreadHolder();

    public static Thread firstThr = new Thread(new Runnable() {
        @Override
        public void run() {
            System.out.println("first thread is running now");
            String thisThreadName = Thread.currentThread().getName();
            while (!LockDataHolder.instance.getLockedThread()) {
                System.out.println("first thread is still running, state: " + Thread.currentThread().getState());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }
            }
            System.out.println("first thread stopped");
        }
    });

    public static Thread secondThr = new Thread(new Runnable() {
        @Override
        public void run() {
            Thread.currentThread().setName("SecondThread");
            System.out.println("second thread is running now");
            try {
                Thread.sleep(1000);
                InterClassData.incrementCounter();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    });

    private ThreadHolder() {
    }
}
