package icom.stream.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamWithManySources {
    private static List<User> guestUsers = new ArrayList<>();
    private static List<User> residentUsers = new ArrayList<>();

    public static void main(String[] args) {
        guestUsers.add(new User("andrew", +91_985__632_56_23L));
        guestUsers.add(new User("peter", +81_298_563_25_62L));
        guestUsers.add(new User("maria", +21_544_512_02_85L));
        residentUsers.add(new User("marta", +31_522_531_22_55L));
        residentUsers.add(new User("nikita", +51_789_789_01_82L));

        System.out.println("Ascending hash code key order of all users: ");
        List<Integer> res = Stream
                .of(guestUsers, residentUsers)
                .flatMap(List::stream)
                .map(User::getId)
                .sorted(Comparator.comparingInt(Integer::intValue))
                .peek(System.out::println)
                .collect(Collectors.toList());

        String searchedLetter = "a";
        System.out.printf("User names with %s letter:\n", searchedLetter);
        long countUsersWithLetter =
                Stream
                        .of(residentUsers, guestUsers)
                        .flatMap(Collection::stream)
                        .map(User::getName)
                        .filter(s -> s.contains(searchedLetter))
                        .peek(System.out::println)
                        .count();
        List<User> usersWithLetter = Stream
                .of(residentUsers, guestUsers)
                .flatMap(Collection::stream)
                .filter(user -> user.getName().contains(searchedLetter))
                .sorted(Comparator.comparing(User::getName))
                .collect(Collectors.toList());
        System.out.println("breakpoint");
    }
}

class User {
    private final String name;
    private final int idIsHashFromPhone;

    public User(String name, Long idByPhoneNumberStartWithPlus) {
//        check for valid name and phone number
        this.idIsHashFromPhone = idByPhoneNumberStartWithPlus.hashCode();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return idIsHashFromPhone;
    }
}