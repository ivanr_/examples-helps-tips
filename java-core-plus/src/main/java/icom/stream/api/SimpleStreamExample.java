package icom.stream.api;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class SimpleStreamExample {
    public static void main(String[] args) {
        List<String> cities = new ArrayList<>();
        cities.add("Dnipro");
        cities.add("Toronto");
        cities.add("Amsterdam");
        cities.add("Bristole");
        cities.add("Kamenka");
        cities.add("Rome");
        cities.add("Ribbon");

        AtomicInteger id = new AtomicInteger(0);
        cities
                .stream()
                .map(city -> city + " is " + id.getAndIncrement() + " number at list")
                .peek(System.out::println)
                .collect(Collectors.toList());

        System.out.println("\nCity that contain letters A or E is:");
        cities
                .stream()
                .sorted()
                .filter(city -> city.contains("a") | city.contains("e"))
                .peek(System.out::println)
                .collect(Collectors.toList());

        String beginningLetters = "r";
        System.out.println("\nFirst letters is " + beginningLetters + " and the next city start from it: ");
        cities
                .stream()
                .filter(s -> s.toLowerCase().startsWith(beginningLetters.toLowerCase()))
                .peek(System.out::println)
                .collect(Collectors.toList());

        int minLettersNumberAtName = 6;
        System.out.println("\nCity with number of letters more than " + minLettersNumberAtName + " is:");
        List<String> citiesWithLettersNumberFilter = cities
                .stream()
                .filter(s -> s.length() > minLettersNumberAtName)
                .peek(System.out::println)
                .collect(Collectors.toList());

        System.out.println("\nSorted collection of the cities with letter numbers:");
        cities
                .stream()
                .map(s -> s + "(" + s.length() + ")")
                .sorted(Comparator.comparingInt(String::length).reversed())
                .peek(System.out::println)
                .collect(Collectors.toList());
    }
}
