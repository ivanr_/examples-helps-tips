package icom.stream.api;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class UpgradedInterface {
}

interface Performance {
    public String getName();

    public Stream<Artist> getMusicians();
}

interface UpgradedPerformance extends Performance {
    public default Stream<String> getAllMusicians(Artist artist) {
        return Stream
                .concat(
                        getMusicians()
                                .map(Artist::getName),
                        Stream
                                .of(artist.getParticipators())
                                .filter(Objects::nonNull)
                                .flatMap(Collection::stream));
    }
}

class Artist {
    private final String Name;
    private List<String> participators;

    public Artist(String name, List<String> participators) {
        this.Name = name;
        this.participators = participators;
    }

    public String getName() {
        return Name;
    }

    public List<String> getParticipators() {
        return participators;
    }

    public void setParticipators(List<String> participators) {
        this.participators = participators;
    }
}