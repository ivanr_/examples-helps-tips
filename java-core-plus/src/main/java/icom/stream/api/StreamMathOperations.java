package icom.stream.api;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamMathOperations {

    public static void main(String[] args) {
        new DataHolder();
        int result = DataHolder.numbers
                .stream()
                .reduce(0, (accumulator, integer2) -> accumulator + integer2);
        System.out.printf("Summary result is %d, got from list of number above\n", result);

        result = DataHolder.numbers
                .stream()
                .min(Comparator.comparing(Integer::intValue))
                .get();
        System.out.printf("Minimum value is %d of numbers from list above\n", result);

        result = DataHolder.numbers
                .stream()
                .max(Comparator.comparing(Integer::intValue))
                .get();
        System.out.printf("Maximum value is %d of numbers from list above\n", result);

        System.out.println("The numbers of Uppercase letters in line is next");
        List<Long> o =
                DataHolder.stringsList
                        .stream()
                        .peek(s -> System.out.print(s + "\""))
                        .map(res -> res
                                .chars()
                                .filter(Character::isUpperCase)
                                .count())
                        .peek(s -> System.out.print(s + "\"\n"))
                        .collect(Collectors.toList());

        Long maxNumberOfLowercaseLetter = DataHolder.stringsList
                .stream()
                .mapToLong(str -> str
                        .chars()
                        .filter(Character::isLowerCase)
                        .count()
                        )
                .max()
                .getAsLong();
        System.out.printf("The max number of lowercase letter is %d in the strings of list \n %s\n",
                maxNumberOfLowercaseLetter,
                DataHolder.stringsList.toString());
        System.out.println("breakpoint");
    }
}

class DataHolder {
    static List<Integer> numbers = new ArrayList<>();
    static List<String> stringsList = new ArrayList<>();

    public DataHolder() {
        numbers.add(10);
        numbers.add(54);
        numbers.add(394);
        numbers.add(77);
        stringsList.add("here just One upper case letter");
        stringsList.add("here TWo upper case letters");
        stringsList.add("Here Three Upper case letters");
        stringsList.add("Here Five Upper Case Letters");
        stringsList.add("here upper case letters is absent");
    }
}