package icom.stream.api;

import java.util.List;

public class RefactorToOptional {
}

class Artists {
    private List<Artist> artists;

    public Artists(List<Artist> artists) {
        this.artists = artists;
    }

    public Artist getArtist(int index) {
        if (index < 0 || index >= artists.size()) {
            indexException(index);
        }
        return artists.get(index);
    }

    private void indexException(int index) {
        throw new IllegalArgumentException(index + " doesn't correspond to an Artist");
    }

    public String getArtistName(int index) {
        try {
            Artist artist = getArtist(index);
            return artist.getName();
        } catch (IllegalArgumentException е) {
            return "unknown";
        }
    }
}

class OptionalRefactoredArtists extends Artists {

    public OptionalRefactoredArtists(List<Artist> artists) {
        super(artists);
    }
}