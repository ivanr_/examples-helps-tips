package icom.stream.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OptionalExample {
    public static void main(String[] args) {
        List<UserData> userDataList = new ArrayList<>();
//        add valid user
        userDataList.add(new UserData("Denny", 123));
//        add uncheckable null data
        userDataList.add(null);

        Optional<UserData> userNotNull = Optional.ofNullable(userDataList.get(0));
        Optional<UserData> userDataNull = Optional.ofNullable(userDataList.get(1)); //NPE if use ".of()"
        System.out.println("Data is present in optional variable userNotNull: " + userNotNull.isPresent());
        System.out.println("Data is present in optional variable userDataNull: " + userDataNull.isPresent());

        UserData requestedNotNullUser = userNotNull.orElse(new UserData("time limited session name", 000));
        UserData requestedNotNullUserGet = userNotNull.orElseGet(() -> new UserData("time limited session name", 000));
        UserData requestedDataNull = userDataNull.orElse(new UserData("time limited session name", 000));
        System.out.println("Debug breakpoint");
    }
}

class UserData {
    private final String NAME;
    private final int ID;
    private int cardTotal = 0;

    public UserData(String NAME, int ID) {
//        doing some validation
        this.NAME = NAME;
        this.ID = ID;
    }

    public String getNAME() {
        return NAME;
    }

    public int getID() {
        return ID;
    }

    public int getCardTotal() {
        return cardTotal;
    }

    public void addToCardTotal(int moneyToAddCardTotal) {
        this.cardTotal += moneyToAddCardTotal;
    }
}