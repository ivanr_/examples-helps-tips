package icom.serialization.simple.example;

import java.io.*;

class ParentClassA implements Serializable {
    String dataA1 = "some original data class A NOT marked as transient";
    transient String dataA2 = "some original data class A mark transient";
}

class ParentClassC implements Serializable /*, Externalizable*/ {
    String dataC1 = "some original data class A NOT marked as transient";
    transient String dataC2 = "some original data class A mark transient";

    ParentClassC() {
    }

    public void setDataC1(String dataC1) {
        this.dataC1 = dataC1;
    }

    public void setDataC2(String dataC2) {
        this.dataC2 = dataC2;
    }

//    @Override
//    public void writeExternal(ObjectOutput out) throws IOException {
//            out.writeChars(dataC1);
//            out.writeChars(dataC2);
////            out.writeObject(this);
//    }
//
//    @Override
//    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
//        System.out.println(in.available());
//        dataC1 = (String) in.readObject();
//        dataC2 = (String) in.readObject();
////        this = (ParentClassC) in.readObject();
//    }
}

class ChildClassB extends ParentClassA implements Serializable {
    ParentClassC classC = new ParentClassC();
    transient int charDataB = 4;
    public int intDataB = 5;
    String stringDataB = "d";
}


public class DoSerializeDeserialize {
    public void doOperations() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("DtoFile.out"))) {
            ParentClassC ts = new ParentClassC();
            System.out.println("dataC1 before updating: " + ts.dataC1);
            System.out.println("dataC2 before updating: " + ts.dataC2);
            ts.setDataC1("updated dataC1");
            ts.setDataC2("updated dataC2");
            oos.writeObject(ts);
            oos.flush();
            oos.close();
            System.out.println("dataC1 after updating: " + ts.dataC1);
            System.out.println("dataC2 after updating: " + ts.dataC2);
        } catch (IOException ex) {
            // error
        }
        try (ObjectInputStream oin = new ObjectInputStream(new FileInputStream("DtoFile.out"))) {
            ParentClassC ts = (ParentClassC) oin.readObject();
            System.out.println("dataC1 after restoring: " + ts.dataC1);
            System.out.println("dataC2 after restoring: " + ts.dataC2);
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("");
        }
    }
}
