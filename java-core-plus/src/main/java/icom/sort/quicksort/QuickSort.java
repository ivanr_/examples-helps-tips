package icom.sort.quicksort;

import icom.sort.algorithms.Sort;

import java.util.Arrays;

public class QuickSort implements Sort {

    @Override
    public int[] sort(int[] input) {

        System.out.println("Input array: " + Arrays.toString(input));
        if (input.length == 1 | input.length == 0) return input;
        if (input.length == 2) return sortMinorLeftMajorRight(input);
        int pivotIndex = input.length / 2;
        int pivotElement = input[pivotIndex];
        input = sortMinorLeftMajorRight(input);
        System.out.println("Input array: " + Arrays.toString(input));

//        find left array
        int[] leftArray = Arrays.copyOfRange(input, 0, findPivotIndexByElement(input, pivotElement) + 1);
        System.out.println("Left array: " + Arrays.toString(leftArray));
//        find right array
        int[] rightArray = Arrays.copyOfRange(input, findPivotIndexByElement(input, pivotElement) + 1, input.length);
        System.out.println("Right array: " + Arrays.toString(rightArray));

//        sort left
        leftArray = sort(leftArray);
//        sort right
        rightArray = sort(rightArray);
//        merge leftArray-pivotElement-rightArray
        input = mergeLeftPivotRightArray(leftArray, rightArray);

        System.out.println("Output array: " + Arrays.toString(input));
        return input;
    }

    private int[] sortMinorLeftMajorRight(int[] input) {

//        place minors left and majors right
        int pivotIndex = input.length / 2;
        int[] tempArr = new int[input.length];
        int arrIndex = 0;
        int arrBackIndex = 0;
        System.out.println("Pivot element index: " + pivotIndex);
        for (int count = 0; count < input.length; count++) {
            if (input[count] < input[pivotIndex]) {
                tempArr[arrIndex++] = input[count];
            }
            if (input[count] > input[pivotIndex]) {
                tempArr[input.length - 1 - arrBackIndex++] = input[count];
            }
        }
        int pivotElementAdditionCount = input.length - (arrIndex + arrBackIndex);
        for (int count = 0; count < pivotElementAdditionCount; count++) {
            tempArr[arrIndex++] = input[pivotIndex];
        }
        return tempArr;
    }

    private int findPivotIndexByElement(int[] input, int pivotElement) {
        for (int count = 0; count < input.length; count++) {
            if (input[count] == pivotElement) {
                return count;
            }
        }
        throw new IllegalArgumentException("element not found");
    }

    private int[] mergeLeftPivotRightArray(int[] leftArray, int[] rightArray) {
        int[] mergedArray = new int[leftArray.length + rightArray.length];
        System.arraycopy(leftArray, 0, mergedArray, 0, leftArray.length);
        System.arraycopy(rightArray, 0, mergedArray, leftArray.length, rightArray.length);
        return mergedArray;
    }
}

