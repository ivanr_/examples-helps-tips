package icom.sort;

import icom.sort.algorithms.Sort;

import java.util.Arrays;
// NOT REFACTORED YET!!!!!!!!!!!!
public class MySortMethodComplexityOn implements Sort {
    //README ---- sort function works with positive elements ONLY (right now)
    @Override
    public int[] sort(int[] input) {
        System.out.println("My method for arrays sorting");
        System.out.println("Input array: " + Arrays.toString(input));
//        find max value from input massive - algorithm not optimised with definition of min value
        int maxValue = input[0];
        int minValue = input[0];

        boolean zeroIsPresent = false;
        for (int count = 1; count < input.length; count++) {
            if (maxValue < input[count]) {
                maxValue = input[count];
            }
            if (minValue > input[count]) {
                minValue = input[count];
            }
            if (!zeroIsPresent && input[count] == 0) {
                zeroIsPresent = true;
            }
        }
//        create massive to output sorted data
        int[] sortedResult = new int[maxValue + 1];
//        place element to sortedResult massive - OUTPUT with ZERO elements
        for (int count = 0; count < input.length; count++) {
            sortedResult[input[count]] = input[count];
        }
        System.out.println("Massive with zeros: " + Arrays.toString(sortedResult));
//        relocate elements to massive without zeros from sortedResult massive
        int[] excludeZeroResult = new int[input.length];
        int resultCount = 0;
        for (int count = 0; count < sortedResult.length; count++) {
            if (sortedResult[count] != 0) {
                excludeZeroResult[resultCount++] = sortedResult[count];
            }
        }
        System.out.println("Massive without zeros inside : " + Arrays.toString(excludeZeroResult));
//        delete zeros from the end (if was duplicated values)
        for (resultCount = 0; resultCount < excludeZeroResult.length; resultCount++) {
            if (excludeZeroResult[excludeZeroResult.length - 1 - resultCount] != 0) {
                break; //used for getting numbers of zero into variable resultCount!!!
            }
        }
        System.out.println("Zero's number at the end of massive (number of elements duplications): " + resultCount);
        if (resultCount != 0) {
            int[] returnMassive = new int[excludeZeroResult.length-resultCount];
            for (int count = 0; count < returnMassive.length; count++) {
                returnMassive[count] = excludeZeroResult[count];
            }
            System.out.println("Finally sorted without duplications: " + Arrays.toString(returnMassive));
            return returnMassive;
        }
        System.out.println("Finally sorted without duplications: " + Arrays.toString(excludeZeroResult));
        return excludeZeroResult;
    }
}