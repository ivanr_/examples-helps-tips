package icom.sort.algorithms;

public class Util {
    public static int[] swap(int[] inputArray, int firstIndexToSwap, int secondIndexToSwap) {
        int tempObj = inputArray[firstIndexToSwap];
        inputArray[firstIndexToSwap] = inputArray[secondIndexToSwap];
        inputArray[secondIndexToSwap] = tempObj;
        return inputArray;
    }
}
