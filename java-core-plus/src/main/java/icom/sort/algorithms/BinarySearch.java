package icom.sort.algorithms;

import java.util.Arrays;

public class BinarySearch {
    public void search(int[] inputArray, int numberToFind) {
        double iterationCount = Math.log(inputArray.length) / Math.log(2);

        iterationCount = Math.ceil(iterationCount);
        int resultIndex = 0;
        boolean resultIsAbsent = true;

        for (int counter = 0; counter < iterationCount; counter++) {
//        find the middle number of array
            int middleIndex = (int) Math.floor(inputArray.length / 2.);
            resultIndex += middleIndex;
//        check middle number for matching
            if (numberToFind == inputArray[middleIndex]) {
                System.out.println("Number " + numberToFind + " is present at array with index " + (resultIndex));
                resultIsAbsent = false;
                return; // operator for first founded result to done
            }
            if (numberToFind <= inputArray[middleIndex]) {
                inputArray = Arrays.copyOf(inputArray, middleIndex);
                resultIndex -= middleIndex;
            } else if (numberToFind > inputArray[middleIndex]) {
                inputArray = Arrays.copyOfRange(inputArray, middleIndex, inputArray.length);
            }

            System.out.println("Temporary array" + Arrays.toString(inputArray));
            if (counter == iterationCount - 1 & resultIsAbsent) {
                System.out.println("Result is absent");
            }
        }
    }
}
