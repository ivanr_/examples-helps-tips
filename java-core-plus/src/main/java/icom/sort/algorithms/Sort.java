package icom.sort.algorithms;

public interface Sort{
    int[] sort(int[] input);
}
