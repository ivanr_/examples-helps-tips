package icom.sort.algorithms;

import java.util.Arrays;

public class InsertionSort implements Sort {

    @Override
    public int[] sort(int[] input) {


        for (int i = 0; i < input.length - 1; i++) {
            System.out.println("Array before sorting: " + Arrays.toString(input));

            int min = input[i];
            int indexMin = i;
            int tempValueMinDefine;
            for (int j = i; j < input.length; j++) {
                if (input[j] < min) {
                    min = input[j];
                    indexMin = j;
                }
            }
            tempValueMinDefine = input[i];
            input[i] = min;
            input[indexMin] = tempValueMinDefine;

            System.out.println("Array after sorting: " + Arrays.toString(input) + ". Min value: " + min + "\n");
        }
        return input;
    }
}
