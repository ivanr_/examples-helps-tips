package icom.sort.algorithms;

import java.util.Arrays;

public class MergeSort implements Sort {

    @Override
    public int[] sort(int[] input) {
        System.out.println("Input array: " + Arrays.toString(input));

//        find merge iteration number
        double iterationCount = Math.log(input.length) / Math.log(2);
        iterationCount = Math.ceil(iterationCount);
        int inputedLength = input.length;

//        find maxInputValue value
        int maxInputValue = input[0];
        for (int count = 1; count < input.length; count++) {
            if (maxInputValue < input[count]) {
                maxInputValue = input[count];
            }
        }
//        creating arrays with full 2pow(n) elements
        int[] arrayLengthMathPowerTo2 = new int[(int) Math.pow(2, iterationCount)];
        for (int count = 0; count < arrayLengthMathPowerTo2.length; count++) {
            if (count < input.length) {
                arrayLengthMathPowerTo2[count] = input[count];
            } else {
                arrayLengthMathPowerTo2[count] = maxInputValue;
            }
        }
        input = arrayLengthMathPowerTo2;
        System.out.println("Input array with added values: " + Arrays.toString(input));
//        EXECUTING ARRAY SORT
        for (int count = 0; count < iterationCount; count++) {
            input = sortAndMergeArrays(input, count + 1);
        }
        input = Arrays.copyOf(input, inputedLength);

        System.out.println("Output array" + Arrays.toString(input));
        return input;
    }

    private int[] mergeSortedLeftRight(int[] masLeft, int[] masRight) {
        int[] tempArray = new int[masLeft.length + masRight.length];
        int leftArrayIndex = 0;
        int rightArrayIndex = 0;
        for (int countElement = 0; countElement < tempArray.length; countElement++) {
            if (leftArrayIndex < masLeft.length && rightArrayIndex < masRight.length) {
                if (masLeft[leftArrayIndex] > masRight[rightArrayIndex]) {
                    tempArray[countElement] = masRight[rightArrayIndex++];
                } else {
                    tempArray[countElement] = masLeft[leftArrayIndex++];
                }
            } else {
//                check for the end of left array
                if (leftArrayIndex == masLeft.length && rightArrayIndex != masRight.length) {
                    tempArray[countElement] = masRight[rightArrayIndex++];
                } else {
//                check for the end of left array
                    tempArray[countElement] = masLeft[leftArrayIndex++];
                }
            }
        }
        return tempArray;
    }

    private int[] sortAndMergeArrays(int[] inputArray, int step) {
        int[] arrLeft;
        int[] arrRight;
        int offset = 1;
        int mergeIndex = 0;
        for (int stepCount = 0; stepCount < step; stepCount++) {
            offset *= 2;
        }
        for (int count = 0; count < inputArray.length; count += offset) {
            arrLeft = new int[offset / 2];
            arrRight = new int[offset / 2];
            for (int countLeft = count; countLeft < count + offset / 2; countLeft++) {
                if (countLeft < inputArray.length) {
                    arrLeft[countLeft - count] = inputArray[countLeft];
                }
                if (countLeft + offset / 2 < inputArray.length) {
                    arrRight[countLeft - count] = inputArray[countLeft + offset / 2];
                }
            }
            int[] mergedArray = mergeSortedLeftRight(arrLeft, arrRight);
            for (int outputArrayIndex = 0; outputArrayIndex < mergedArray.length; outputArrayIndex++) {
                inputArray[mergeIndex++] = mergedArray[outputArrayIndex];
            }
        }
        System.out.println("Step " + step + ", sorted array: " + Arrays.toString(inputArray));
        return inputArray;
    }
}
