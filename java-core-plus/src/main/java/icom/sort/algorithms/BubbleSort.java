package icom.sort.algorithms;

import java.util.Arrays;

public class BubbleSort implements Sort {

    public int[] sort(int[] input) {
        int temp;

        for (int i = 0; i < input.length - 1; i++) {

            boolean isChanged = false;
            System.out.println("Array before sorting: " + Arrays.toString(input));
            for (int j = 0; j < input.length - 1; j++) {

                if (input[j] > input[j + 1]) {
                    input = Util.swap(input, j, j + 1);
                    isChanged = true;
                }
            }
            System.out.println("Array after sorting: " + Arrays.toString(input) + "\n");
            if (!isChanged) {
                System.out.println("Input data array sorted");
                break;
            }
        }
        return input;
    }
}
