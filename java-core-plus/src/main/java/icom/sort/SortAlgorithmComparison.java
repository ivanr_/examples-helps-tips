package icom.sort;

import icom.sort.quicksort.QuickSort;

public class SortAlgorithmComparison {
    public static void main(String[] args) {
        int[] arrayInt = {4, 9, 7, 8, 5, 4, 3, 1, 11, 13, 4};
        int[] sortedArrayInt = {1, 1, 4, 5, 7, 7, 8, 8, 9, 10, 13, 15, 17, 17, 20};
//        new InsertionSort().sort(arrayInt);
//        new SelectionSort().sort(arrayInt);
//        new BubbleSort().sort(arrayInt);
//        new BinarySearch().search(sortedArrayInt, 20);
//        new MergeSort().sort(arrayInt);

        new QuickSort().sort(arrayInt);

//        new MySortMethodComplexityOn().sort(arrayInt);
    }
}

