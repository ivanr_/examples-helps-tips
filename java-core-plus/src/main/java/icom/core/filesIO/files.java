package icom.core.filesIO;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

class FilterFile {
    void filtrateJavaFiles() {
        String typeFile = "";
        String dirname = (new File("")).getAbsolutePath() + "\\src\\main\\java\\icom\\CORE\\FileIO\\source\\";
        File file1 = new File(dirname);
        HashSet<String> typesOfFilesInDirectory = new HashSet<>();
        String typeSeached = ".xml";
        System.out.println("Filtrated type: " + typeFile);
        ArrayList<String> filtratedFiles = new ArrayList<>(10);
        if (file1.isDirectory()) {
            System.out.println("Directory: " + dirname);
            String[] directoryList = file1.list();

            for (int counter = 0; counter < directoryList.length; counter++) {
                File tempFile = new File(dirname + File.pathSeparator + directoryList[counter]);
                if (tempFile.isDirectory()) {
//                    System.out.println(directoryList[counter] + " is directory");
                } else {
//                    System.out.println(directoryList[counter] + " is file");
                    int indexOfPoint = directoryList[counter].lastIndexOf(".");
                    for (int indexCounter = indexOfPoint; indexCounter < directoryList[counter].length(); indexCounter++) {
                        typeFile += directoryList[counter].charAt(indexCounter);
                    }
                    typesOfFilesInDirectory.add(typeFile);
                    if (typeFile.equals(typeSeached)) {
                        filtratedFiles.add(directoryList[counter]);
                    }
                    typeFile = "";
                }
            }
        } else {
            if (file1.exists()) {
                System.out.println("Path doesn't exist");
            } else {
                System.out.println(dirname + " is NOT a directory");
            }

        }
        System.out.println(typesOfFilesInDirectory.toString() + " - filetypes in present path dataHolders");
        System.out.println(filtratedFiles.toString() + " - dataHolders with filtrated types in present path");
    }
}

class ReadRootDirectories {
    void readRoots() {
        try {
            File tmpFile = File.createTempFile("pref_", "_suff");
            File[] roots = File.listRoots();
            String parentPath = tmpFile.getParent();
            System.out.println(parentPath);
            File parentFile = tmpFile.getParentFile();
            System.out.println(parentFile);
            for (File root : roots) {
                System.out.println(root);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class FromPresentation {
    void runPresentationFile() {
        String dirname = ( (new File("")).getAbsolutePath() ) + "\\src\\main\\java\\icom\\";
        System.out.println(dirname);
        File file1 = new File(dirname);
        if (file1.isDirectory()) {
            System.out.println("Directory: " + dirname);
            String[] directoryList = file1.list();

            for (String s : directoryList) {
                File tempFile = new File(dirname + "\\" + s);
                if (tempFile.isDirectory()) {
                    System.out.println(s + " is directory");
                } else {
                    System.out.println(s + " is file");
                }
            }
        } else {
            System.out.println(dirname + " is NOT a directory");
        }
    }
}

public class files {

    public static void main(String[] args) {
        ReadRootDirectories roots = new ReadRootDirectories();
        FromPresentation myRun = new FromPresentation();
        FilterFile filt = new FilterFile();
        File f = new File((new File("")).getAbsolutePath());
        filt.filtrateJavaFiles();
        System.out.println("Temp and root directories is: ");
        roots.readRoots();
        System.out.println("Run");
        myRun.runPresentationFile();
    }
}