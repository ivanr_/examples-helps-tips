package icom.core.generics;
class TwoGenerics <T, A>
{
    T object1;
    A object2;
    TwoGenerics (T one, A two)
    {
        object1 = one;
        object2 = two;
    }
    public T getFirst ()
    {
        return object1;
    }
    public A getSecond ()
    {
        return object2;
    }
}
public class generics_slides1 {
    public static void main(String[] args) {
        TwoGenerics <Integer, String> pair = new TwoGenerics <Integer, String> (6, "Apr");
        System.out.println(pair.getFirst() + pair.getSecond());
    }
}
