package icom.core.generics;

import java.util.Objects;

class Date <DD, MM, YYYY>
{
    private DD object1;
    private MM object2;
    private YYYY object3;
    Date (DD day, MM month, YYYY year)
    {
        object1 = day;
        object2 = month;
        object3 = year;
    }
    DD getDay()
    {
        return object1;
    }
    MM getMonth()
    {
        return object2;
    }
    YYYY getYear()
    {
        return object3;
    }
    public void setDate (DD day, MM month, YYYY year)
    {
        object1 = day;
        object2 = month;
        object3 = year;
    }
}
class MyMap_Key_Value1_Value2 <K, V1, V2>
{
    private K key;
    private V1 value1;
    private V2 value2;
    MyMap_Key_Value1_Value2 (K key, V1 value1, V2 value2)
    {
        this.key = key;
        this.value1 = value1;
        this.value2 = value2;
    }

    public K getKey() {return key;}
    public V1 getValue1() {return value1;}
    public V2 getValue2() {return value2;}
    public void setKey(K key) {this.key = key;} //maybe do private/ add delete-remove function/ check for another SAME KEY to prevent wrong values1-2
    public void setValue1(V1 value1) {this.value1 = value1;}
    public void setValue2(V2 value2) {this.value2 = value2;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyMap_Key_Value1_Value2<?, ?, ?> that = (MyMap_Key_Value1_Value2<?, ?, ?>) o;
        return Objects.equals(key, that.key) &&
                Objects.equals(value1, that.value1) &&
                Objects.equals(value2, that.value2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value1, value2);
    }
}

public class generics1 {
    public static void main(String[] args) {
        Date <Integer, String, Integer> dateIntStrInt = new Date<> (22, "Aug", 2018);
        Date <Integer, Integer, Integer> dateIntIntInt = new Date<> (22, 8, 2018);
        Date <String, String, Integer> dateStrIntInt = new Date<> ("twenty two", "Aug", 2018);
        System.out.println("Date is:\n" + "Day: " + dateIntIntInt.getDay() + "\nMonth: " + dateIntIntInt.getMonth() + "\nYear: " + dateIntIntInt.getYear());
        System.out.println("\nBy another type Date is:\n" + "Day: " + dateIntStrInt.getDay() + "\nMonth: " + dateIntStrInt.getMonth() + "\nYear: " + dateIntStrInt.getYear());
        System.out.println("\nBy third type Date is:\n" + "Day: " + dateStrIntInt.getDay() + "\nMonth: " + dateStrIntInt.getMonth() + "\nYear: " + dateStrIntInt.getYear());
        Date<Integer, Integer, Integer> dateSetAllInteger = new Date<>(22, 8, 2018);
        Date<Integer, String, Integer> dateSetIntegerAndString = new Date<>(22, "Aug", 2018);
        MyMap_Key_Value1_Value2 <String, String, Integer> map1 = new MyMap_Key_Value1_Value2<>("1", "some name field", 1);
        MyMap_Key_Value1_Value2 <Integer, String, Integer> map2 = new MyMap_Key_Value1_Value2<>(1, "some name field", 1);
        System.out.println(map1.getKey());
        System.out.println(map2.getKey());
        // if (map1.getKey()=map2.getKey()) {System.out.println("The same");}   field type is different, error!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        System.out.println( map1.equals(map2) ); // they are not equal!!!!!!!!!!!!!!!!!!!
    }
}
