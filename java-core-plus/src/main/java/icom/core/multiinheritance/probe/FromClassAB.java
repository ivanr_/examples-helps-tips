package icom.core.multiinheritance.probe;

import icom.core.multiinheritance.probe.parents.ParentClassA;
import icom.core.multiinheritance.probe.parents.ParentClassB;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

interface FromClassAB {
    default void testA(String methodName) throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        Method method = ParentClassA.class.getMethod(methodName);
        method.invoke(ParentClassA.class.newInstance());
    }

    default void testB(String methodName) throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        Method method = ParentClassB.class.getMethod(methodName);
        method.invoke(ParentClassB.class.newInstance());
    }
}
