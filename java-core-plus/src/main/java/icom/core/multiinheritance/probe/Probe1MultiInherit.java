package icom.core.multiinheritance.probe;

import java.lang.reflect.InvocationTargetException;

public class Probe1MultiInherit implements FromClassAB {
    void test() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        testA("");
        testB("");
    }
}
