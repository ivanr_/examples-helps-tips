package icom.core.multiinheritance.probe;

import icom.core.multiinheritance.probe.parents.ParentClassA;
import icom.core.multiinheritance.probe.parents.ParentClassB;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Probe2MultiInherit {
    public void testA(String methodName) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Method methodA = ParentClassA.class.getMethod(methodName);
        methodA.invoke(ParentClassA.class.newInstance());
    }

    public void testB(String methodName) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Method methodB = ParentClassB.class.getMethod(methodName);
        methodB.invoke(ParentClassB.class.newInstance());
    }
}
