package icom.core.multiinheritance.probe.parents;

public class ParentClassA {
    public void testA() {
        System.out.println("This is parent A, method TestA()");
    }
}
