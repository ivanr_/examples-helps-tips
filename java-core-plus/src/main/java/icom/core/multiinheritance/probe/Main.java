package icom.core.multiinheritance.probe;

import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Probe1MultiInherit multiInherit1 = new Probe1MultiInherit();
        Probe2MultiInherit multiInherit2 = new Probe2MultiInherit();
        Probe3MultiInherit multiInherit3 = new Probe3MultiInherit();

        System.out.println("Probe1 results:");
        multiInherit1.testA("testA");
        multiInherit1.testB("testB");

        System.out.println("Probe2 results:");
        multiInherit2.testA("testA");
        multiInherit2.testB("testB");

        System.out.println("Probe3 results:");
        multiInherit3.testFromA();
        multiInherit3.testFromB();

    }
}
