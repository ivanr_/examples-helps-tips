package icom.core.multiinheritance.probe.parents;

public class ParentClassB {
    public void testB() {
        System.out.println("This is parent B, method TestB()");
    }
}
