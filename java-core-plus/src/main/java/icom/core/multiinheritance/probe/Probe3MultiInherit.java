package icom.core.multiinheritance.probe;

import icom.core.multiinheritance.probe.parents.ParentClassA;
import icom.core.multiinheritance.probe.parents.ParentClassB;

public class Probe3MultiInherit {
    private ParentClassA a = new ParentClassA();
    private ParentClassB b = new ParentClassB();

    public void testFromA() {
        a.testA();
    }

    public void testFromB() {
        b.testB();
    }
}
