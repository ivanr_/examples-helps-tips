package icom.core.multiinheritance.collisions;

public class InterfaceWithDefaultCollisions implements C, D{
    @Override
    public void duplicatedMethod(String someData) {
        System.out.println("method MUST BE overridden");
    }
}


interface C{
    default void duplicatedMethod(String someData){
        System.out.println("interface C message");
    }

    default void someDefaultMethodNoNeedToBeImplemented(){

    }
}

interface D{
    default void duplicatedMethod(String someData) {
        System.out.println("interface D message");
    }
}