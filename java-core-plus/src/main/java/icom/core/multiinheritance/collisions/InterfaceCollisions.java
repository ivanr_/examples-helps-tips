package icom.core.multiinheritance.collisions;

public class InterfaceCollisions implements A, B{
    @Override
    public void duplicatedMethod(String someData) {
        System.out.println("successful implementation");
    }
}

interface A{
    void duplicatedMethod(String someData);
}

interface B{
    void duplicatedMethod(String someData);
}
