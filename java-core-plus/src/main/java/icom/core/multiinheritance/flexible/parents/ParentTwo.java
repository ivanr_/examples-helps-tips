package icom.core.multiinheritance.flexible.parents;

public class ParentTwo {
    public void method() {
        System.out.println("This is method From class ParentTwo");
    }

    public void methodA() {
        System.out.println("This is methodA From class ParentTwo");
    }

    public void methodB() {
        System.out.println("This is methodB From class ParentTwo");
    }
}
