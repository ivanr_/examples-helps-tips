package icom.core.multiinheritance.flexible;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FlexMultiInheritImpl extends MultiInheritedAbstrClass {

    FlexMultiInheritImpl(ArrayList<Class> inheritedClasses) {
        super(inheritedClasses);
    }

    @Override
    protected Object softUseOfInheritedMethod(String methodName) {
        ArrayList<Method> methodList = new ArrayList<>();
        for (int classCounter = 0; classCounter < inheritedClasses.size(); classCounter++) {
            Method[] methods = inheritedClasses.get(classCounter).getMethods();
            for (int methodCounter = 0; methodCounter < methods.length; methodCounter++) {
                methodList.add(methods[methodCounter]);
                if (methodName == methods[methodCounter].getName()) {
                    Method findedMethod = methods[methodCounter];
                    try {
//                        ACCOUNT FOR INVOKE PARAMETERS!!!!!!!!!!
                        return findedMethod.invoke(inheritedClasses.get(classCounter).newInstance());
                    } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        methodList.stream().peek(method -> System.out.println(method.getName())).collect(Collectors.toSet());

        return methodList;
    }

    @Override
    protected Object forceUseOfInheritedMethod(String methodName) {
        return null;
    }

    @Override
    protected void includeClasses(List<Class> inheritedClassesMassive) {
        inheritedClasses.addAll(inheritedClassesMassive);
    }
}
