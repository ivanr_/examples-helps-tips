package icom.core.multiinheritance.flexible;

import java.util.ArrayList;
import java.util.List;

public abstract class MultiInheritedAbstrClass {
    protected ArrayList<Class> inheritedClasses;

    MultiInheritedAbstrClass(ArrayList<Class> inheritedClasses) {
        this.inheritedClasses = inheritedClasses;
    }

    //    Object is a returned type, must be casted to needed type individually

    //    soft method use first method from added classes massive, MUST return exception
    abstract protected Object softUseOfInheritedMethod(String methodName);

    //    force method use first method from added classes massive, MUST NOT return exception
    abstract protected Object forceUseOfInheritedMethod(String methodName);

    abstract protected void includeClasses(List<Class> inheritedClassesMassive);

    ArrayList<Class> getInheritedClasses() {
        return inheritedClasses;
    }
}
