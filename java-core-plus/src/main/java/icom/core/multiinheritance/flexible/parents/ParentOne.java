package icom.core.multiinheritance.flexible.parents;

public class ParentOne {
    public void methodOne() {
        System.out.println("This is methodOne From class ParentOne");
    }

    public void methodTwo() {
        System.out.println("This is methodTwo From class ParentOne");
    }

    public void method() {
        System.out.println("This is method From class ParentOne");
    }
}
