package icom.core.enumerations;

public class EnumOfTextFields {
    public static void main(String[] args) {
        SystemMessages.ERROR.getMessage();
        System.out.println("breakpoint");
    }
}

enum SystemMessages {
    ERROR("this is ERROR message"),
    LOADING("something is LOADING"),
    UPLOADING("UPLOADING data from user"),
    FAILURE("something went wrong, it's FAILURE message");

    private String message;

    SystemMessages(String message) {
        this.message = message;
    }

    public String getMessage() {
        System.out.println(message);
        return message;
    }
}