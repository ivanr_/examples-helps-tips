package icom.core.enumerations;

enum CarType {Sedan, Limousine, SportCar, Crossover, Hatchback}

enum GetForCar {
    NameFromTypes {
        public String action(MyCar[] carList, CarType car1Type) {
            StringBuilder s = new StringBuilder("\nList of all car type " + car1Type.toString() + ":\n");
            for (MyCar myCar : carList) {
                if (myCar.carType == car1Type) {
                    s.append(myCar.carName).append("\n");
                }
            }
            return s.toString();
        }
    },
    AllNamesCar {
        public String action(MyCar[] carList, CarType c) {
            String[] arrayCarName = new String[carList.length];
            StringBuilder s = new StringBuilder("\nList of all car name for all types:\n");
            for (MyCar myCar : carList) {
                //arrayCarName [i] = carList[i].carName; its array with car's names
                s.append(myCar.carName).append("\n");
            }
            return s.toString();
        }
    },
    MinVolumeEngine {  //irrational without Array [] returns from NameFromTypes!!!!!!!!!!! maybe do later

        public String action(MyCar[] carList, CarType carType1) {
            int minVolEng = 0;
            String firstCarName = "";
            StringBuilder s = new StringBuilder("ooops");
            for (int i = 0; i < carList.length; i++) {
                if (carList[i].carType.equals(carType1)) {
                    s = new StringBuilder("\nMin engine volume has car:\n" + carList[i].showCarData());
                    minVolEng = carList[i].engineVolume;
                    firstCarName = carList[i].carName;
                    break;
                }
            }
            //System.out.println(s);
            for (int i = 0; i < carList.length; i++) {
                if ((carList[i].engineVolume < minVolEng) & (carList[i].carType.equals(carType1))) {
                    s = new StringBuilder("\nMin engine volume has car:\n" + carList[i].showCarData());
                    minVolEng = carList[i].engineVolume;
                } else {
                    if ((carList[i].engineVolume == minVolEng) & !carList[i].carName.equals(firstCarName)
                            & (carList[i].carType.equals(carType1))) {
                        s.append("\n\nThe same Min engine volume has car:\n").append(carList[i].showCarData());
                    }
                }

            }
            return s.toString();
        }
    },
    MaxVolumeEngine {  //irrational without Array [] returns from NameFromTypes!!!!!!!!!!! maybe do later

        public String action(MyCar[] carList, CarType carType1) {
            int maxVolEng = 0;
            String firstCarName = "";
            StringBuilder s = new StringBuilder("ooops");
            for (int i = 0; i < carList.length; i++) {
                if (carList[i].carType.equals(carType1)) {
                    s = new StringBuilder("\nMax engine volume has car:\n" + carList[i].showCarData());
                    maxVolEng = carList[i].engineVolume;
                    firstCarName = carList[i].carName;
                    break;
                }
            }
            //System.out.println(s);
            for (int i = 0; i < carList.length; i++) {
                if ((maxVolEng < carList[i].engineVolume) & (carList[i].carType.equals(carType1))) {
                    s = new StringBuilder("\nMax engine volume has car:\n" + carList[i].showCarData());
                    maxVolEng = carList[i].engineVolume;
                } else {
                    if ((maxVolEng == carList[i].engineVolume) & !carList[i].carName.equals(firstCarName) &
                            (carList[i].carType.equals(carType1))) {
                        s.append("\n\nThe same Max engine volume has car:\n").append(carList[i].showCarData());
                    }
                }

            }
            return s.toString();
        }
    };

    public abstract String action(MyCar[] carList, CarType carType);

}

class MyCar {
    String carName;
    CarType carType;
    int engineVolume;

    MyCar(String s, CarType t, int i) {
        carName = s;
        carType = t;
        engineVolume = i;
    }

    void setCarData(String s, CarType t, int i) {
        carName = s;
        carType = t;
        engineVolume = i;
    }

    String showCarData(MyCar car) {
        String s = car.carName + " ";
        s += car.carType.toString() + " ";
        s += Integer.toString(car.engineVolume);
        return s;
    }

    String showCarData() {
        String s = "Car Name: " + carName;
        s += "\nCar Type: " + carType.toString();
        s += "\nCar Engine Volume: " + Integer.toString(engineVolume);
        return s;
    }
}

public class EnumField {

    public static void main(String[] args) {
        MyCar LadaGranta = new MyCar("Lada Granta", CarType.Sedan, 1500);
        MyCar VolvoV60 = new MyCar("Volvo V60", CarType.Sedan, 1900);
        MyCar RenaultLogan = new MyCar("Renault Logan", CarType.Sedan, 1500);
        MyCar LifanSolano = new MyCar("Lifan Solano", CarType.Sedan, 1800);

        MyCar Zil41047 = new MyCar("Zil-41047", CarType.Limousine, 2500);
        MyCar MercedesBenzW100 = new MyCar("Mercedes-Benz W100", CarType.Limousine, 2900);
        MyCar LincolnTownCar = new MyCar("Lincoln Town Car", CarType.Limousine, 3000);

        MyCar Mazda = new MyCar("Mazda", CarType.SportCar, 4500);
        MyCar Mclaren570GT = new MyCar("Mclaren 570GT", CarType.SportCar, 3900);
        MyCar BugattiVisionGranTurismo = new MyCar("Bugatti Vision Gran Turismo", CarType.SportCar, 5000);
        MyCar AcuraNSX = new MyCar("Acura NSX", CarType.SportCar, 5000);

        MyCar RenaultDuster = new MyCar("Renault Duster7", CarType.Crossover, 1800);
        MyCar ChevroletNiva = new MyCar("Chevrolet Niva", CarType.Crossover, 1900);
        MyCar NissanTerran = new MyCar("Nissan Terran", CarType.Crossover, 2000);

        MyCar SkodaFabia = new MyCar("Skoda Fabia", CarType.Hatchback, 1800);
        MyCar KIACeed = new MyCar("KIA Ceed", CarType.Hatchback, 1900);
        MyCar RenaultSandero = new MyCar("Renault Sandero", CarType.Hatchback, 2000);

        MyCar[] carAll = new MyCar[]{LadaGranta, VolvoV60, RenaultLogan, LifanSolano, Zil41047, MercedesBenzW100, LincolnTownCar,
                Mazda, Mclaren570GT, BugattiVisionGranTurismo, AcuraNSX, RenaultDuster, ChevroletNiva,
                NissanTerran, SkodaFabia, KIACeed, RenaultSandero};

        //System.out.println(SkodaFabia.showCarData());
        //String Sout = GetForCar.NameFromTypes.action(carAll, CarType.Limousine);
        //String Sout = GetForCar.MinVolumeEngine.action(carAll, CarType.Hatchback);
        //String Sout = GetForCar.MaxVolumeEngine.action(carAll, CarType.Hatchback);
        String Sout = GetForCar.AllNamesCar.action(carAll, CarType.SportCar);
        System.out.println(Sout);


    }

}


