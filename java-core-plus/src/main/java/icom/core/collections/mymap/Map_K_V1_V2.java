package icom.core.collections.mymap;

//how this tree works:

import java.util.ArrayList;
import java.util.Map;

class MyMapKVV<K, V1, V2> {
    private K key;
    private V1 value1;
    private V2 value2;
    private int size;
    private ArrayList <insideList> list = new ArrayList <>(10);

    private class insideList {
        private Object key;
        private V1 value1;
        private V2 value2;
        public insideList(Object key, V1 value1, V2 value2) {
            this.key = key;
            this.value1 = value1;
            this.value2 = value2;
        }
        public insideList() {
        }
    }
    public MyMapKVV (K key, V1 value1, V2 value2){
        this.key = key;
        this.value1 = value1;
        this.value2 = value2;
    }
    public MyMapKVV() {
    }

    public int size() {
        int counter = 0;
        try {
            while (list.get(counter)!=null) {
                counter++;
            }
        }
        catch (IndexOutOfBoundsException e) {
        }
        this.size = counter;
        return counter;
    }

    public boolean isEmpty() {
        for (int i = 0; i<list.size(); i++) {
            if (list.get(i)!=(null)) {
                return false;
            }
        }
//        System.out.println(list.size());
        return true;
    }

    public boolean containsKey(Object key) {
        int counter = 0;
        try {
            while (list.get(counter)!=null) {
                if (list.get(counter).key == key) {
                    return true;
                }
                counter++;
            }
        }
        catch (IndexOutOfBoundsException ignored) {
        }
        return false;
    }

    public boolean containsValue(Object value) {
        int counter = 0;
        try {
            for (counter = 0; counter< list.size(); counter++) {
                if ( (list.get(counter).value1 == value) | (list.get(counter).value2 == value)  ){
                    return true;
                }
            }
        }
        catch (IndexOutOfBoundsException e) {
        }
        return false;
    }

    public Object getValue1(Object key) {
        int counter;
        Object outValues = new Object();
        for (counter = 0; counter<list.size(); counter++) {
            if (list.get(counter).key == key) {
                outValues = (list.get(counter).value1);
                break;
            };
        }
        return outValues;
    }

    public Object getValue2(Object key) {
        int counter;
        Object outValues = new Object();
        for (counter = 0; counter<list.size(); counter++) {
            if (list.get(counter).key == key) {
                outValues = (list.get(counter).value2);
                break;
            };
        }
        return outValues;
    }

    public Object put(K key, V1 value1, V2 value2) {
        boolean alreadyAdd = false;
        try {
            for (int counter = 0; counter< list.size(); counter++) {
                if (list.get(counter).key == key) {
                    list.get(counter).value1 = value1;
                    list.get(counter).value2 = value2;
                    alreadyAdd = true;
                    break;
                }
            }
            if (!alreadyAdd) {
                insideList insertData = new insideList(key, value1, value2);
                list.add(insertData);
            }
            return true;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }


    public Object remove(Object key) {
        int counter = 0;
        try {
            while (list.get(counter)!=null) {
                if (list.get(counter).key == key) {
                    list.remove(counter);
                    return true;
                }
                counter++;
            }
        }
        catch (IndexOutOfBoundsException e) {
        }
        return false;
    }

    public void putAll(Map m) {

    }

    public void clear() {
        list.clear();
        size = 0;
    }

    public ArrayList keySet() {
        int counter = 0;
        ArrayList out = new ArrayList();
        try {
            while (list.get(counter)!=null) {
                out.add(list.get(counter).key);
                counter++;
            }
        }
        catch (IndexOutOfBoundsException e) {
        }
        return out;
    }

    public ArrayList [] values() {
        int counter = 0;
        ArrayList out1 = new ArrayList();
        ArrayList out2 = new ArrayList();
        try {
            while (list.get(counter)!=null) {
                out1.add(list.get(counter).value1);
                out2.add(list.get(counter).value2);
                counter++;
            }
        }
        catch (IndexOutOfBoundsException e) {
        }
        ArrayList [] out = new ArrayList[2];
        out [0] = out1;
        out [1] = out2;

        return out;
    }

//    public Set<Entry> entrySet() {
//        return null;
//    }

    public boolean equals(Object o) {
        return false;
    }

    public int hashCode() {
        return 0;
    }
}

public class Map_K_V1_V2 {
    public static void main(String[] args) {
        MyMapKVV <Object, Integer, String> map = new MyMapKVV();
        map.put ("key1", 1111, "as");
        map.put ("key2", 2222, "a21s");
        map.put (3, 3333, "a21s33");
        map.put (34, 3333, "a21s33");
        System.out.println("Is the map empty: " + map.isEmpty() + ". Map size: " + map.size());
        System.out.println("Map contains v1: " + map.getValue1("key1"));
        System.out.println("Map contains v2: " + map.getValue2("key1"));
        System.out.println("Map contains value 1111: " + map.containsValue(1111)); //FALSE!!!!!!!!!!!!!!!!!!!!! TYPE INCORRECT?
        System.out.println("Remove by key 3: " + map.remove(3) + " Map size: " + map.size());
        System.out.println("Key list at map: " + map.keySet().toString());
//        System.out.println(map.containsKey(3));
    }
}
