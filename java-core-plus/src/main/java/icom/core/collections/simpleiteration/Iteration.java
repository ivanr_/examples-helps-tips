package icom.core.collections.simpleiteration;

import java.util.*;

class People implements Iterable <People> {
    private String name;
    private String surName;
    private Iterator <People> iter = null;

    public People(String name, String surName) {
        this.name = name;
        this.surName = surName;
    }
    public People() {
    }

    public String getName() {
        return name;
    }

    public String getSurName() {
        return surName;
    }

    @Override
    public Iterator<People> iterator() {
        return iter;
    }
    public Iterator<People> iterator(int iterNumber) {
        return iter;
    }
}
 public class Iteration {
    public static void main(String[] args) {

        ArrayList <People> myPeoples = new ArrayList<>(Arrays.asList(new People("Alex", "Naumov"), new  People("Eugen", "Zakharov"),
                new People("Andrey", "Petrov")));

        ArrayList <String> myList = new ArrayList<>();
        myList.add("one");
        myList.add("two");
        myList.add("three");
        myList.add("four");
        myList.add("five");

        Iterator iter1 = myList.iterator();
        for (int i = 0; iter1.hasNext(); i++) {
            System.out.println(iter1.next());
        }

        Iterator<People> iterPeople = myPeoples.iterator();
        for (int i = 0; iterPeople.hasNext(); i++){
            People out =  iterPeople.next();
            System.out.println( "Name: " + out.getName() + "\n SurName: " + out.getSurName());
        }
    }
}
