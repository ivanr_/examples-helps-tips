package icom.core.collections.mylinkedlist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

class MyList implements Collection {
//    private int counter;
    private Object o;
    private int size;
    private Object [] list = new Object[10];

    @Override
    public int size() {
        int counter = 0;
        try {

            while (list[counter]!=null) {
                counter++;
            }
        }
        catch (ArrayIndexOutOfBoundsException e) {
        }
        this.size = counter;
        return counter;
    }

    @Override
    public boolean isEmpty() {
        for (int i = 0; i<list.length; i++) {
            if (list[i]!=(null)) {
                return false;
            }
        }
        System.out.println(list.length);
        return true;
    }

    @Override
    public boolean contains(Object o) {
        int counter = 0;
        for (int i = 0; i<list.length; i++) {
            if (list[i]==o) return true;
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return iterator();
    }

    @Override
    public Object[] toArray() { //all data should be type NUMBER???????????????????
        Object [] out = new Object[list.length];
        int countList = 0;
        int countOut = 0;
        for (countList = 0; countList<list.length; countList++) {

            if (list[countList]!=null){
                out [countList] = list[countList];
                countOut++;
            }
        }
        return out;
    }

    @Override
    public boolean add(Object o) {
        try {
            int counter = 0;

            while (list[counter]!=null) {
                counter++;
            }
            list[counter] = o;
            return true;
        }
        catch (Exception e) {
            System.out.println("asadsad");
            return false;
        }
    }

    @Override
    public boolean remove(Object o) {
        try {
            int counter = 0;
            while (list[counter]!=null) {
                counter++;
            }
            list[counter] = o;
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean addAll(Collection c) {
        try {
            Object [] temp = c.toArray();
            int countTemp = temp.length; //maybe with NULL data contains
            int summarySize = countTemp + size;
            Object[] result = new Object[summarySize];
            System.arraycopy(list, 0, result, 0, this.size);
            System.arraycopy(temp, 0, result, this.size, temp.length);
            list = result;
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray (Object [] a) {
        return new Object[0];
    }
}

class MyLinkedList {
    private Object o;
    private int size;
    private Object [] data;
    private insideElement pointer;
    private ArrayList <insideElement> list = new ArrayList<>(10);

    class insideElement{
        private Object data;
        private boolean first;
        private boolean last;

        insideElement (Object data, boolean first, boolean last) {
            this.data = data;
            this.first = first;
            this.last = last;
        }
    }
    MyLinkedList () {
    }

    public int getSize () {
        return list.size();
    }

    public boolean add (Object data) {
        boolean first;
        boolean last;
        try {
            if (list.size()==0) {
                first = last = true;
                list.add(new insideElement(data, first, last) );
                pointer = list.get(0);
            } else {
                first = false;
                list.get(list.size()-1).last = false;
                last = true;
                list.add(new insideElement(data, first, last) );
            }
            return true;
        }
        catch (Exception e) {
            System.out.println(e.fillInStackTrace());
            return false;
        }

    }

    public String getData () {
        String out = "";
        for (int counter = 0; counter<list.size(); counter++) {
             out += " " + list.get(counter).data.toString();
            if (list.get(counter).first==true) {
                out += "(first)";
            }
            if (list.get(counter).last==true) {
                out += "(last)";
            }
        }
        return out;
    }

    public Object previous() {
        if (list==null) {
            return new Object();
        }
        int counter = list.size()-1;
        while (list.get(counter)!=pointer) {
            counter--;
        }
        if (counter==0) {
            pointer = list.get(list.size()-1);
            return list.get(list.size()-1).data;
        } else {
            pointer = list.get(counter-1);
            return list.get(counter-1).data;
        }
    }

    public Object next() {
        if (list==null) {
            return new Object();
        }
        int counter = 0;
        while (list.get(counter)!=pointer) {
            counter++;
        }
        if (counter==list.size()-1) {
            pointer = list.get(0);
        } else {
            pointer = list.get(counter+1);
        }
        return list.get(counter).data;
    }

    public Object getFirst () {
        int counter = 0;
        while (list.get(counter).first!=true) {
            counter++;
        }
        pointer = list.get(counter);
        return pointer.data;
    }

    public Object getLast () {
        int counter = list.size();
        while (list.get(counter-1).last!=true) {
            counter--;
        }
        pointer = list.get(counter-1);
        return pointer.data;
    }


}

public class CircleLinked {
    public static void main(String[] args) {
        MyLinkedList myLinked = new MyLinkedList();
        System.out.println(myLinked.getSize());
        myLinked.add(35);
        myLinked.add(25);
        myLinked.add(15);
        myLinked.add(5);
        myLinked.add("25as");
        System.out.println(myLinked.getData().toString());

        System.out.println("Previous " + myLinked.previous());
        System.out.println("Previous " + myLinked.previous());
        System.out.println("Previous " + myLinked.previous());
        System.out.println("Previous " + myLinked.previous());
        System.out.println("Previous " + myLinked.previous());
        System.out.println("Previous " + myLinked.previous());
        System.out.println("Previous " + myLinked.previous());
        System.out.println("Next " + myLinked.next());
        System.out.println("Next " + myLinked.next());
        System.out.println("Next " + myLinked.next());
        System.out.println("Next " + myLinked.next());

        MyList myList = new MyList();
        MyList myAddList = new MyList();
        myList.add(12);
        myList.add("121");
        myList.add("123");
        myList.add("124");
        myAddList.add("162");
        myAddList.add("127");
        myAddList.add("128");
        myAddList.add("129");

        System.out.println(myList.size());
        System.out.println(myList.addAll(myAddList));
        System.out.println(myList.toArray().toString());
        int[] a= {1,2};
        int[] b= {3,4};
        int[] temp = new int[a.length+b.length];
        System.arraycopy(a,0,temp,0,a.length);
        System.arraycopy(b,0,temp,a.length,b.length);
        System.out.println(temp[0]);
        System.out.println(temp[1]);
        System.out.println(temp[2]);
        System.out.println(temp[3]);
    }
}
