package icom.core.exceptions;

import java.util.Scanner;

class myClassDivZero { //class for dividing by zero
    int c(int a) {
        int b = 0;
        return a / b;
    }
}

public class except {

    public static void main(String[] args) {
        // creating objects and init variables
        Scanner input = new Scanner(System.in);
        myClassDivZero divFunct = new myClassDivZero();
        Exception myExceptOut10 = new Exception("outOf10");
        int inpInt = 1;
        // print program title
        System.out.println("Input any NUMBER to get ArithmeticException");
        System.out.println("Input any STRING data type for NumberFormatException");
        System.out.println("Input any NUMBER GREATER THAN 10 to get Inserted Exception");
        String inpStr = input.nextLine();
        // unlimited cycle for trying exception program
        while (!inpStr.equals("exit")) {
            try {
                //try to convert to integer
                inpInt = Integer.parseInt(inpStr);
                // check for the number out of 10 before dividing by zero
                if (inpInt > 10) {
                    throw myExceptOut10;
                }
                //realisation dividing by zero
                int inpIntDiv = divFunct.c(inpInt);
            } catch (ArithmeticException e) {
                System.out.println("Find exception: " + e.fillInStackTrace());
                System.out.println("After this exception the programm is still on!");
            } catch (NumberFormatException e) {
                System.out.println("Find unexceptable data for integer type, reson: " + e.fillInStackTrace());
                System.out.println("After this exception the programm is still on!");
            } catch (Exception e) {
                // checking data of created above exception
                if (e.toString().contains("outOf10")) {
                    System.out.println("inserted exception finded, you number " + inpInt + "more than 10");
                }
                // created by handle exception was not founded
                else {
                    System.out.println("Unexceptable exception, reson: " + e.fillInStackTrace());
                    System.out.println("After this exception the programm is still on!");
                }
            }
            // retyping the title
            finally {
                System.out.println("You inputed: " + inpStr);
                System.out.println("You can try again or press \"exit\" to finish");
                System.out.println("Input any NUMBER to get ArithmeticException");
                System.out.println("Input any STRING data type for NumberFormatException");
                System.out.println("Input any NUMBER GREATER THAN 10 to get Inserted Exception");
                inpStr = input.nextLine();
            }
        }
        System.out.println("See you");
    }

}
