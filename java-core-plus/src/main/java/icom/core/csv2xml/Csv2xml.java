package icom.core.csv2xml;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.ArrayList;

public class Csv2xml {


    public static void main(String[] args) throws IOException, ParserConfigurationException, TransformerException {
        String path = "src\\main\\java\\icom\\CORE\\Csv2xml\\source.csv";
        String inputData = new LinesReaderCsv().readLineFromFile(path);
//        System.out.println( inputData );
        ArrayList linesCsvData = new LinesReaderCsv().splitDataAtLines(inputData);
//        System.out.println(linesCsvData);

        ArrayList usersHeader = new UsersData().getHeader(linesCsvData, ";", true);
//        System.out.println(usersHeader);
        ArrayList usersData = new UsersData().getUsersData(linesCsvData, true, ";");
        System.out.println(((UsersData.T) usersData.get(0)).getUserData().toString());

        new CreatorXML().saveData2XmlJAXP(usersData);


//        System.out.println(new PlaceTags("firstName").addTags("Nikita"));
//        System.out.println(new PlaceTags("surName").addTags("Ivanov"));
    }
}
