package icom.core.csv2xml;

import java.util.ArrayList;

public class UsersData {
    private ArrayList headerList = new ArrayList();
    private ArrayList<T> usersData = new ArrayList();
    private ArrayList headerListTypes = new ArrayList();
    private String spliterator;
    public static final String name = "name";
    public static final String surName = "name";
    public static final String age = "age";

    UsersData() {
    }

    public ArrayList getHeaderListTypes() {
        return headerListTypes;
    }

    ArrayList<T> getUsersData(ArrayList inputLinesData, boolean hasHeader, String spliterator) {
        this.spliterator = spliterator;
        ArrayList<T> usersData = new ArrayList<>();
        int startCounter = 0;
        if (hasHeader) startCounter = 1;
        for (int countLines = startCounter; countLines < inputLinesData.size(); countLines++) {
            T objectAdd = new T();
            String[] userDataAtLine = inputLinesData.get(countLines).toString().split(spliterator);
//            System.out.println(Arrays.toString(userDataAtLine));
            objectAdd.setUserDataFromMass(userDataAtLine);
//            System.out.println(objectAdd.getUserData());
            usersData.add(objectAdd);
        }
//        System.out.println(usersData.toString());
        return usersData;
    }

    ArrayList getHeader(ArrayList inputData, String splitter, boolean hasHeader) {
        ArrayList headerList = new ArrayList();
        if (hasHeader) {
            String[] header = inputData.get(0).toString().split(splitter);
            for (int countHeadNumber = 0; countHeadNumber < (header.length); countHeadNumber++) {
                headerList.add(header[countHeadNumber].trim());
            }
            this.headerList = headerList;
            return headerList;
        } else return null;
    }

    private String findDataType(String inputData) {
        String headType;
        try {
            Integer.parseInt(inputData.trim());
            headType = "Integer";
        } catch (Exception e) {
            try {
                inputData.trim();
                headType = "String";
            } catch (Exception e1) {
                headType = "Object";
            }
        }
        return headType;
    }

    public class T {
        private String name;
        private String lastName;
        private int age;
        private ArrayList userData = new ArrayList();

        public T() {
        }

        ArrayList setUserDataFromMass(String[] userDataAtLine) {
            String[] massUserData = userDataAtLine;
            for (String data : massUserData) {
                userData.add(data);
            }
//            System.out.println("User from line: " + userData);
            return userData;
        }

        ArrayList getUserData() {
            return userData;
        }
    }
}
