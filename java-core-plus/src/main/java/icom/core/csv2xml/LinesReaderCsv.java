package icom.core.csv2xml;

import java.io.*;
import java.util.ArrayList;

class LinesReaderCsv {

    LinesReaderCsv() {

    }

    String readLineFromFile(String path) {
        StringBuilder dataFromCSV = new StringBuilder();
        ArrayList outputList = new ArrayList();
        try {
            FileInputStream fileInputStream = new FileInputStream(path);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            while (bufferedInputStream.available() > 0) {
                char c = (char) bufferedInputStream.read();
//                System.out.println(c);
                if ((c != 13) & (c != 10)) dataFromCSV.append(String.valueOf(c));
                else {
                    if (c == 10) dataFromCSV.append("NL");
                    if (c == 13) dataFromCSV.append("NL");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataFromCSV.toString();
    }

    ArrayList splitDataAtLines(String dataFromCSV) {
        ArrayList outputLines = new ArrayList();
        String [] temp;
        String [] tempSplitted;
        temp = dataFromCSV.split("NLNL"); //FOR DIFFERENT OS
        for (int countLines = 0; countLines < temp.length; countLines++) {
            tempSplitted = temp[countLines].split("NL");
            for (int countSplitted = 0; countSplitted < tempSplitted.length; countSplitted++) {
                outputLines.add(tempSplitted[countSplitted]);
//                System.out.println(tempSplitted[countSplitted]);
            }
//            System.out.println( temp[countLines]);
        }
//        System.out.println(temp.length);
        return outputLines;
    }
}
