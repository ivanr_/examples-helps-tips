package icom.core.csv2xml;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CreatorXML {

    CreatorXML() {
    }

    private String getHead() {
        return "<?xml version=\"1.1\" encoding=\"UTF-8\" ?>" + "\n";
    }

    private String getDateOfFile() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd hh-mm-ss");
        return ("[" + sdf.format(date) + "]");
    }

    public void saveUsersAtXML(ArrayList usersData) {

        String xmlOutputPath = "src\\main\\java\\icom\\CORE\\Csv2xml" + getDateOfFile() + ".xml";
        File xml = new File(xmlOutputPath);

        String outputData = "";
        outputData += getHead();

        System.out.println(outputData);

        try (FileOutputStream fileOutputStream = new FileOutputStream(xml)) {
            for (int counter = 0; counter < outputData.length() - 1; counter++) {
                fileOutputStream.write(outputData.charAt(counter));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void saveData2XmlJAXP(ArrayList usersData) throws ParserConfigurationException, TransformerException, FileNotFoundException {
        File outXml = new File("src\\main\\java\\icom\\CORE\\Csv2xml\\" + getDateOfFile() + ".xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation impl = builder.getDOMImplementation();
        Document doc = impl.createDocument(
                null, // namespaceURI
                null, // qualifiedName
                null); // doctype
        Element e0 = doc.createElement("users");
        Element e1 = doc.createElement("user");
        Element e2 = doc.createElement("name");
        Element e3 = doc.createElement("surName");
        Element e4 = doc.createElement("age");
        doc.appendChild(e0);
        e2.setAttribute("data_type", "String");
        e3.setAttribute("data_type", "String");
        e4.setAttribute("data_type", "int");
        e1.appendChild(e2);
        e1.appendChild(e3);
        e1.appendChild(e4);
        int userCount = 0;
        ArrayList userNode = new ArrayList(usersData.size());
        for (Object userData : usersData) {
            userCount++;
            ArrayList contentUser = ((UsersData.T) userData).getUserData();
            e2.setTextContent((String) contentUser.get(0));
            e3.setTextContent((String) contentUser.get(1));
            e4.setTextContent((String) contentUser.get(2));
            e1.setAttribute("number", String.valueOf(userCount));
            e0.appendChild(e1.cloneNode(true));
        }

        DOMSource source = new DOMSource(doc);

        FileOutputStream fileOutputStream = new FileOutputStream(outXml);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);

        StreamResult resultConsole = new StreamResult(System.out);
        StreamResult resultFile = new StreamResult(outputStreamWriter);
        TransformerFactory transFactory = TransformerFactory.newInstance(); // Об этом подробней в 4 вопросе
        Transformer transformer = transFactory.newTransformer();
        transformer.transform(source, resultFile);
        transformer.transform(source, resultConsole);
    }
}
