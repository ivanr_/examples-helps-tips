package icom.core.csv.store;

import java.io.*;
import java.util.ArrayList;

class Backup {
    private String fs = System.getProperty("file.separator");
    private ArrayList<UsersData.T> usersData = new ArrayList<>();
    private ArrayList headerList = new ArrayList();
    private UsersData usersTable = new UsersData();
    private String separator = "";

    Backup(UsersData usersData) {
        this.usersTable = usersData;
        this.headerList = usersTable.getHeaderList();
        this.usersData = usersTable.getUsersData();
    }

    private String getStoredData(String separator) throws IOException {
        ArrayList userData = new ArrayList();
        StringBuilder result = new StringBuilder();
        this.separator = separator;
        if (separator.equals("")) separator = ";";
        result.append(headerList.get(headerList.size() - 1)).append(separator);
        for (int countHead = 0; countHead < headerList.size() - 1; countHead++) {
            result.append(((String) headerList.get(countHead)).trim());
            result.append(separator);
        }
        result = new StringBuilder(result.substring(0, result.length() - 1) + "\n");
        for (int countUsers = 0; countUsers < usersData.size(); countUsers++) {
            userData = usersData.get(countUsers).getUserData();
            result.append(((String) userData.get(userData.size() - 1)).trim());
            result.append(separator);
            for (int dataAtUser = 0; dataAtUser < userData.size() - 1; dataAtUser++) {
                result.append(((String) userData.get(dataAtUser)).trim());
                result.append(separator);
//                System.out.println(userData.get(dataAtUser).toString());
                result = new StringBuilder(result.toString().trim());
            }
            result = new StringBuilder(result.substring(0, result.length() - 1));
            result.append("\n");
        }
//        System.out.println(result);
        return result.toString();
    }

    void save2Store(String path) throws IOException {
        String data2Save = getStoredData(separator);

        Writer outStream = null;
        try {
            outStream = new OutputStreamWriter( new FileOutputStream(
                    "src" + fs
                    + "main" + fs
                            + "java" + fs
                            + "icom" + fs
                            + "core" + fs
                            + "CsvStore" + fs
                            + "storage.csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (outStream != null) {
            try (final BufferedWriter bufferedWriter = new BufferedWriter(outStream)) {
                bufferedWriter.write(data2Save);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
