package icom.core.csv.store;

import java.io.*;
import java.util.ArrayList;

public class Restore {
    private String fs = System.getProperty("file.separator");
    private ArrayList<T> usersData = new ArrayList<>();
    private ArrayList headerList = new ArrayList();
    private UsersData usersTable = new UsersData();
    private String spliterator = "";
    private String path = "storage.csv";

    Restore(String path) {
        this.path = path;
    }

    String getFromFile(String pathInput) {
        if (pathInput.equals("")) pathInput = path;
        StringBuilder result = new StringBuilder();
        File file = new File(pathInput);
        try {
            FileInputStream fileInputStream = new FileInputStream(pathInput);
            BufferedInputStream buffer = new BufferedInputStream(fileInputStream);
            while (buffer.available() > 0) {
                char c = (char) buffer.read();
//                if (c != -1) result += String.valueOf(c);
                result.append(String.valueOf(c));
//                System.out.println(c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        System.out.println("Loaded from storage:\n" + result);
        getUsersDataFromStorage(result.toString(), true, ";");
        return result.toString();
    }

    private ArrayList<T> getUsersDataFromStorage(String dataFromFile, boolean hasHeader, String spliterator) {
//        get data from string to Lines (header-user1-user2-...)
        ArrayList outputLines = new ArrayList();
        String[] tempLines;
        String tempSplitted;
        String separator = System.getProperty("line.separator");
        if ((separator != null) && (!separator.equals(""))) separator = "\n";
        tempLines = dataFromFile.split(separator);

        for (int countLines = 0; countLines < tempLines.length; countLines++) {
            tempSplitted = tempLines[countLines];
            outputLines.add(tempSplitted);
        }
        System.out.println("Array with lines from file:\n" + outputLines.toString());
//        get users data from every line
        ArrayList<T> usersData = new ArrayList<>();
        int startCounter = 0;
        if (hasHeader) startCounter = 1;
        for (int countLines = startCounter; countLines < outputLines.size(); countLines++) {
            T objectAdd = new T();
            String[] userDataAtLine = outputLines.get(countLines).toString().split(spliterator);
//            System.out.println(Arrays.toString(userDataAtLine));
            String temp = userDataAtLine[0];
            userDataAtLine[0] = userDataAtLine[userDataAtLine.length-1];
            userDataAtLine[userDataAtLine.length-1] = temp;
            objectAdd.setUserDataFromMass(userDataAtLine);
//            System.out.println("Objects to add at usersData: " + objectAdd.getUserData());
            usersData.add(objectAdd);
        }
        this.usersData = usersData;
//        System.out.println(usersData.toString());
        return usersData;
    }
    public class T {
        private ArrayList userData = new ArrayList();

        public T() {
        }

        ArrayList setUserDataFromMass(String[] userDataAtLine) {
            int countUser = 0;
            for (String data : userDataAtLine) {
                userData.add(data);
            }
//            System.out.println("User from line: " + userData);
            return userData;
        }

        public ArrayList getUserData() {
            return userData;
        }
    }
}
