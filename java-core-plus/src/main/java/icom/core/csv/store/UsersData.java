package icom.core.csv.store;

import java.util.ArrayList;

public class UsersData {
    private ArrayList headerList = new ArrayList();
    private ArrayList<T> usersData = new ArrayList();

    private ArrayList headerListTypes = new ArrayList();

    UsersData() {
    }

    ArrayList<T> getUsersData() {
        return usersData;
    }

    ArrayList getHeaderList() {
        return headerList;
    }

    public ArrayList getHeaderListTypes() {
        return headerListTypes;
    }

    ArrayList<T> getUsersDataFromLines(ArrayList inputLinesData, boolean hasHeader, String spliterator) {
        ArrayList<T> usersData = new ArrayList<>();
        int startCounter = 0;
        if (hasHeader) startCounter = 1;
        for (int countLines = startCounter; countLines < inputLinesData.size(); countLines++) {
            T objectAdd = new T();
            String[] userDataAtLine = inputLinesData.get(countLines).toString().split(spliterator);
//            System.out.println(Arrays.toString(userDataAtLine));
            objectAdd.setUserDataFromMass(userDataAtLine);
//            System.out.println("Objects to add at usersData: " + objectAdd.getUserData());
            usersData.add(objectAdd);
        }
        this.usersData = usersData;
//        System.out.println(usersData.toString());
        return usersData;
    }

    ArrayList getHeader(ArrayList inputData, String splitter, boolean hasHeader) {
        ArrayList headerList = new ArrayList();
        CsvReader reader = new CsvReader();
        if (hasHeader) {
            headerList = reader.getDataFromLine((String) inputData.get(0), "");
            this.headerList = headerList;
            return headerList;
        } else return null;
    }

    private String findDataType(String inputData) {
        String headType;
        try {
            Integer.parseInt(inputData.trim());
            headType = "Integer";
        } catch (Exception e) {
            try {
                inputData.trim();
                headType = "String";
            } catch (Exception e1) {
                headType = "Object";
            }
        }
        return headType;
    }

    public class T {
        private ArrayList userData = new ArrayList();

        public T() {
        }

        ArrayList setUserDataFromMass(String[] userDataAtLine) {
            for (String data : userDataAtLine) {
                userData.add(data);
            }
//            System.out.println("User from line: " + userData);
            return userData;
        }

        ArrayList getUserData() {
            return userData;
        }
    }
}
