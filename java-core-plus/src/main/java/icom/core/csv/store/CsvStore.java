package icom.core.csv.store;

import java.io.IOException;
import java.util.ArrayList;

public class CsvStore {
    public static void main(String[] args) throws IOException {
        String dataFromCsv =
                "Name; Surname; Age; ID\n" +
                        "Petya; Pupkin; 21; 1015\n" +
                        "Zoya; Dubko; 42; 931\n" +
                        "Liza; Blabla; 22; 3277\n" +
                        "Yasha; Kuchkin; 32; 4918";

        CsvReader reader = new CsvReader();
        ArrayList dataInLines;
        dataInLines = reader.splitDataAtLines(dataFromCsv, ";");

        final UsersData usersData = new UsersData();
        ArrayList head =  usersData.getHeader(dataInLines, "", true);
        usersData.getUsersDataFromLines(dataInLines, true, ";");
//        System.out.println("User data: " + usersData.getUsersData().get(0).getUserData().toString());

        Backup userStore = new Backup(usersData);
        userStore.save2Store(";");

        UserSaving userSaving = new UserSaving();
        userSaving.save2Csv(usersData, head, ";", "");

        Restore usersRestored = new Restore("src\\main\\java\\icom\\core\\csvstore\\storage.csv");
        usersRestored.getFromFile("");

        System.out.println(reader.getDataFromLine((String) dataInLines.get(1), ""));

    }
}
