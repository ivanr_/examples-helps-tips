package icom.core.csv.store;

import java.io.*;
import java.util.ArrayList;

public class UserSaving {
    private String separator = ",";
    private ArrayList head = new ArrayList();
    private ArrayList headType = new ArrayList();
    private String fs = System.getProperty("file.separator");

    UserSaving() {
    }

    void save2Csv(UsersData usersData, ArrayList head, String separator, String path) {
        String usersDataString = getUsersDataString(usersData, head, separator);

        Writer outStream = null;
        try {
            outStream = new OutputStreamWriter(new FileOutputStream(
                    "src" + fs
                            + "main" + fs
                            + "java" + fs
                            + "icom" + fs
                            + "core" + fs
                            + "CsvStore" + fs
                            + "savedCsv.csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (outStream != null) {
            try (final BufferedWriter bufferedWriter = new BufferedWriter(outStream)) {
                bufferedWriter.write(usersDataString);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getUsersDataString(UsersData usersData, ArrayList head, String separator) {
        StringBuilder result = new StringBuilder();
        if (separator.equals("")) separator = ";";
        for (int countHead = 0; countHead < head.size(); countHead++) {
            result.append(head.get(countHead)).append(separator);
        }
        result = new StringBuilder(result.substring(0, result.length() - 1) + "\n");

        for (UsersData.T userData : usersData.getUsersData()) {
            for (Object data : userData.getUserData()) {
                result.append(data.toString()).append(separator);
            }
            result = new StringBuilder(result.substring(0, result.length() - 1) + "\n");
        }
        return result.toString();
    }

}
