package icom.core.csv.store;

import java.util.ArrayList;

class CsvReader {

    ArrayList splitDataAtLines(String dataFromCSV, String dataSeparator) {
        ArrayList outputLines = new ArrayList();
        String[] tempLines;
        String tempSplitted;
        String separator = System.getProperty("line.separator");
        if ((separator != null) && (!separator.equals(""))) separator = "\n";
        tempLines = dataFromCSV.split(separator);

        for (int countLines = 0; countLines < tempLines.length; countLines++) {
            tempSplitted = tempLines[countLines];
            outputLines.add(tempSplitted);
        }
        return outputLines;
    }

    ArrayList getDataFromLine(String inputLine, String separator) {
        String[] massLineData;
        ArrayList result = new ArrayList();
        if (separator.equals("")) separator = ";";
        if (!inputLine.contains(";")) separator = ",";
        massLineData = inputLine.split(separator);
        for (String str : massLineData) {
            result.add(str.trim());
        }
//        System.out.println(massLineData.toString());

        return result;
    }
}
