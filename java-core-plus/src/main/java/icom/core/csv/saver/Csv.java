package icom.core.csv.saver;

import java.io.*;
import java.util.ArrayList;

class DataForSave {
    public ArrayList list = new ArrayList();

    DataForSave(Object inputed) {
        list.add(inputed);
    }

    public DataForSave() {
    }

    ArrayList getTable() {
        return list;
    }

    public void setTable(ArrayList list) {
        this.list = list;
    }

    void addID(Object position) {
        list.add(position);
    }
}

class SaverAtCsv {
    void print(ArrayList list) {
        final String encoding = "UTF-8";
//        final String path = "c:\\Users\\PC\\AppData\\Local\\Temp\\";
        File outputFile = new File(new File("").getAbsolutePath()
                + "\\src\\main\\java\\icom\\CORE\\CsvSaver\\output.csv");
        Writer outStream = null;
        try {
            outStream = new OutputStreamWriter( new FileOutputStream(outputFile.getPath()),encoding);
        } catch (UnsupportedEncodingException | FileNotFoundException e) {
            e.printStackTrace();
        }
        String[] temp;
        if (outStream != null) {
            try (final BufferedWriter bufferedWriter = new BufferedWriter(outStream)) {
                for (int counter = 0; counter<list.size(); counter++) {
                    temp = (String[]) list.get(counter);
                    bufferedWriter.write(temp[0]+","+temp[1]+"\n");
                    System.out.println(temp[0]+","+temp[1]);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}

public class Csv {

    public static void main(String[] args) {
        DataForSave employeesPositions = new DataForSave(new String[]{"Vasya", "manager"});
        employeesPositions.addID(new String[]{"Petruha", "coach"});
        employeesPositions.addID(new String[]{"Katya", "saver"});
        employeesPositions.addID(new String[]{"Sasha", "master"});

        ArrayList dataForPrint = employeesPositions.getTable();

//        this data will be inputted at file
//        String[] temp = new String[2];
//        for (int counter = 0; counter < dataForPrint.size(); counter++) {
//            temp = (String[]) dataForPrint.get(counter);
//            System.out.print(temp[0] + ",");
//            System.out.println(temp[1]);
//        }

        SaverAtCsv saverAtCsv =  new SaverAtCsv();
        saverAtCsv.print(dataForPrint);
//        System.out.println(employeesPositions.list);
    }

}
