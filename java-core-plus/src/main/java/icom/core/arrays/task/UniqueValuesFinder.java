package icom.core.arrays.task;

import java.util.Arrays;
import java.util.List;

public class UniqueValuesFinder {
    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList(getArray());

        int uniqueValue;
        for (int i = 0; i < integers.size(); i++) {
            Integer duplicatedValue = integers.get(i);
            if (duplicatedValue != null && isNotLastElement(integers, i)) {
                duplicatedValue = checkFoRepetitionsAndRemoveDuplications(integers, i, duplicatedValue);
            }
            if (duplicatedValue != null){
                uniqueValue = duplicatedValue;
                System.out.println("Unique value: " + uniqueValue);
            }
        }
    }

    private static Integer checkFoRepetitionsAndRemoveDuplications(List<Integer> integers, int i, Integer duplicatedValue) {
        for (int j = i + 1; j < integers.size(); j++) {
            if (duplicatedValue.equals(integers.get(j))) {
                duplicatedValue = null;
                integers.set(j, null);
                break;
            }
        }
        return duplicatedValue;
    }

    private static boolean isNotLastElement(List<Integer> integers, int i) {
        return i != integers.size() - 1;
    }

    static Integer[] getArray() {
        return new Integer[]{3, 4, 2, 3, 3, 3, 4, 5};
    }
}
