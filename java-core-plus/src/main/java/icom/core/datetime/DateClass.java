package icom.core.datetime;

import java.util.Date;

public class DateClass {
    public static void main(String[] args) {
        Date osDate = new Date();
        System.out.println("Current operation system date using Date class:\n" + osDate);

        osDate.setTime(1000_000_000);
        System.out.println("Operation system date in 1000_000_000 ms:\n" + osDate);
    }
}
