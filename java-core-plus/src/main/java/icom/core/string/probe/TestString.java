package icom.core.string.probe;

import java.util.Arrays;
import java.util.Scanner;

public class TestString {
    private void printStringPractice() {
        String mainExpression = "Main" + " expression ";
        System.out.println("Main expression: " + mainExpression);

        char[] charsToAppend = {'w', 'i', 't', 'h', ' ', 'a', 'p', 'p', 'e', 'n', 'd', 'e', 'd', ' ', 'l', 'i', 'n', 'e',};
        System.out.println("Chars to append: " + Arrays.toString(charsToAppend));

        String appendExpr = new String(charsToAppend);
        String appendedExpression = new StringBuffer(mainExpression).append(appendExpr).toString();
        System.out.println("Appended expression: " + appendedExpression);

        String replaced = appendedExpression.replace('l', 'f');
        System.out.println("Replaced l->f: " + replaced);

        String substringExpession = replaced.substring(5, 15);
        System.out.println("Substring from string: " + substringExpession);

        String calcExpression = substringExpession + "\n +calculated (2+2):" + (2 + 2) + "\n +added 5+3:" + 5 + 3;
        System.out.println(calcExpression);

        String copyOfSubstr = "expression";
        System.out.print(copyOfSubstr + " == " + substringExpession + "->");
        System.out.println(copyOfSubstr.equals(substringExpession));
        System.out.print(copyOfSubstr + ".equals " + substringExpession + "->");
        System.out.println(copyOfSubstr.equals(substringExpession));
        System.out.println("LastIndexOf 'e' in Appended expression to 15th char is " + appendedExpression.lastIndexOf('e', 15));
        System.out.println("LastIndexOf 'e' in Appended expression to 7th char is " + appendedExpression.lastIndexOf('e', 7));
        String reversedExpression = new StringBuffer(mainExpression).reverse().toString();
        System.out.println("Reversed main expression: " + reversedExpression);
        System.out.println("Main expression toLowerCase: " + mainExpression.toLowerCase());
        System.out.println("Main expression toUpperCase: " + mainExpression.toUpperCase());
    }

    public static void main(String[] args) {
        TestString test = new TestString();
        test.printStringPractice();

        String s1 = "WooHoo";
        String s2 = "Hello";
        String s3 = "abcdef";
        System.out.println(s1.substring(0, s1.length() / 2) + "/" + s2.substring(0, s2.length() / 2) + "/" + s3.substring(0, s3.length() / 2));

        String[] s = new String[]{s1, s2, s3};
        String endChars = "lo";
        for (String countS : s) {
            if (countS.contains(endChars) && countS.lastIndexOf(endChars) == countS.length() - endChars.length()) {
                System.out.println("Chars " + countS);
            }
        }
        String inputLine = "";
        Scanner scan = new Scanner(System.in);
        String[] list = new String[0];
        boolean ascending = false;
        while (!inputLine.equalsIgnoreCase("exit")) {
            System.out.print("Enter a sentence (print \"exit\" to out) to output and sort chars length \n");
            inputLine = scan.nextLine();
//            System.out.println(inputLine);
            if (inputLine.equals("exit")) break;
            if (list.length == 0) {
                list = new String[]{inputLine};
            } else {
                list = test.addAndSortAscendingMass(list, inputLine, ascending);
            }
            for (String value : list) {
                System.out.println(value);
            }
        }
    }

    public String[] addAndSortAscendingMass(String[] inputMass, String inputData, boolean ascending) {
        if (inputMass.length == 0) return null;
        int indexForInsert = 0;
        int lengthOfMassElement = 0;
        for (int countMass = 0; countMass < inputMass.length; countMass++) {
            lengthOfMassElement = inputMass[countMass].length();
            if ((lengthOfMassElement > inputData.length()) & ascending) {
                indexForInsert = countMass;
                break;
            } else {
                if ((lengthOfMassElement < inputData.length()) & !ascending) {
                    indexForInsert = countMass;
                    break;
                } else {
                    indexForInsert = countMass + 1;
                }

            }
        }
        String[] result = new String[inputMass.length + 1];
        if (indexForInsert >= 0) {
            System.arraycopy(inputMass, 0, result, 0, indexForInsert);
        }
        result[indexForInsert] = inputData;
        if (result.length - indexForInsert + 1 >= 0)
            System.arraycopy(inputMass, indexForInsert + 1 - 1, result, indexForInsert + 1, result.length - indexForInsert + 1);
        return result;
    }
}
