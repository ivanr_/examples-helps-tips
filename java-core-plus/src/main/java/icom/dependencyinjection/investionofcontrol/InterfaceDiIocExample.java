package icom.dependencyinjection.investionofcontrol;

import java.util.ArrayList;

public class InterfaceDiIocExample implements InterfaceToInject {

    public String getQuery(String path, int id) {
        ArrayList data = build().getData();
        String result = "";
        try {
            System.out.println("returned path: " + path + " and data: " + data.get(id));
            result = path + data.get(id).toString();
        } catch (Exception e) {
            System.out.println("Got exception from IoC setter method:  " + e.getMessage());
        }
        return result;
    }
}
