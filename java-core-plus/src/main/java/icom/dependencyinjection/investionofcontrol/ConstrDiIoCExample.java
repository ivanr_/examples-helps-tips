package icom.dependencyinjection.investionofcontrol;

import icom.dependencyinjection.factorymethod.DiDbFactory;

import java.util.ArrayList;

public class ConstrDiIoCExample {
    private DiDbFactory diDbFactory;

    public ConstrDiIoCExample(DiDbFactory dbFactoryDb) {
        this.diDbFactory = dbFactoryDb;
    }

    public String getQuery(String path, int id) {
        ArrayList data = diDbFactory.build().getData();
        String result = "";
        try {
            System.out.println("returned path: " + path + " and data: " + data.get(id));
            result = path + data.get(id).toString();
        } catch (Exception e) {
            System.out.println("Got exception:  " + e.getMessage());
        }
        return result;
    }
}
