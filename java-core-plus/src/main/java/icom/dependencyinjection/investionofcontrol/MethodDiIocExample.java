package icom.dependencyinjection.investionofcontrol;

import icom.dependencyinjection.Db;
import icom.dependencyinjection.factorymethod.DiDbFactory;

import java.util.ArrayList;

public class MethodDiIocExample {
    public MethodDiIocExample() {
    }

    public String getQuery(String path, int id) {
        ArrayList data = setDb(Db::new).getData();
        String result = "";
        try {
            System.out.println("returned path: " + path + " and data: " + data.get(id));
            result = path + data.get(id).toString();
        } catch (Exception e) {
            System.out.println("Got exception from IoC setter method:  " + e.getMessage());
        }
        return result;
    }

    private Db setDb(DiDbFactory interfaceFactory) {
        return interfaceFactory.build();
    }
}

