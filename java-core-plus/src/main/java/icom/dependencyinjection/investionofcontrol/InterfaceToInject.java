package icom.dependencyinjection.investionofcontrol;

import icom.dependencyinjection.Db;

public interface InterfaceToInject {
    default Db build() {return new Db();}
}
