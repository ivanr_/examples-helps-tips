package icom.dependencyinjection;

import java.util.ArrayList;

public class BadExampleDbQuery {
    public String getQuery(String path, int id) {
        Db myDb = new Db();
        ArrayList data = myDb.getData();
        String result = "";
        try {
            System.out.println("returned path: " + path + " and data: " + data.get(id));
             result = path + data.get(id).toString();
        } catch (Exception e) {
            System.out.println("Got exception:  " + e.getMessage());
        }
        return result;
    }
}
