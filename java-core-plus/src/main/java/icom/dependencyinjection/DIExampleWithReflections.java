package icom.dependencyinjection;

import icom.dependencyinjection.factorymethod.FactoryIoCExampleDbQuery;
import icom.dependencyinjection.investionofcontrol.ConstrDiIoCExample;
import icom.dependencyinjection.investionofcontrol.InterfaceDiIocExample;
import icom.dependencyinjection.investionofcontrol.MethodDiIocExample;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DIExampleWithReflections {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Method badMethod = BadExampleDbQuery.class.getMethod("getQuery", String.class, int.class);
        Method factoryIoCMethod = FactoryIoCExampleDbQuery.class.getMethod("getQuery", String.class, int.class);
        Method constrDiIoCMethod = ConstrDiIoCExample.class.getMethod("getQuery", String.class, int.class);
        Method setterDiIocMethod = MethodDiIocExample.class.getMethod("getQuery", String.class, int.class);
        Method interfaceDiIoCMethod = InterfaceDiIocExample.class.getMethod("getQuery", String.class, int.class);

        badMethod.invoke(new BadExampleDbQuery(), new Object[]{"bad example string path", 0});
        factoryIoCMethod.invoke(new FactoryIoCExampleDbQuery(), new Object[]{"factory DI query string", 0});
        constrDiIoCMethod.invoke(new ConstrDiIoCExample(Db::new), new Object[]{"constructor IoC query string", 0});
        setterDiIocMethod.invoke(new MethodDiIocExample(),  new Object[] {"setter IoC query string", 0});
        interfaceDiIoCMethod.invoke(new InterfaceDiIocExample(),new Object[] {"interface IoC query string", 0});
    }
}

