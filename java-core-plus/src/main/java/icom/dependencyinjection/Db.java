package icom.dependencyinjection;

import java.util.ArrayList;

public class Db {
    private ArrayList data;

    public Db() {
    }

    public Db(ArrayList data) {
        this.data = data;
    }

    public ArrayList getData() {
        if (data == null) {
            data = new ArrayList();
            data.add("database first record");
        }
        return data;
    }
}
