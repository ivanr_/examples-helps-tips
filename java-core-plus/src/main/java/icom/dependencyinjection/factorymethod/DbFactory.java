package icom.dependencyinjection.factorymethod;

import icom.dependencyinjection.Db;

public abstract class DbFactory {
    abstract public Db build();
}
