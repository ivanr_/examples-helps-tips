package icom.dependencyinjection.factorymethod;

import icom.dependencyinjection.Db;

public class FactoryIoCExampleDbQuery extends InitDbFactory {

    public String getQuery(String path, int id) {
        Db myDb = build();
        String result;
        try {
            System.out.println("returned path: " + path + " and data: " + myDb.getData().get(id));
            result = path + myDb.getData().get(id).toString();
        } catch (Exception e) {
            result = e.getMessage();
        }
        return result;
    }
}
