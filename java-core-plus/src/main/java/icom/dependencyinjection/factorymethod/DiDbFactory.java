package icom.dependencyinjection.factorymethod;

import icom.dependencyinjection.Db;

public interface DiDbFactory {
    public Db build();
}
