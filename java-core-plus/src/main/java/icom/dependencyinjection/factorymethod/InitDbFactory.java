package icom.dependencyinjection.factorymethod;

import icom.dependencyinjection.Db;

public class InitDbFactory extends DbFactory {

    @Override
    public Db build() {
        return new Db();
    }
}
