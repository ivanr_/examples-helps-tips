package icom.dependencyinjection;

import icom.dependencyinjection.factorymethod.DiDbFactory;
import icom.dependencyinjection.factorymethod.FactoryIoCExampleDbQuery;
import icom.dependencyinjection.investionofcontrol.ConstrDiIoCExample;
import icom.dependencyinjection.investionofcontrol.InterfaceDiIocExample;
import icom.dependencyinjection.investionofcontrol.MethodDiIocExample;

public class DiExample {
    public static void main(String[] args) {
        FactoryIoCExampleDbQuery factoryDI = new FactoryIoCExampleDbQuery();
        ConstrDiIoCExample constructorDI = new ConstrDiIoCExample(new DiDbFactory() {
            @Override
            public Db build() {
                return new Db();
            }
        });
        InterfaceDiIocExample interfaceDI = new InterfaceDiIocExample();
        MethodDiIocExample methodDI = new MethodDiIocExample();

        factoryDI.getQuery("factory DI query string", 0);
        constructorDI.getQuery("constructor IoC query string", 0);
        interfaceDI.getQuery("interface IoC query string", 0);
        methodDI.getQuery("setter IoC query string", 0);
    }
}
