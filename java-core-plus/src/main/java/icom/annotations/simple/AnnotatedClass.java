package icom.annotations.simple;

@ClassAnnotation(packageNumber = 2)
@MarkerAnnotation
public class AnnotatedClass {
    @FieldAnnotation(name = "cash vault", cash = 5)
    private int privateField1;
    @FieldAnnotation(name = "limit", cash = 120)
    private String privateField2 = "default contain of private field 2";
    private String privateField3 = "";

    @MethodAnnotation(user = UserType.ADMINISTRATOR)
    public String appendFileds23() {
        return this.privateField2 + this.privateField3;
    }
//    @MethodAnnotation
    public void methodWithoutAnnotations() {
    }
}
