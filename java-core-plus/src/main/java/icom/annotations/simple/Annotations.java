package icom.annotations.simple;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Annotations {
    static void getClassAnnotations() throws NoSuchMethodException {
        ClassAnnotation annotations = AnnotatedClass.class.getAnnotation(ClassAnnotation.class);
        Class annotDefault = ClassAnnotation.class;
        Method defMeth = annotDefault.getMethod("packageNumber");
        System.out.println("Class annotations:");
        System.out.println("\tNumber of package: " + annotations.packageNumber() + ", default is " + defMeth.getDefaultValue());
    }

    private static void getMethodsAnnotations() {
        Class<? extends AnnotatedClass> cl = AnnotatedClass.class;
        Method[] methods = AnnotatedClass.class.getDeclaredMethods();
        System.out.println("Methods annotations:");
        for (Method method : methods) {
            method.setAccessible(true);
            MethodAnnotation annotations = method.getDeclaredAnnotation(MethodAnnotation.class);
            System.out.println("\tName of the method: " + method.getName() + " has such annotations");
            if (method.isAnnotationPresent(MethodAnnotation.class)) {
                System.out.println("\t\tValues of the annotation: " + annotations.user().toString());
            } else {
                System.out.println("\t\tMethod have no annotation: " + MethodAnnotation.class.getTypeName());
            }
//            System.out.println("breakpoint");
        }
    }

    private static void getFieldsAnnotations() {
        Class<? extends AnnotatedClass> cl = AnnotatedClass.class;
        Field[] fieldsFromAnnotatedClass = AnnotatedClass.class.getDeclaredFields();
        Field[] annotFieldsDefault = FieldAnnotation.class.getDeclaredFields();

        System.out.println("Fields annotations:");
        for (Field field : fieldsFromAnnotatedClass) {
            field.setAccessible(true);
            FieldAnnotation annotations = field.getDeclaredAnnotation(FieldAnnotation.class);
            System.out.println("\tName of the field: " + field.getName());
            if (field.isAnnotationPresent(FieldAnnotation.class)) {
                System.out.println("\t\tValues of the annotation: " + annotations.name() + annotations.cash());
            } else {
                System.out.println("\t\tField have no annotation: " + FieldAnnotation.class.getTypeName());
            }
        }
    }

    public static void main(String[] args) throws NoSuchMethodException {


        getClassAnnotations();
        getMethodsAnnotations();
        getFieldsAnnotations();

    }
}
