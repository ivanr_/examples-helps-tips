package icom.annotations.simple;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

//@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface MethodAnnotation {
    UserType user() default UserType.USER;
}
