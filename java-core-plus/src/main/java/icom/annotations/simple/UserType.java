package icom.annotations.simple;

public enum UserType {
    ADMINISTRATOR, BUILDER, USER
}
