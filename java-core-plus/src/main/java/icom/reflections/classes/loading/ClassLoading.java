package icom.reflections.classes.loading;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ClassLoading {

    public static void main(String[] args) throws ClassNotFoundException, MalformedURLException {
        loadClassFromFolder();
        System.out.println("breakpoint");
    }

    static void loadClassFromFolder() throws ClassNotFoundException, MalformedURLException {
        String pathOfClass = "icom\\Reflections\\ClassLoading\\DataModule.class";
        String pathOfClassToList = System.getProperty("user.dir")
                + "\\src\\main\\java\\icom\\Reflections\\ClassLoading\\ClassFolder\\MyModule.class";
        String path = System.getProperty("user.dir")
                + "\\src\\main\\java\\icom\\Reflections\\ClassLoading\\ClassFolder\\DataModuleCompilled.class";

        System.out.println("Class: " + ClassLoader
                .getSystemResource(pathOfClassToList)
                + "\n\t file path: "
                + pathOfClassToList
                + "\n file exist: "
                + (new File(pathOfClassToList).exists()));
        URL url = new File(pathOfClassToList).toURI().toURL();
        URL[] urls = new URL[]{url};
        ClassLoader classLoader = new URLClassLoader(urls);
        Class myClass = classLoader.loadClass(pathOfClassToList);

        ArrayList list = Stream
                .of(new File(pathOfClassToList))
                .map(File::exists)
                .collect(Collectors.toCollection(ArrayList::new));
        System.out.println("breakpoint");
    }

    void loadSingleClass() throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class DataModuleLoaded = ClassLoader
                .getSystemResource("icom.Reflections.ClassLoading.DataModuleCompilled.class")
                .getClass()
                .getClassLoader()
                .loadClass("icom.reflections.ClassLoading.DataModuleCompilled.class");

        DataModule moduleFromClassLoader = (DataModule) DataModuleLoaded.getDeclaredConstructor().newInstance();
        Class clazz = Class.forName(String.valueOf(DataModuleLoaded));
        DataModule moduleFromReflection = (DataModule) clazz.getDeclaredConstructor().newInstance();

        DataModule dataModuleFromNew = new DataModule();

        DataModule.class.getMethod("showText").invoke(moduleFromClassLoader);
        DataModule.class.getMethod("showText").invoke(moduleFromReflection);
        DataModule.class.getMethod("showText").invoke(dataModuleFromNew);
    }


}


