package icom.reflections.classes.analizing;

public class Reflections {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        ReflectionsInfoClass myReflection = new ReflectionsInfoClass();
        MyClassForReflect reflect = new MyClassForReflect();

        myReflection.LoadHashMapClass(); //practice #1
        myReflection.FieldsAndMethodsArrayList(); //practice #2
        myReflection.ChangeFieldType(reflect); //practice #3
        System.out.println("Trying to get class cast exception");
        myReflection.getClassCastException(); //practice #4
    }
}
