package icom.reflections.classes.analizing;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class ReflectionsInfoClass {
    Object hashMapClass = null;
    Object map = null;

    public void LoadHashMapClass() {
        Class<? extends Object> hashMapClass = HashMap.class;//получаем класс
        this.hashMapClass = hashMapClass;
        if (hashMapClass.getName() == "java.util.HashMap") {
            System.out.println("Class name of hashMapClass is " + hashMapClass.getName());
            try {
                HashMap map = (HashMap) hashMapClass.getConstructor().newInstance();
                this.map = map;
            } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Class name of hashMapClass is not HashMap. It names: " + hashMapClass.getName());
        }
        System.out.println("Class " + hashMapClass.getName() + " has " + hashMapClass.getConstructors().length + " constructors");
        System.out.println("One of constructors is: " + hashMapClass.getConstructors()[0]);
        System.out.println("Parent class for hashMapClass is: " + hashMapClass.getSuperclass());
        System.out.println(hashMapClass.getName() + " has " + hashMapClass.getAnnotatedInterfaces().length + " such annotated interfaces as:");
        Object[] massOfAnnotInterf = hashMapClass.getAnnotatedInterfaces();
        Object[] massOfGenericInterf = hashMapClass.getGenericInterfaces();
        for (Object output : massOfAnnotInterf) {
            System.out.println("\t" + output);
        }
        System.out.println(hashMapClass.getName() + " has " + hashMapClass.getGenericInterfaces().length + " such generic interfaces as:");
        for (Object output : massOfGenericInterf) {
            System.out.println("\t" + output);
        }
    }

    public void FieldsAndMethodsArrayList() {
        Class arraylistClass = ArrayList.class;

        Field[] openedFields = arraylistClass.getFields();
        Method[] openedMethods = arraylistClass.getMethods();
        System.out.println(arraylistClass.getName() + " has such number of opened (public) fields: " + openedFields.length);
        System.out.println(arraylistClass.getName() + " has such number of opened (public) methods: " + openedMethods.length);

        System.out.println("\nOpened fields from " + arraylistClass.getName() + " class: ");
        for (Object output : openedFields) {
            System.out.println("\t" + output);
        }
        System.out.println("\nOpened methods from " + arraylistClass.getName() + " class: ");
        for (Object output : openedMethods) {
            System.out.println("\t" + output);
        }

        Field[] allFields = arraylistClass.getDeclaredFields();
        Method[] allMethods = arraylistClass.getDeclaredMethods();
        System.out.println(arraylistClass.getName() + " has such number of declared fields: " + allFields.length);
        System.out.println(arraylistClass.getName() + " has such number of declared methods: " + allMethods.length);

        System.out.println("\nPrivated fields from " + arraylistClass.getName() + " class: ");
        for (Object myfield : allFields) {
            boolean isOpen = false;
            for (Object openedField : openedFields) {
                if (myfield.equals(openedField)) {
                    isOpen = true;
                }
            }
            if (!isOpen) System.out.println("\t" + myfield);
        }
        System.out.println("\nPrivated methods from " + arraylistClass.getName() + " class: ");
        for (Object myMethod : allMethods) {
            boolean isOpen = false;
            for (Object openedMethod : openedMethods) {
                if (myMethod.equals(openedMethod)) {
                    isOpen = true;
                }
            }
            if (!isOpen) System.out.println("\t" + myMethod);
        }
    }

    public void ChangeFieldType(Object objectClassMayBeNull) throws NoSuchFieldException, IllegalAccessException {
        Class objClass;
        if (objectClassMayBeNull != null) {
            objClass = objectClassMayBeNull.getClass();
        } else {
            objClass = MyClassForReflect.class;
        }
        Field[] openedFields = objClass.getFields();
        System.out.println("All opened fields:");
        for (Field field : openedFields) {
            System.out.println("\t" + field);
        }

        Field[] allFields = objClass.getDeclaredFields();
        System.out.println("All declared fields:");
        for (int count = 0; count < allFields.length; count++) {
            System.out.println("\t" + allFields[count]);
            allFields[count].setAccessible(true);
        }

//        Field privateField = objClass.getDeclaredField("publicStr");
//        privateField.setAccessible(true);
//        System.out.println("Was privated " + privateField.getDec(objClass));

        Field[] allReopenedFields = objClass.getFields();
        System.out.println("Reopened fields: ");
        for (Field field : allReopenedFields) {
            System.out.println("\t" + field);
        }
    }

    public void getClassCastException() {
        Class hashMapClass = HashMap.class;
        try {
            ArrayList map = (ArrayList) hashMapClass.getConstructor().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (ClassCastException e) {
            System.out.println("ooops, it's class cast exception");
            e.printStackTrace();
        }

    }
}
