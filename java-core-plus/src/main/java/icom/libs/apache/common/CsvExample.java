package icom.libs.apache.common;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import java.io.*;

public class CsvExample {
    public static void main(String[] args) throws IOException {
        File csvFile = new File("tempFile.csv");
        System.out.println("csv file created: " + csvFile.createNewFile());
        FileOutputStream fos = new FileOutputStream(csvFile.getName());
        fos.write(("Last Name,First Name" + System.lineSeparator()).getBytes());
        fos.write("test last name value,test first name value".getBytes());
        fos.close();
        System.out.println("csv file exists: " + csvFile.exists());

        Reader in = new FileReader(csvFile.getName());
        CSVParser records = CSVFormat.DEFAULT
                .withHeader()
                .parse(in);
        System.out.println("records:");
        records.getRecords().forEach(record ->
                records.getHeaderNames()
                        .forEach(headerName -> System.out.println(headerName + ":" + record.get(headerName))));

        System.out.println("file deleted: " + csvFile.delete());
    }
}
