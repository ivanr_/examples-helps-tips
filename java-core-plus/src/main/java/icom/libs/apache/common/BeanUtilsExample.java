package icom.libs.apache.common;

import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class BeanUtilsExample {
    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        runCloneExample();

        runBeanDescriptionExample();

        runBeanPropertiesCopyExample();

        runBeanPropertiesAsArrayExample();
    }

    private static void runBeanPropertiesAsArrayExample() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        System.out.println("----- array property example-----\nresult of field by provided name");
        BeanForClone bean = new BeanForClone();
        Arrays.stream(BeanUtils.getArrayProperty(bean, "textField")).forEach(System.out::println);
    }

    private static void runBeanPropertiesCopyExample() throws IllegalAccessException, InvocationTargetException {
        System.out.println("----- copy bean props example----- ");
        BeanForClone bean1 = new BeanForClone();
        bean1.id = 100;
        BeanForClone bean2 = new BeanForClone();
        System.out.println("bean1: " + bean1);
        bean1.setTextField("bean1");
        bean1.id = 100L;
        System.out.println("bean2 before properties copying from bean1: " + bean2);
        BeanUtils.copyProperties(bean2, bean1);
        System.out.println("bean2 after properties copying from bean1: " + bean2);
        System.out.println("NOTE: public fields are NOT copied");
    }

    private static void runBeanDescriptionExample() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        System.out.println("-----bean description example-----");
        System.out.println(BeanUtils.describe(new BeanForClone()).toString());
    }

    private static void runCloneExample() throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        System.out.println("-----bean clone example-----");
        BeanForClone bean1 = new BeanForClone();
        Object bean2 = BeanUtils.cloneBean(bean1);
        System.out.println("Bean2 is the instance of " + BeanForClone.class.getSimpleName() + ": " + (bean2 instanceof BeanForClone));
        System.out.println("Bean1 and Bean2 are equals: " + bean1.equals(bean2));
    }

    public static class BeanForClone {
        private String textField = "init value";
        public long id = 10L;

        @SuppressWarnings("unused") //used by bean utils
        public String getTextField() {
            return textField;
        }

        public void setTextField(String textField) {
            this.textField = textField;
        }

        @Override
        public String toString() {
            return "BeanForClone{" +
                    "textField='" + textField + '\'' +
                    ", id=" + id +
                    '}';
        }
    }
}
