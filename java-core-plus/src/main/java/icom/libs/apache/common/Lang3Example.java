package icom.libs.apache.common;


import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

public class Lang3Example {
    public static void main(String[] args) throws IOException, InterruptedException {
        runAllBlankExample();
        runAbbreviationExample();
        runSubstringExample();
        runCenterExample();
        runContainsAnyExample();
        runDeleteWhitespacesExample();
        runAllLowercaseExample();
        runAlfanumericExample();
        runStringJoinExample();

    }

    private static void runStringJoinExample() {
        System.out.println("-----string join example-----");
        System.out.println("joined\t'a'+' '+'b'+'c'=" + StringUtils.join("a", " ", "b", "c"));
        System.out.println("joined with comma separator\t'1'+'2'+'3'+'4'=" + StringUtils.join(new int[]{1, 2, 3, 4}, ','));
        System.out.println("joined with column separator\t'a'+' '+'b'+'c'=" + StringUtils.join(new String[]{"a", " ", "b", "c"}, ':'));
    }

    private static void runAlfanumericExample() {
        System.out.println("-----alfanumeric example-----");
        System.out.println("'as34' is alfanumeric: " + StringUtils.isAlphanumeric("as34"));
        System.out.println("'as 34' is alfanumeric: " + StringUtils.isAlphanumeric("as 34"));
        System.out.println("'as-34' is alfanumeric: " + StringUtils.isAlphanumeric("as-34"));
    }

    private static void runAllLowercaseExample() {
        System.out.println("-----all lower case example-----");
        System.out.println("'asd' is in lower case: " + StringUtils.isAllLowerCase("asd"));
        System.out.println("'asD' is in lower case: " + StringUtils.isAllLowerCase("asD"));
        System.out.println("'asd1' is in lower case: " + StringUtils.isAllLowerCase("asd1"));
    }

    private static void runDeleteWhitespacesExample() {
        System.out.println("-----deleteWhitespace example-----");
        System.out.println("removed whitespaces from ' 123 321 ':\n" + StringUtils.deleteWhitespace(" 123 321 "));
    }

    private static void runContainsAnyExample() {
        System.out.println("-----contains any example-----");
        System.out.println("12345 containsAny 15 or 16: " + StringUtils.containsAny("12345", "16", "15"));
        System.out.println("12345 containsAny 15 or 1234: " + StringUtils.containsAny("12345", "16", "1234"));
    }

    private static void runCenterExample() {
        System.out.println("-----center example-----");
        System.out.println(StringUtils.center("1234", 2));
        System.out.println(StringUtils.center("1234", 8));
        System.out.println(StringUtils.center("1234", 12));
    }

    private static void runSubstringExample() {
        System.out.println("-----substring example-----");
        System.out.println(StringUtils.substring("1234", -2));
        System.out.println(StringUtils.substring("1234", -1));
        System.out.println(StringUtils.substring("1234", 0));
        System.out.println(StringUtils.substring("1234", 1));
        System.out.println(StringUtils.substring("1234", 2));
    }

    private static void runAbbreviationExample() {
        System.out.println("----- abbreviation example-----");
        System.out.println(StringUtils.abbreviate("123456", 4));
        System.out.println(StringUtils.abbreviate("123456", 5));
        System.out.println(StringUtils.abbreviate("123456", 6));
        System.out.println(StringUtils.abbreviate("123456", 7));
    }

    private static void runAllBlankExample() {
        System.out.println("----- all blank example-----");
        System.out.println("all values are blank: " + StringUtils.isAllBlank(" ", "", "  \n", " \t "));
    }
}
