package icom.libs.apache.common;

import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.DefaultFileComparator;
import org.apache.commons.io.comparator.DirectoryFileComparator;
import org.apache.commons.io.file.Counters;
import org.apache.commons.io.file.PathUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Arrays;

public class IoExample {

    private static final File FILE1 = new File("path1");
    private static final File FILE2 = new File("path2");
    private static final File FILE3 = new File("path1");

    public static void main(String[] args) throws Exception {
        runCountDirectoryExample();

        runDefaultFileComparisonResult();

        runDirectoryFileComparisonResult();

        runFileFilterExample();

        runDirectoryFilterExample();

        runMonitorExample();
    }

    private static void runMonitorExample() throws Exception {
        System.out.println("-----monitor example-----");
        //get the file object
        File inputFile = FileUtils.getFile("input.txt");
        String absolutePath = inputFile.getAbsolutePath();
        String parent = absolutePath.substring(0, absolutePath.indexOf("input.txt"));
        File parentDirectory = FileUtils.getFile(parent);
        FileAlterationObserver observer = new
                FileAlterationObserver(parentDirectory);
        observer.addListener(new FileAlterationListenerAdaptor() {
            @Override
            public void onDirectoryCreate(File file) {
                System.out.println("[Listener] Folder created: " + file.getName());
            }

            @Override
            public void onDirectoryDelete(File file) {
                System.out.println("[Listener] Folder deleted: " + file.getName());
            }

            @Override
            public void onFileCreate(File file) {
                System.out.println("[Listener] File created: " + file.getName());
            }

            @Override
            public void onFileDelete(File file) {
                System.out.println("[Listener] File deleted: " + file.getName());
            }
        });
        //create a monitor to check changes after every 500 ms
        FileAlterationMonitor monitor = new FileAlterationMonitor(500, observer);
        monitor.start();
        //create a new directory
        File newFolder = new File("test");
        File newFile = new File("test1");
        System.out.println("[main thread] folder created: " + newFolder.mkdirs());
        Thread.sleep(1000);
        System.out.println("[main thread] file created: " + newFile.createNewFile());
        Thread.sleep(1000);
        FileDeleteStrategy.NORMAL.delete(newFolder);
        Thread.sleep(1000);
        FileDeleteStrategy.NORMAL.delete(newFile);
        Thread.sleep(1000);
        monitor.stop(10000);
    }

    private static void runDirectoryFilterExample() throws IOException {
        System.out.println("-----directory filter example-----");
        Path tempPath = Files.createDirectories(Paths.get("temp" + LocalDate.now()));
        IOFileFilter directoryFilter = FileFilterUtils.directoryFileFilter();
        System.out.println("directory created at path: " + tempPath);
        File[] filteredDir = FileFilterUtils.filter(directoryFilter, FILE1, FILE2, FILE3, tempPath.toFile());
        System.out.println("filtered directories:");
        Arrays.stream(filteredDir)
                .map(File::toURI)
                .forEach(System.out::println);
        if (tempPath.toFile().exists()) {
            Files.delete(tempPath);
            System.out.println("temp directory removed");
        }
    }

    private static void runFileFilterExample() {
        System.out.println("-----run file filters example-----");
        IOFileFilter orFileFilter = FileFilterUtils.or(
                FileFilterUtils.nameFileFilter("path1"),
                FileFilterUtils.nameFileFilter("path3"));
        File[] filtered = FileFilterUtils.filter(orFileFilter, FILE1, FILE2, FILE3);
        System.out.println("filtered files:");
        Arrays.stream(filtered)
                .map(File::toURI)
                .forEach(System.out::println);
    }

    private static void runDirectoryFileComparisonResult() {
        System.out.println("-----file comparison example, directory comparator-----");
        DirectoryFileComparator directoryFileComparator = new DirectoryFileComparator();
        System.out.println(directoryFileComparator.compare(FILE1, FILE2));
        System.out.println("files (file1/file2) supposed to BE equals, result: " + directoryFileComparator.compare(FILE1, FILE2));
        System.out.println("files (file1/file3) supposed to BE equals, result: " + directoryFileComparator.compare(FILE1, FILE3));
    }

    private static void runDefaultFileComparisonResult() {
        System.out.println("-----file comparison example, default comparator-----");
        DefaultFileComparator defaultFileComparator = new DefaultFileComparator();
        System.out.println("files (file1/file2) supposed to BE NOT equals, result: " + defaultFileComparator.compare(FILE1, FILE2));
        System.out.println("files (file1/file3) supposed to BE equals, result: " + defaultFileComparator.compare(FILE1, FILE3));
    }

    private static void runCountDirectoryExample() throws IOException {
        System.out.println("-----count directory example-----");
        Counters.PathCounters pathCounters = PathUtils.countDirectory(Paths.get("", ""));
        System.out.println(pathCounters);
        System.out.println(Paths.get("", "").toUri());
    }
}
