package icom.libs.guava;

import com.google.common.base.Preconditions;

import java.util.Arrays;
import java.util.List;

public class PreconditionsExample {

    public static final String PRECONDITIONS_MESSAGE_ERROR = "value must be positive but is %s";
    public static final String CHECK_STATE_MESSAGE_ERROR = "error message for check state";

    public static void main(String[] args) {
        checkArgumentExample();

        checkPositionIndexExample();

        checkStateExample();
    }

    private static void checkStateExample() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        Preconditions.checkState(numbers.contains(1));

        try {
            Preconditions.checkState(numbers.contains(10), CHECK_STATE_MESSAGE_ERROR);
        } catch (Exception e) {
            System.out.printf("caught the exception: %s, with message: %s%n", e.getClass(), e.getMessage());
        }
    }

    private static void checkPositionIndexExample() {
        int[] numbers = new int[]{1, 2, 3, 4, 5};
        System.out.println("Check position result: " + Preconditions.checkPositionIndex(5, numbers.length));

        try {
            Preconditions.checkPositionIndex(10, (int) Arrays.stream(numbers).count());
        } catch (Exception e) {
            System.out.printf("caught the exception: %s, with message: %s%n", e.getClass(), e.getMessage());
        }
    }

    private static void checkArgumentExample() {
        int value = 100;

        checkPreconditions(value);
        System.out.println("First precondition success");

        value = -100;
        try {
            checkPreconditions(value);
        } catch (Exception e) {
            System.out.printf("caught the exception: %s, with message: %s%n", e.getClass(), e.getMessage());
        }
    }

    private static void checkPreconditions(int value) {
        Preconditions.checkArgument(value > 0, PRECONDITIONS_MESSAGE_ERROR, value);
    }
}
