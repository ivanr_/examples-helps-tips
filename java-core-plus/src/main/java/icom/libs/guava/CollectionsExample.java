package icom.libs.guava;

import com.google.api.client.util.Joiner;
import com.google.common.collect.*;

import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CollectionsExample {
    public static void main(String[] args) {
        hashMapRunExample();

        multiMapRunExample();

        runKeyKeyValueMapExample();

        runCartesianCombinationExample();

        runSetsIntersectionExample();

        runSetsDifferenceExample();

        runPowerSetExample();

        runContiguousSetRangeExample();

        runRangeSetExample();

        runMultiSetExample();

        runMapMakerExample();

        runEvictingQueueExample();
    }

    @SuppressWarnings("UnstableApiUsage")
    private static void runEvictingQueueExample() {
        System.out.println("-----evicting queue-----");
        Queue<Integer> evictingQueue = EvictingQueue.create(10);
        IntStream.range(0, 10)
                .forEach(evictingQueue::add);
        System.out.println("filled queue: " + evictingQueue);
        evictingQueue.add(100);
        System.out.println("filled queue after element have been added: " + evictingQueue);
    }

    private static void runMapMakerExample() {
        System.out.println("-----map maker-----");
        ConcurrentMap<Integer, String> cache = new MapMaker()
                .initialCapacity(100)
                .concurrencyLevel(2)
                .weakKeys()
                .weakValues()
                .makeMap();
        System.out.println("map maker created cache with weak keys/values, concurrencyLevel and init capacity" + cache);
    }

    private static void runMultiSetExample() {
        System.out.println("-----hash multi set-----");
        Multiset<String> names = HashMultiset.create();
        names.add("John");
        names.add("Adam", 3);
        names.add("John");
        System.out.println("init collection: " + names);
        names.remove("John");
        System.out.println("after John removed 1 time" + names);
        names.remove("Adam", 2);
        System.out.println("after Adam removed twice" + names);
    }

    private static void runKeyKeyValueMapExample() {
        Table<String, Integer, String> complexDescription = HashBasedTable.create();
        complexDescription.put("firstRow", 1, "value");
        complexDescription.put("firstRow", 1, "value 2");
        complexDescription.put("other row", 1, "value 3");
        System.out.println("as 2-dimensional array table contains: " + complexDescription);
    }

    private static void multiMapRunExample() {
        Multimap<Integer, String> multiMap = ArrayListMultimap.create();
        multiMap.put(1, "one");
        multiMap.put(1, "two");
        multiMap.put(2, "three");
        multiMap.put(2, "two");
        System.out.println("Whole multiMap" + multiMap);
        System.out.println("MultiMap values: " + multiMap.values());
    }

    private static void hashMapRunExample() {
        BiMap<Integer, String> biMap = HashBiMap.create();
        biMap.put(1, "one");
        biMap.put(2, "two");
        BiMap<String, Integer> invertedMap = biMap.inverse();

        System.out.println("After inverting key and values is switched: " + invertedMap.keySet().equals(biMap.values()));
        System.out.println("Bimap: " + biMap);
        System.out.println("Inverted Bimap: " + invertedMap);
    }


    @SuppressWarnings("UnstableApiUsage")
    private static void runRangeSetExample() {
        System.out.println("-----range set | unstable-----");
        RangeSet<Integer> rangeSet = TreeRangeSet.create();
        rangeSet.add(Range.closed(1, 10));
        rangeSet.add(Range.closed(12, 15));
        System.out.println("range set before adding 10..12: " + rangeSet);
        System.out.println("range joined set is: " + rangeSet.asRanges().size());
        rangeSet.add(Range.closed(10, 12));
        System.out.println("range set after adding 10..12: " + rangeSet);
        System.out.println("range joined set is: " + rangeSet.asRanges().size());
    }

    private static void runContiguousSetRangeExample() {
        System.out.println("-----ContiguousSet range-----");
        int start = 10;
        int end = 30;
        ContiguousSet<Integer> set = ContiguousSet.create(
                Range.closed(start, end), DiscreteDomain.integers());
        System.out.println("set: " + set + ", size: " + set.size());
    }

    private static void runPowerSetExample() {
        System.out.println("-----power set-----");
        Set<Character> chars = ImmutableSet.of('a', 'b');
        Set<Set<Character>> result = Sets.powerSet(chars);
        System.out.println("DOESN'T WORK");
        System.out.println("result of power set: " + result);
    }

    private static void runSetsDifferenceExample() {
        System.out.println("-----sets difference-----");
        Set<Character> first = ImmutableSet.of('a', 'b', 'c');
        Set<Character> second = ImmutableSet.of('b', 'c', 'd');
        Set<Character> difference = Sets.symmetricDifference(first, second);
        System.out.println("first input: " + first);
        System.out.println("second input: " + second);
        System.out.println("sets difference: " + difference);
    }

    private static void runSetsIntersectionExample() {
        System.out.println("-----sets intersection-----");
        Set<String> first = ImmutableSet.of("a", "b", "c");
        Set<String> second = ImmutableSet.of("b", "c", "d");

        Set<String> intersectedResult = Sets.intersection(first, second);
        System.out.println("first input: " + first);
        System.out.println("second input: " + second);
        System.out.println("sets intersection result: " + intersectedResult);
    }

    private static void runCartesianCombinationExample() {
        System.out.println("----- cartesian combination-----");
        Set<String> first = ImmutableSet.of("a", "b", "c");
        Set<Integer> second = ImmutableSet.of(1, 2);
        Set<List<String>> result =
                Sets.cartesianProduct(ImmutableList
                        .of(first, second.stream()
                                .map(String::valueOf).collect(Collectors.toSet())));

        Function<List<String>, String> func = input -> Joiner.on(' ').join(input);
        Iterable<String> joined = result.stream().map(func).collect(Collectors.toList());
        System.out.println("first input: " + first);
        System.out.println("second input: " + second);
        System.out.println("cartesian combination result: " + joined);
    }
}
