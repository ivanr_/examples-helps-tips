package icom.libs.guava;

import com.google.common.base.CharMatcher;

public class CharMatcherExample {

    private static String input = "H*el.lo,}12";

    public static void main(String[] args) {
        CharMatcher matcher = runLetterOrDigitsMatcherExampleAndReturn();


        runRangeFilteringExample();

        CharMatcher matcher3 = runExactCharsFilteringExampleAndReturn();

        runCombinedMatchersExample(matcher, matcher3);

        runCharCheckExample();

        runTrimExample();

        runCollapseExample();
    }

    private static void runCollapseExample() {
        System.out.println("----- collapse examples-----");
        input = "   h  el  lo   ";
        System.out.println("input value: " + input);
        System.out.println("collapse example with spaces turning into the plus: " + CharMatcher.is(' ').collapseFrom(input, '+'));
        System.out.println("collapse example with spaces turning into the minus and trimming : " + CharMatcher.is(' ').trimAndCollapseFrom(input, '-'));
    }

    private static void runTrimExample() {
        System.out.println("----- trim examples-----");
        System.out.println("trim trailing '12' from input, result: " + CharMatcher.anyOf("12").trimTrailingFrom(input));
        System.out.println("trim leading 'H*' from input, result: " + CharMatcher.anyOf("H*").trimLeadingFrom(input));
    }

    private static void runCharCheckExample() {
        System.out.println("-----char functions example-----");
        CharMatcher matcher4 = CharMatcher.is('l');
        System.out.println("extracting predefined symbol 'l' from input, result:" + matcher4.retainFrom(input));
        System.out.println("first index of predefined char 'l', result: " + matcher4.indexIn(input));
        System.out.println("is match of any char (l) in the sequence: " + matcher4.matchesAnyOf(input));
    }

    private static void runCombinedMatchersExample(CharMatcher matcher, CharMatcher matcher3) {
        System.out.println("-----combined matchers example-----");
        System.out.println("result from LetterOrDigits matcher AND l21-chars matcher: " + matcher.and(matcher3).retainFrom(input));
        System.out.println("result from LetterOrDigits matcher OR l21-chars matcher: " + matcher.or(matcher3).retainFrom(input));
    }

    private static CharMatcher runExactCharsFilteringExampleAndReturn() {
        System.out.println("-----exact chars filtering example-----");
        CharMatcher matcher3 = CharMatcher.anyOf("l21");
        System.out.println("result with anyOf 'l21': " + matcher3.retainFrom(input));
        return matcher3;
    }

    private static void runRangeFilteringExample() {
        System.out.println("-----range filtering example-----");
        CharMatcher matcher1 = CharMatcher.inRange('0', 'F');
        System.out.println("result with range from 0 to F: " + matcher1.retainFrom(input));

        CharMatcher matcher2 = CharMatcher.inRange('0', 'f');
        System.out.println("result with range from 0 to f: " + matcher2.retainFrom(input));
    }

    @SuppressWarnings("deprecation")
    private static CharMatcher runLetterOrDigitsMatcherExampleAndReturn() {
        System.out.println("-----letter or digits filtering example-----");
        System.out.println("input: " + input);
        CharMatcher matcher = CharMatcher.javaLetterOrDigit();
        String result = matcher.retainFrom(input);
        System.out.println("result as letters or digits: " + result);
        return matcher;
    }
}
