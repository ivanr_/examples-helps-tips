package icom.libs.guava;

import com.google.api.client.util.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.ByteSink;
import com.google.common.io.CharSink;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class IoWriteReadExample {
    public static void main(String[] args) throws IOException {
        runSinkWriteExample();

        runSinkWriteLinesExample();

        runSinkAsBytesExample();

        runReadLinesExample();
    }

    @SuppressWarnings("UnstableApiUsage")
    private static void runReadLinesExample() throws IOException {
        System.out.println("-----readLines-----");
        String expectedValue = "Hello\nworld";
        File writeFile = new File("test.txt");
        ByteSink sink = Files.asByteSink(writeFile);
        sink.write(expectedValue.getBytes());
        System.out.println("file wrote, expected data: " + expectedValue);
        File readFile = new File("test.txt");
        List<String> result = Files.readLines(readFile, Charsets.UTF_8);
        System.out.println("read data as list: " + result);
        writeFile.deleteOnExit();
    }

    @SuppressWarnings({"deprecation", "UnstableApiUsage"})
    private static void runSinkAsBytesExample() throws IOException {
        System.out.println("-----sink write as bytes-----");
        String expectedValue = "Hello world";
        File file = new File("test-asBytes.txt");
        ByteSink sink = Files.asByteSink(file);
        sink.write(expectedValue.getBytes());
        String result = Files.toString(file, Charsets.UTF_8);
        System.out.println("file wrote by sink.write() as bytes with data " + result);
        file.deleteOnExit();
    }

    @SuppressWarnings({"deprecation", "UnstableApiUsage"})
    private static void runSinkWriteLinesExample() throws IOException {
        System.out.println("-----sink writeLines-----");
        List<String> names = Lists.newArrayList("John", "Jane", "Adam", "Tom");
        File file = new File("test-writeLines.txt");
        CharSink sink = Files.asCharSink(file, Charsets.UTF_8);
        sink.writeLines(names, System.lineSeparator());
        String result = Files.toString(file, Charsets.UTF_8);
        System.out.println("file wrote by sink.writeLines() with data " + result);
        file.deleteOnExit();
    }

    @SuppressWarnings({"deprecation", "UnstableApiUsage"})
    private static void runSinkWriteExample() throws IOException {
        System.out.println("-----sink write-----");
        String expectedValue = "Hello world";
        File file = new File("test-write.txt");
        CharSink sink = Files.asCharSink(file, Charsets.UTF_8);
        sink.write(expectedValue);
        String result = Files.toString(file, Charsets.UTF_8);
        System.out.println("file wrote by sink.write() with data \n" + result);
        file.deleteOnExit();
    }
}
