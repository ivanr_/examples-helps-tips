package icom.libs.guava;

import com.google.common.math.DoubleMath;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DoubleMathExample {
    public static void main(String[] args) {
        System.out.println("factorial evaluation: " + DoubleMath.factorial(4));

        fuzzyCompare();

        assertEquals(1.0, 0.9, 0.1);
    }

    private static void fuzzyCompare() {
        double v1 = 1.;
        double v2 = 0.999;
        double precision1 = 0.01;
        double precision2 = 0.000_1;
        System.out.printf("\nvalues to compare: %f and %f", v1, v2);
        System.out.printf("\nprecision is %f, result: %d", precision1, DoubleMath.fuzzyCompare(v1, v2, precision1));
        System.out.printf("\nprecision is %f, result: %d%n", precision2, DoubleMath.fuzzyCompare(v1, v2, precision2));
    }
}
