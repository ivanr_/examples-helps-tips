package icom.libs.guava;

import com.google.common.collect.Ordering;
import com.google.common.primitives.Ints;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class OrderingExample {
    public static void main(String[] args) {
        runNaturalOrderWithNullFirstExample();

        runReverseOrderingExample();

        runStringOrderingWithProvidedComparatorExample();

        runOrderingWithCustomClass();

        runExplicitOrderCheckExample();
    }

    private static void runExplicitOrderCheckExample() {
        System.out.println("----- explicit order-----");
        List<Integer> input = Arrays.asList(1, 3, 5, 2, 4);
        System.out.println("input before sort: " + input);
        List<Integer> order1 = Arrays.asList(1, 3, 5, 2, 6, 4);
        Ordering<Integer> explicit1 = Ordering.explicit(order1);
        List<Integer> order2 = Arrays.asList(1, 3, 5, 4, 6, 2);
        Ordering<Integer> explicit2 = Ordering.explicit(order2);
        System.out.println("input matches with order1 (" + order1 + "): " + explicit1.isOrdered(input));
        System.out.println("input matches with order2 (" + order2 + "): " + explicit2.isOrdered(input));
    }

    private static void runOrderingWithCustomClass() {
        System.out.println("-----custom ordering class-----");
        final List<String> input = Arrays.asList("am", "not", "I", "only", "funny", "rabbit");
        System.out.println("before ordering: " + input);
        input.sort(new OrderingByLength());
        System.out.println("after ordering: " + input);
    }

    private static void runStringOrderingWithProvidedComparatorExample() {
        System.out.println("-----custom comparator to ordering-----");
        final List<String> input = Arrays.asList("am", "not", "I", "only", "funny", "rabbit");
        System.out.println("before ordering: " + input);
        input.sort(Ordering.from(Comparator.comparingInt(String::length)));
        System.out.println("after ordering: " + input);
    }

    private static void runReverseOrderingExample() {
        System.out.println("-----reversed ordering-----");
        List<Integer> input = Arrays.asList(1, 4, 6, 2, 3);
        System.out.println("input list: " + input);
        input.sort(Ordering.natural().reverse());
        System.out.println("reversed order for the input: " + input);
    }

    private static void runNaturalOrderWithNullFirstExample() {
        final List<String> input = Arrays.asList("one", "two", "three", null, "four");
        System.out.println("-----sort with natural order, null first-----");
        System.out.println("input unordered: " + input);
        input.sort(Ordering.natural().nullsFirst());
        System.out.println("filtered list, null goes first: " + input);
    }

    private static class OrderingByLength extends Ordering<String> {
        @Override
        public int compare(String s1, String s2) {
            assert s1 != null && s2 != null;
            return Ints.compare(s1.length(), s2.length());
        }
    }
}
