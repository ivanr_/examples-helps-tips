package icom.libs.guava;

import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

public class FunctionsPredicateExample {
    public static void main(String[] args) {
        runListFiltrationExample();

        runNullFiltrationExample();

        runFiltrationWithPredicateNegationExample();

        System.out.println("\nNOTE: com.google.common.base.Predicate AND java.util.function.Predicate usage is the same");
    }

    @SuppressWarnings("all")
    private static void runFiltrationWithPredicateNegationExample() {
        System.out.println("-----list filtration with predicate negation example-----");
        List<String> input = Lists.newArrayList("one", "two", "three");
        System.out.println("input list: " + input);
        Predicate<String> moreThanThreeCharsPredicate = string -> string.length() > 3;
        List<String> filtratedInput = Lists.newArrayList(Collections2.filter(input, Predicates.not(moreThanThreeCharsPredicate::test)));
        System.out.println("filtrated list with less than or 3 chars: " + filtratedInput);
    }

    private static void runNullFiltrationExample() {
        System.out.println("-----list filtration example-----");
        List<String> input = Lists.newArrayList("one", null, "three");
        System.out.println("input list: " + input);
        System.out.println("filtrated list without nulls: " + Iterables.filter(input, Objects::nonNull));
    }

    private static void runListFiltrationExample() {
        System.out.println("-----list filtration example-----");
        List<String> input = Lists.newArrayList("one", "two", "three");
        System.out.println("input list: " + input);
        Predicate<String> moreThanThreeCharsPredicate = string -> string.length() > 3;
        List<String> filtratedInput = Lists.newArrayList(Collections2.filter(input, moreThanThreeCharsPredicate::test));
        System.out.println("filtrated list with more than 3 chars: " + filtratedInput);
    }
}
