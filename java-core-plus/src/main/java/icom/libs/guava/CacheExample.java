package icom.libs.guava;

import com.google.common.cache.*;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CacheExample {

    private static int invocationNumber = 0;

    public static void main(String[] args) throws InterruptedException {
        runCacheWithBuilderExample();

        runCacheWithRemovalListenerExample();
    }

    @SuppressWarnings("all")
    private static void runCacheWithRemovalListenerExample() {
        System.out.println("-----removal listener with cache-----");
        CacheLoader<String, String> loader;
        loader = new CacheLoader<String, String>() {
            @Override
            public String load(final String key) {
                return key.toUpperCase();
            }
        };

        RemovalListener<String, String> listener;
        listener = n -> {
            if (n.wasEvicted()) {
                String cause = n.getCause().name();
                assertEquals(RemovalCause.SIZE.toString(),cause);
                System.out.println("removal cause: " + cause + ", value: " + n);
            }
        };

        LoadingCache<String, String> cache;
        cache = CacheBuilder.newBuilder()
                .maximumSize(3)
                .removalListener(listener)
                .build(loader);

        cache.getUnchecked("first");
        cache.getUnchecked("second");
        cache.getUnchecked("third");
        cache.getUnchecked("last");
        cache.getUnchecked("the very last");
    }

    private static void runCacheWithBuilderExample() throws InterruptedException {
        System.out.println("-----cache with max size and expiration time-----");
        LoadingCache<Integer, Long> memo = CacheBuilder.newBuilder()
                .expireAfterAccess(2, TimeUnit.SECONDS)
                .maximumSize(5)
                .softValues()
                .build(CacheLoader.from(CacheExample::getSquareFunction));

        memo.refresh(0);
        System.out.println("Cache map: " + memo.asMap());
        memo.refresh(0);
        memo.refresh(2);
        System.out.println("Cache map after refresh: " + memo.asMap());

        Thread.sleep(2100);

        System.out.println("Cache map after expiration: " + memo.asMap());
    }

    private static Long getSquareFunction() {
        System.out.println("function called");
        invocationNumber++;
        return (long) invocationNumber * invocationNumber;
    }
}
