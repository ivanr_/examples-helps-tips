package icom.libs.guava;

import com.google.common.base.Suppliers;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class MemoizerExample {
    public static void main(String[] args) throws InterruptedException {
        runMemoizerWithExpirationExample();

        runMemoizerWithoutExpirationExample();

        runMemoizerWithComposeFunction();

        System.out.println("\nNOTE: java.util.function.Supplier and com.google.common.base.Supplier ARE the same");
    }

    private static void runMemoizerWithComposeFunction() throws InterruptedException {
        System.out.println("-----memoize with composed function-----");
        Supplier<String> compose = Suppliers.compose(
                input -> "new string with value: " + input,
                Suppliers.memoizeWithExpiration(MemoizerExample::getInt, 2, TimeUnit.SECONDS));
        System.out.println("before expiration: " + compose.get());
        Thread.sleep(3000);
        System.out.println("after expiration: " + compose.get());
    }

    private static void runMemoizerWithoutExpirationExample() throws InterruptedException {
        System.out.println("-----memoizeWithoutExpiration-----");
        Supplier<Integer> memoizedSupplier = Suppliers.memoize(MemoizerExample::getInt);
        System.out.println("value from memoized: " + memoizedSupplier.get());
        Thread.sleep(500);
        System.out.println("value from memoized after 500ms: " + memoizedSupplier.get());
        Thread.sleep(3000);
        System.out.println("value from memoized after ones more 3000ms: " + memoizedSupplier.get());
    }

    private static void runMemoizerWithExpirationExample() throws InterruptedException {
        System.out.println("-----memoizeWithExpiration-----");
        Supplier<Integer> memoizedSupplier = Suppliers.memoizeWithExpiration(MemoizerExample::getInt, 3, TimeUnit.SECONDS);
        System.out.println("value from memoized: " + memoizedSupplier.get());
        Thread.sleep(500);
        System.out.println("value from memoized after 500ms: " + memoizedSupplier.get());
        Thread.sleep(3000);
        System.out.println("value from memoized after ones more 3000ms: " + memoizedSupplier.get());
    }

    private static int getInt() {
        System.out.println("new value assigned");
        return new Random().nextInt(100);
    }
}
