package icom.libs.guava;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CollectionJoinAndSplitExample {
    public static void main(String[] args) {
        runNewArrayAndLinkedListsExample();

        runJoinerExample();

        runListCollectingExample();

        runStringToListExample();

        runStringToMapExample();

        runStringToListWithPatternExample();

        runSplittingWithOutputLimitedExample();
    }

    private static void runSplittingWithOutputLimitedExample() {
        System.out.println("-----string to list with output limited nodes-----");
        String input = "one| two | three";
        System.out.println("input value: " + input);
        System.out.println("result with limit 2: " + Splitter.on("|").trimResults().limit(2).splitToList(input));
    }

    private static void runStringToListWithPatternExample() {
        System.out.println("-----string to list with pattern example-----");
        String input = "one, two.three / four";
        System.out.println("input: " + input);
        System.out.println("split string into the list with pattern, result: " + Splitter.onPattern("[,./]").trimResults().splitToList(input));
    }

    @SuppressWarnings("UnstableApiUsage")
    private static void runStringToMapExample() {
        System.out.println("-----string to map example-----");
        String input = "{\"name\": \"john\",\"address\": 123}";
        System.out.println("input string: " + input);
        String trimmedInput = CharMatcher.anyOf("{}").trimFrom(input);
        System.out.println("trimmed string from input: " + trimmedInput);
        System.out.println("result map: " + Splitter.on(",").withKeyValueSeparator(':').split(trimmedInput));
    }

    private static void runStringToListExample() {
        System.out.println("-----string to list with trimming example-----");
        String input = "one  - two-  three-four   -   five";
        System.out.println("list from the string: " + Splitter.on('-').trimResults(CharMatcher.anyOf(" ")).splitToList(input));
    }

    private static void runListCollectingExample() {
        System.out.println("----- lists collecting example-----");
        List<Object> list = Lists.newArrayList(
                "zero",
                Lists.newArrayList("one"),
                Lists.newArrayList(2),
                Lists.newArrayList("three", "four"));
        System.out.println("collected list from three lists and 1 element: " + list);
    }

    private static void runJoinerExample() {
        System.out.println("-----map Joiner example-----");
        HashMap<Integer, String> hashMap = Maps.newHashMap();
        hashMap.put(1, "one");
        hashMap.put(2, "two");
        hashMap.put(4, null);
        hashMap.put(null, null);
        System.out.println(Joiner.on("/").useForNull("IS_NULL").withKeyValueSeparator("-").join(hashMap));
    }

    private static void runNewArrayAndLinkedListsExample() {
        System.out.println("----- new lists creation example-----");
        List<String> arrayListInOneConstructor = Lists.newArrayList("one", "two", "three");
        System.out.println("arrayListWithValuesInConstructor: " + arrayListInOneConstructor);

        List<String> linkedListInOneConstructor = Lists.newLinkedList(Arrays.asList("one", "two", "three"));
        System.out.println("linkedListWithValuesInConstructor: " + linkedListInOneConstructor);
    }
}
