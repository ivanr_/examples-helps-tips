package icom.libs.guava;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.*;

public class ImmutableMapAndListExample {
    public static void main(String[] args) {
        runKeyAndValueMapCreationExample();

        runImmutableMapCreationExample();

        runImmutableListCreationWithBuilderExample();
    }

    private static void runImmutableListCreationWithBuilderExample() {
        System.out.println("----- immutable list creation with builder-----");
        List<String> javaApiList = new ArrayList<>();
        javaApiList.add("one");
        javaApiList.add("two");
        javaApiList.add("three");
        System.out.println("init list: " + javaApiList);
        ImmutableList<Object> immutableList = ImmutableList.builder()
                .addAll(javaApiList)
                .add("four")
                .add("five", 6)
                .build();
        System.out.println("immutable list from builder: " + immutableList);
        javaApiList.add("zero");
        System.out.println("init mutated list: " + javaApiList);
        System.out.println("immutable list from builder: " + immutableList);
    }

    private static void runImmutableMapCreationExample() {
        System.out.println("-----immutable map creation from java api map-----");
        Map<String, Integer> javaApiMap = new HashMap<>();
        javaApiMap.put("key", 1);
        System.out.println("init map: " + javaApiMap);
        ImmutableMap<String, Integer> immutableMap = ImmutableMap.copyOf(javaApiMap);
        System.out.println("immutable map: " + immutableMap);
        javaApiMap.put("key", 2);
        System.out.println("init changed map: " + javaApiMap);
        System.out.println("immutable map: " + immutableMap);
    }

    private static void runKeyAndValueMapCreationExample() {
        System.out.println("-----key-value map creation-----");
        ImmutableMap<Integer, String> map = ImmutableMap.of(1, "one", 2, "two", 3, "three");
        System.out.println("simple map creation result: " + map);
    }
}
