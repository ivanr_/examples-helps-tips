package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JsonIdentityInfoExample {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        User user = new User(22, "my name");
        User newUser = new User(33, "new user");
        Car car = new Car(44, 321, "bmw", "x2");
        Car newCar = new Car(55, 444, "mart", "zz");

        user.cars.add(car);
        newUser.cars.addAll(Arrays.asList(car, newCar));

        car.users.addAll(Arrays.asList(user, newUser));
        newCar.users.add(newUser);

        String jsonUser = MAPPER.writeValueAsString(user);
        System.out.println("user with car: " + jsonUser);
        System.out.println("helps to exclude circular links, to check - remove @JsonIdentityInfo");
    }

    @JsonIdentityInfo(
            generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    private static class User {
        public long id;
        public String name;
        public List<Car> cars = new ArrayList<>();

        public User(long id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    @JsonIdentityInfo(
            generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    private static class Car {
        public int id;
        public long code;
        public String make;
        public String model;
        public List<User> users = new ArrayList<>();

        public Car(int id, long code, String make, String model) {
            this.id = id;
            this.code = code;
            this.make = make;
            this.model = model;
        }
    }
}

