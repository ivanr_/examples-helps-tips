package icom.libs.jackson.annotations;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

public class JsonPOJOBuilderExample {

    public static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        String jsonUser = "{\"id\":123,\"name\":\"My name 1\"}";
        POJOBuilderUser user = MAPPER.readValue(jsonUser, POJOBuilderUser.class);

        System.out.println("input: " + jsonUser);
        System.out.println("user from builder: " + user);
    }

    @JsonDeserialize(builder = UserBuilder.class)
    private static class POJOBuilderUser {
        public long idValue;
        public String nameValue;

        public POJOBuilderUser(long id, String name) {
            this.idValue = id;
            this.nameValue = name;
        }

        @Override
        public String toString() {
            return "POJOBuilderUser{" +
                    "idValue=" + idValue +
                    ", nameValue='" + nameValue + '\'' +
                    '}';
        }
    }

    @JsonPOJOBuilder(buildMethodName = "createBean", withPrefix = "construct")
    private static class UserBuilder {
        private int id;
        private String name;

        public UserBuilder constructId(int id) {
            this.id = id;
            return this;
        }

        public UserBuilder constructName(String name) {
            this.name = name;
            return this;
        }

        public POJOBuilderUser createBean() {
            return new POJOBuilderUser(id, name);
        }
    }


}
