package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonViewExample {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        User user = new User(1, "my name", "my address");
        String jsonUserPublic = MAPPER.writerWithView(Views.PublicName.class).writeValueAsString(user);
        String jsonUserDelivery = MAPPER.writerWithView(Views.Delivery.class).writeValueAsString(user);
        String jsonUserDb = MAPPER.writerWithView(Views.DbAllData.class).writeValueAsString(user);
        System.out.println("only public name: " + jsonUserPublic);
        System.out.println("name/address for delivery: " + jsonUserDelivery);
        System.out.println("id/name for db: " + jsonUserDb);
    }

    private static class User {
        @JsonView(Views.DbAllData.class)
        public long id;
        @JsonView({Views.PublicName.class, Views.Delivery.class, Views.DbAllData.class})
        public String name;
        @JsonView(Views.Delivery.class)
        public String address;

        public User(long id, String name, String address) {
            this.id = id;
            this.name = name;
            this.address = address;
        }
    }

    private static class Views {
        public static class PublicName {
        }

        public static class Delivery {
        }

        public static class DbAllData {
        }
    }
}
