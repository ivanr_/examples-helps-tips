package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonSetterAndJsonPropertyExample {
    public static void main(String[] args) throws JsonProcessingException {
        String jsonUser = "{\"name\":1,\"titleString\":\"My title\"}";
        User user = new ObjectMapper().readValue(jsonUser, User.class);
        System.out.println(user);
    }


    private static class User {
        private String title;
        @JsonProperty
        public String name;

        @JsonSetter("titleString")
        @SuppressWarnings("unused")
        public void setTitle(String title) {
            this.title = title + " PLUS data from setter";
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", title='" + title + '\'' +
                    '}';
        }
    }
}


