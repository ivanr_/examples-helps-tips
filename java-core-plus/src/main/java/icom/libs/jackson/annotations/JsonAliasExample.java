package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonAliasExample {
    public static void main(String[] args) throws JsonProcessingException {
        String jsonUser1 = "{\"id\":123,\"theName\":\"My name 1\"}";
        String jsonUser2 = "{\"id\":456,\"currentName\":\"My name 2\"}";

        User user1 = new ObjectMapper().readValue(jsonUser1, User.class);
        System.out.println(user1);
        User user2 = new ObjectMapper().readValue(jsonUser2, User.class);
        System.out.println(user2);
    }

    private static class User {
        @JsonProperty
        public String id;
        @JsonAlias({"theName", "currentName"})
        public String name;

        @Override
        public String toString() {
            return "User{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
