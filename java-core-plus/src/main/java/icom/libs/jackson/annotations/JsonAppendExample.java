package icom.libs.jackson.annotations;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonAppend;

import java.time.LocalDateTime;

public class JsonAppendExample {

    public static final ObjectMapper MAPPER = new ObjectMapper();
    public static final String CREATED_AT = "createdAt";

    public static void main(String[] args) throws JsonProcessingException {
        User user = new User();
        String jsonUser = MAPPER
                .writer()
                .withAttribute("createdAt", LocalDateTime.now().toString())
                .writeValueAsString(user);

        System.out.println(jsonUser);
    }

    @JsonAppend(attrs = {
            @JsonAppend.Attr(value = CREATED_AT)
    })
    private static class User {
        public final long id = 12;
        public final String name = "my name";
    }
}
