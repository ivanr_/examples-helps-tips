package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonIgnoreExample {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        String jsonUser1 = "{\"id\":123,\"name\":\"My name 1\"}";
        String jsonUser2 = "{\"id\":456,\"name\":\"My name 2\"}";

        User user1 = OBJECT_MAPPER.readValue(jsonUser1, User.class);
        System.out.println(user1);
        User user2 = OBJECT_MAPPER.readValue(jsonUser2, User.class);
        System.out.println(user2);
        System.out.println("new id set for user2");
        user2.id = "newId";
        System.out.println(user2);
        System.out.println(OBJECT_MAPPER.writeValueAsString(user2));
    }

    private static class User {
        @JsonIgnore
        public String id;
        @JsonProperty
        public String name;

        @Override
        public String toString() {
            return "User{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
