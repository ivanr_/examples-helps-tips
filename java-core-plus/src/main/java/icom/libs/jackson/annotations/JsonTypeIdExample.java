package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonTypeId;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import com.fasterxml.jackson.databind.jsontype.DefaultBaseTypeLimitingValidator;

@SuppressWarnings({"deprecation", "unused"})
public class JsonTypeIdExample {

    public static final ObjectMapper MAPPER1 = new ObjectMapper();
    public static final ObjectMapper MAPPER2 = new ObjectMapper();
    public static final ObjectMapper MAPPER3 = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        MAPPER1.enableDefaultTyping(ObjectMapper.DefaultTyping.EVERYTHING);
        MAPPER2.activateDefaultTyping(new DefaultBaseTypeLimitingValidator(), ObjectMapper.DefaultTyping.EVERYTHING);
        MAPPER3.activateDefaultTyping(
                BasicPolymorphicTypeValidator.builder().build(),
//                ObjectMapper.DefaultTyping.JAVA_LANG_OBJECT //Description won't be present
//                ObjectMapper.DefaultTyping.EVERYTHING //Description will be present
                ObjectMapper.DefaultTyping.NON_FINAL //Description will be present
        );

        System.out.println("deprecated case: " + MAPPER1.writeValueAsString(new User()));
        System.out.println("fresh case with default validator: " + MAPPER2.writeValueAsString(new User()));
        System.out.println("fresh case with polymorphic validator: " + MAPPER3.writeValueAsString(new User()));
    }

    private static class User {
        public final long id = 12;
        public final String userName = "my name";
        @JsonTypeId
        public Description d = new Description();
    }

    private static class Description {
        public long descId = 11;
        public String descriptionData = "some data";

        @Override
        public String toString() {
            return "DescriptionToStringValue{" +
                    "descId=" + descId +
                    ", descriptionData='" + descriptionData + '\'' +
                    '}';
        }
    }
}
