package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonAutoDetectExample {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        User user = new User("some private data", "some public data");
        System.out.println(user);
        String jsonUser = OBJECT_MAPPER.writeValueAsString(user);
        System.out.println("mapper can get an access to private (closed) field in POJO\n" + jsonUser);
        System.out.println("after deserializing");
        System.out.println(OBJECT_MAPPER.readValue(jsonUser, User.class));
    }

    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    private static class User {
        private String privateFieldShouldBeOutOfJson;
        public String publicFieldShouldBeInJson;

        public User(String privateFieldShouldBeOutOfJson, String publicFieldShouldBeInJson) {
            this.privateFieldShouldBeOutOfJson = privateFieldShouldBeOutOfJson;
            this.publicFieldShouldBeInJson = publicFieldShouldBeInJson;
        }

        @SuppressWarnings("unused") //used by jackson
        public User() {
        }

        @Override
        public String toString() {
            return "User{" +
                    "privateFieldShouldBeOutOfJson='" + privateFieldShouldBeOutOfJson + '\'' +
                    ", publicFieldShouldBeInJson='" + publicFieldShouldBeInJson + '\'' +
                    '}';
        }
    }
}