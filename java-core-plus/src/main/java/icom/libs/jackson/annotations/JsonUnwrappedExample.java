package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUnwrappedExample {
    public static void main(String[] args) throws JsonProcessingException {
        User user = new User(1, "my name", 12345, "my address");
        String jsonUser = new ObjectMapper().writeValueAsString(user);
        System.out.println(jsonUser);
    }

    private static class User {
        public long id;
        public String name;
        @JsonUnwrapped
        public Address address;

        public User(long id, String name, int zipCode, String fullAddress) {
            this.id = id;
            this.name = name;
            this.address = new Address(zipCode, fullAddress);
        }
    }

    private static class Address {
        public int zipCode;
        public String fullAddress;

        public Address(int zipCode, String fullAddress) {
            this.zipCode = zipCode;
            this.fullAddress = fullAddress;
        }
    }
}
