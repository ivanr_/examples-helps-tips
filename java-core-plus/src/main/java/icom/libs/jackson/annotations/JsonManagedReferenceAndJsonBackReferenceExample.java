package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("unused")
public class JsonManagedReferenceAndJsonBackReferenceExample {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        Tools tools = new Tools(Arrays.asList("js", "nodejs", "office"));
        Skills skills = new Skills(Arrays.asList("estimating", "procrastinating"));
        User user = new User(100, "my example name", tools, skills);

        tools.userLink = user;

        String jsonUser = MAPPER.writeValueAsString(user);
        System.out.println("note: user fields WAS NOT SHOWN for JsonBackReference");
        System.out.println("user: " + jsonUser);
        String jsonTools = MAPPER.writeValueAsString(tools);
        System.out.println("tools with userLink: " + jsonTools);
        String jsonSkills = MAPPER.writeValueAsString(skills);
        System.out.println("skills without userLink: " + jsonSkills);
    }

    private static class User {
        public long id;
        public String name;
        @JsonBackReference
        public Tools tools;
        @JsonBackReference
        public Skills skills;

        public User(long id, String name, Tools tools, Skills skills) {
            this.id = id;
            this.name = name;
            this.tools = tools;
            this.skills = skills;
        }
    }

    private static class Tools {
        public final String createdAt = LocalDateTime.now().toString();
        public List<String> tools;
        @JsonManagedReference
        public User userLink;

        public Tools(List<String> tools) {
            this.tools = tools;
        }
    }

    private static class Skills {
        public final String createdAt = LocalDateTime.now().toString();
        public final List<String> skills;
        @JsonManagedReference
        public User userLink;

        public Skills(List<String> skills) {
            this.skills = skills;
        }
    }
}