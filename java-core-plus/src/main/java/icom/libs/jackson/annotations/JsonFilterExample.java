package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

public class JsonFilterExample {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        User user = new User(22, "my name", "my last name");

        FilterProvider exceptIdFilter = new SimpleFilterProvider()
                .addFilter("userWithoutIdFilter", SimpleBeanPropertyFilter.serializeAllExcept("id"));
        FilterProvider filterAllButLeaveIdNameFilter = new SimpleFilterProvider()
                .addFilter("userWithoutIdFilter", SimpleBeanPropertyFilter.filterOutAllExcept("id", "lastName"));

        String jsonUserRemoveFilter = MAPPER.writer(exceptIdFilter).writeValueAsString(user);
        String jsonUserLeaveFieldFilter = MAPPER.writer(filterAllButLeaveIdNameFilter).writeValueAsString(user);
        System.out.println("user pojo: " + user);
        System.out.println("user json without id: " + jsonUserRemoveFilter);
        System.out.println("user json with id and lastName only: " + jsonUserLeaveFieldFilter);
    }

    @JsonFilter("userWithoutIdFilter")
    private static class User {
        public long id;
        public String name;
        public String lastName;

        public User(long id, String name, String lastName) {
            this.id = id;
            this.name = name;
            this.lastName = lastName;
        }

        @Override
        public String toString() {
            return "User{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", lastName='" + lastName + '\'' +
                    '}';
        }
    }
}

