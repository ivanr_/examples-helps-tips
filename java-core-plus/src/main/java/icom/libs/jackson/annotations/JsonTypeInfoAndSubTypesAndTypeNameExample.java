package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonTypeInfoAndSubTypesAndTypeNameExample {
    public static void main(String[] args) throws JsonProcessingException {
        Dog dog = new Dog("lacy", 75);
        Zoo zoo1 = new Zoo(dog);
        Cat cat = new Cat("laila", false, 6);
        Zoo zoo2 = new Zoo(cat);

        String result1 = new ObjectMapper().writeValueAsString(zoo1);
        String result2 = new ObjectMapper().writeValueAsString(zoo2);
        System.out.println(result1);
        System.out.println(result2);
        System.out.println("\noutput when JsonTypeInfo.As.WRAPPER_OBJECT\n{\"animal\":{\"dog\":{\"name\":\"lacy\",\"barkVolume\":75.0}}}\n");
        System.out.println("output when JsonTypeInfo.As.WRAPPER_ARRAY\n{\"animal\":[\"dog\",{\"name\":\"lacy\",\"barkVolume\":75.0}]}\n");
    }


    private static class Zoo {
        public Animal animal;

        public Zoo(Animal animal) {
            this.animal = animal;
        }
    }

    @SuppressWarnings("all")
    @JsonTypeInfo(
            use = JsonTypeInfo.Id.NAME,
            include = JsonTypeInfo.As.PROPERTY,
            property = "type")
    @JsonSubTypes({
            @JsonSubTypes.Type(value = Dog.class, name = "dog"),
            @JsonSubTypes.Type(value = Cat.class, name = "cat")
    })
    private static class Animal {
        public String name;

        public Animal(String name) {
            this.name = name;
        }
    }

    @JsonTypeName("doggy")
    private static class Dog extends Animal {
        public double barkVolume;

        public Dog(String name, double volume) {
            super(name);
            this.barkVolume = volume;
        }
    }

    @JsonTypeName("kitty")
    private static class Cat extends Animal {
        public boolean likesCream;
        public int lives;

        public Cat(String name, boolean likesCream, int lives) {
            super(name);
            this.likesCream = likesCream;
            this.lives = lives;
        }
    }
}