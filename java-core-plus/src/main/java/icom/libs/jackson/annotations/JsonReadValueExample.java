package icom.libs.jackson.annotations;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonReadValueExample {
    public static void main(String[] args) throws JsonProcessingException {
        String json = "{\"id\":1,\"theName\":\"My bean\"}";
        ClassWithCreator classWithCreator = new ObjectMapper().readValue(json, ClassWithCreator.class);
        System.out.println(classWithCreator);
    }
}