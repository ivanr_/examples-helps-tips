package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class JsonAnySetterExample {
    public static void main(String[] args) throws JsonProcessingException {
        String json = "{\"name\":\"My bean\",\"attr2\":\"val2\",\"attr1\":\"val1\",\"id\":\"123123123\"}";

        WithAnySetterPropertiesBean bean = new ObjectMapper()
                .readerFor(WithAnySetterPropertiesBean.class)
                .readValue(json);
        System.out.println("unexpected fields suppose to be put into properties");
        System.out.println(bean);
    }

    private static class WithAnySetterPropertiesBean {
        private final Map<String, String> properties = new HashMap<>();
        public String id;
        public String name;

        @JsonAnySetter
        public void add(String key, String value) {
            properties.put(key, value);
        }

        @Override
        public String toString() {
            return "WithAnySetterPropertiesBean{" +
                    "properties=" + properties +
                    ", id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
