package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class JsonCustomAnnotationExample {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        User user = new User(22, "my name", null);

        String jsonUserRemoveFilter = MAPPER.writeValueAsString(user);
        System.out.println("user pojo: " + user);
        System.out.println("user json without id: " + jsonUserRemoveFilter);
    }

    @CustomJsonAnnotation
    private static class User {
        public long id;
        public String name;
        public String lastName;

        public User(long id, String name, String lastName) {
            this.id = id;
            this.name = name;
            this.lastName = lastName;
        }

        @Override
        public String toString() {
            return "User{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", lastName='" + lastName + '\'' +
                    '}';
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    @JacksonAnnotationsInside
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({"lastName", "name", "id"})
    private @interface CustomJsonAnnotation {
    }
}

