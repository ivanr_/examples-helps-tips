package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonCreatorExample {
    public static void main(String[] args) throws JsonProcessingException {
        String json = "{\"id\":1,\"theName\":\"My bean\"}";

        ClassWithCreator bean1 = new ObjectMapper()
                .readerFor(ClassWithCreator.class)
                .readValue(json);
        System.out.println(bean1);

        System.out.println(new ObjectMapper().readValue(json, ClassWithCreator.class).toString());
    }
}

class ClassWithCreator {
    public int id;
    public String name;

    @JsonCreator
    public ClassWithCreator(@JsonProperty("id") int id, @JsonProperty("theName") String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "ClassWithCreator{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}