package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class JsonRootAndJsonSerializerExample {
    public static void main(String[] args) throws JsonProcessingException {
        UserWithJsonRootNameAndJsonSerialize user = new UserWithJsonRootNameAndJsonSerialize(1, "John");

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
        String result = mapper.writeValueAsString(user);
        System.out.println(result);
    }

    @SuppressWarnings("unused")
    @JsonRootName(value = "user")
    private static class UserWithJsonRootNameAndJsonSerialize {
        public int id;
        public String name;
        @JsonSerialize(using = CustomDateSerializer.class)
        public final Date date = new Date();
        @JsonSerialize(using = CustomLocalDateSerializer.class)
        public final LocalDateTime newDate = LocalDateTime.now();

        public UserWithJsonRootNameAndJsonSerialize(int id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    private static class CustomDateSerializer extends StdSerializer<Date> {
        private static final SimpleDateFormat formatter
                = new SimpleDateFormat("dd-MM-yyyy");

        protected CustomDateSerializer(Class<Date> t) {
            super(t);
        }

        @SuppressWarnings("unused")
        public CustomDateSerializer() {
            this(null);
        }

        @Override
        public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(formatter.format(date));
        }
    }

    private static class CustomLocalDateSerializer extends StdSerializer<LocalDateTime> {
        private static final DateTimeFormatter formatter
                = DateTimeFormatter.ofPattern("yyyy_MM_dd:hh-mm-ss");

        protected CustomLocalDateSerializer(Class<LocalDateTime> t) {
            super(t);
        }

        @SuppressWarnings("unused")
        public CustomLocalDateSerializer() {
            this(null);
        }

        @Override
        public void serialize(LocalDateTime dateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(Instant.now()
                    .atZone(ZoneId.systemDefault())
                    .format(formatter));
        }
    }
}