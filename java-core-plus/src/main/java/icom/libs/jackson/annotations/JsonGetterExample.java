package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonGetterExample {
    public static void main(String[] args) throws JsonProcessingException {
        User user = new User("my name", "my title");
        String jsonUser = new ObjectMapper().writeValueAsString(user);
        System.out.println(jsonUser);
    }

    private static class User {
        private String name;

        @JsonProperty("titleString")
        public String title;

        public User(String name, String title) {
            this.name = name;
            this.title = title;
        }

        @JsonGetter("firstName")
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", title='" + title + '\'' +
                    '}';
        }
    }
}
