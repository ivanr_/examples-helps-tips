package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class JsonIncludeExample {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        User userWithNotEmptyNames = new User("123", Arrays.asList("name 1", "name 2"));
        User userWithEmptyNames = new User("123", Collections.emptyList());
        System.out.println(OBJECT_MAPPER.writeValueAsString(userWithNotEmptyNames));
        System.out.println(OBJECT_MAPPER.writeValueAsString(userWithEmptyNames));
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private static class User {
        public String id;
        public List<String> names;

        public User(String id, List<String> names) {
            this.id = id;
            this.names = names;
        }

        @Override
        public String toString() {
            return "User{" +
                    "id='" + id + '\'' +
                    ", names=" + names +
                    '}';
        }
    }
}
