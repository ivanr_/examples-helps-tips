package icom.libs.jackson.annotations;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.time.LocalDateTime;
import java.util.Date;

public class JsonFormatExample {
    public static void main(String[] args) throws JsonProcessingException {
        User user = new User("my name", new Date(), null);
        String jsonUser = new ObjectMapper().writeValueAsString(user);
        System.out.println(jsonUser);
        System.out.println("Jackon version IS NOT WORKING WITH LOCALDATE, see next example");

        LocalDateTime date = LocalDateTime.of(2021, 12, 20, 2, 30);
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        String result = mapper.writeValueAsString(date);
        System.out.println(result);
    }

    private static class User {
        public String name;

        @JsonFormat(
                shape = JsonFormat.Shape.STRING,
                pattern = "yyyy---MM---dd hh::mm::ss")
        public Date createdAt;

        @JsonFormat(
                shape = JsonFormat.Shape.STRING,
                pattern = "yyyy---MM---dd hh::mm::ss")
        public LocalDateTime updatedAt;

        public User(String name, Date createdAt, LocalDateTime updatedAt) {
            this.name = name;
            this.createdAt = createdAt;
            this.updatedAt = updatedAt; //will fail
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", createdAt=" + createdAt +
                    '}';
        }
    }
}
