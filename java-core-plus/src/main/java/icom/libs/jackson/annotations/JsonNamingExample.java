package icom.libs.jackson.annotations;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@SuppressWarnings("unused") //used by jackson
public class JsonNamingExample {

    public static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {

        System.out.println("Snake case user: " + MAPPER.writeValueAsString(new SnakeCaseNameUser()));
        System.out.println("Kebab case user: " + MAPPER.writeValueAsString(new KebabCaseNameUser()));
        System.out.println("Lower case user: " + MAPPER.writeValueAsString(new LowerCaseNameUser()));
        System.out.println("Lower dot case user: " + MAPPER.writeValueAsString(new LowerDotCaseNameUser()));
        System.out.println("Upper camel case user: " + MAPPER.writeValueAsString(new UpperCamelCaseNameUser()));
    }

    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    private static class SnakeCaseNameUser {
        public final long id = 12;
        public final String userName = "my name";
    }

    @JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
    private static class KebabCaseNameUser {
        public final long id = 12;
        public final String userName = "my name";
    }

    @JsonNaming(PropertyNamingStrategies.LowerCaseStrategy.class)
    private static class LowerCaseNameUser {
        public final long id = 12;
        public final String userName = "my name";
    }

    @JsonNaming(PropertyNamingStrategies.LowerDotCaseStrategy.class)
    private static class LowerDotCaseNameUser {
        public final long id = 12;
        public final String userName = "my name";
    }

    @JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy.class)
    private static class UpperCamelCaseNameUser {
        public final long id = 12;
        public final String userName = "my name";
    }
}
