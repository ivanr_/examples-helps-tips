package icom.libs.jackson.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.NumericNode;

import java.util.Comparator;

public class JsonWithComparatorExample {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        String json1 = "{\"id\":1,\"name\":\"John\"}";
        String json2 = "{\"id\":1.0,\"name\":\"john\"}";

        JsonNode node1 = MAPPER.readTree(json1);
        JsonNode node2 = MAPPER.readTree(json2);

        System.out.println("comparison result: " + node1.equals(getComparator(), node2));
    }

    private static Comparator<JsonNode> getComparator() {
        return (node1, node2) -> {
            if (node1.equals(node2)) {
                return 0;
            }
            if (node1 instanceof NumericNode && node2 instanceof NumericNode
                    && Double.valueOf(node1.asText()).equals(Double.valueOf(node2.asText()))) {
                return 0;
            }
            if (node1.isTextual() && node2.isTextual()
                    && node1.textValue().equalsIgnoreCase(node2.textValue())) {
                return 0;
            }
            return 1;
        };
    }
}

