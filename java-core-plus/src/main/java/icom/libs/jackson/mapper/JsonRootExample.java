package icom.libs.jackson.mapper;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonRootExample {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        String jsonWithRootUser = "{\"user\":{\"id\":1,\"name\":\"John\"}}";
        String jsonUser = "{\"id\":1,\"name\":\"John\"}";

        MAPPER.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);

        User userFromRoot = MAPPER.readValue(jsonWithRootUser, User.class);
        System.out.println("user with root from json: " + userFromRoot);

        System.out.println("\nSimple user without root field fail, reason:");
        User user = null;
        try {
            user = MAPPER.readValue(jsonUser, User.class);
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("user with root from json: " + user);
    }

    @JsonRootName("user")
    private static class User {
        public long id;
        public String name;

        @Override
        public String toString() {
            return "User{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}

