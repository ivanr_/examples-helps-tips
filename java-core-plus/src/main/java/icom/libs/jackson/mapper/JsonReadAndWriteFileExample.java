package icom.libs.jackson.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

public class JsonReadAndWriteFileExample {

    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final File FILE = new File("jackson." + JsonReadAndWriteFileExample.class.getSimpleName() + ".json");

    public static void main(String[] args) throws IOException {
        User user = new User(22, "my name", "my personal last name");
        System.out.println("file exist before saving: " + FILE.exists());
        removeFile();
        createFile();

        MAPPER.writeValue(FILE, user);
        System.out.println("file exist after saving: " + FILE.exists());

        System.out.println("reading file...");
        User userFromFile = MAPPER.readValue(FILE, User.class);
        System.out.println(userFromFile);

        removeFile();
    }

    private static void createFile() throws IOException {
        if (FILE.createNewFile()) {
            System.out.println("new file have been created");
        }
    }

    private static void removeFile() {
        if (FILE.exists() && FILE.delete()) {
            System.out.println("file was removed");
        }
    }

    private static class User {
        public long id;
        public String name;
        public String lastName;
        public String createdAt = LocalDateTime.now().toString();

        public User(long id, String name, String lastName) {
            this.id = id;
            this.name = name;
            this.lastName = lastName;
        }

        @SuppressWarnings("unused") //used by jackson
        public User() {
        }

        @Override
        public String toString() {
            return "User{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", createdAt='" + createdAt + '\'' +
                    '}';
        }
    }
}

