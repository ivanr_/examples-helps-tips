package icom.libs.jackson.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class JsonNodeGetAndPathAndValuesExample {

    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static User user;

    public static void main(String[] args) throws JsonProcessingException {
        prepareUser();

        String jsonUser = MAPPER.writeValueAsString(user);
        System.out.println("json user: " + jsonUser);

        JsonNode node = MAPPER.readTree(jsonUser);
        StringBuilder sb = new StringBuilder("user's internal fields: ");
        node.fieldNames().forEachRemaining(s -> sb.append(s).append(", "));
        System.out.println(sb);

        System.out.println("en name for user with GET: " + node.get("names").get(0).get("naming").get("countryNaming").get("en").toString() + " || note: MAY invoke NullPointerException");
        System.out.println("en name for user with PATH: " + node.path("names").get(0).path("naming").path("countryNaming").path("en").toString());
        System.out.println("en findPath example: " + node.findPath("en"));
        System.out.println("en findValues example: " + node.findValues("en"));

        System.out.println("simplified PATH example:" + getPathsFromNodeAndPath(node, "names", 0, "naming", "countryNaming", "en"));
        System.out.println("simplified GET example:" + getFromNodeAndPath(node, "names", 0, "naming", "countryNaming", "en"));
    }

    private static String getFromNodeAndPath(JsonNode node, Object... args) {
        JsonNode resultNode = node;
        for (Object arg : args) {
            if(resultNode == null) return null;
            if (arg instanceof String) resultNode = resultNode.get((String) arg);
            if (arg instanceof Integer) resultNode = resultNode.get((Integer) arg);
        }
        return resultNode.toString();
    }

    private static String getPathsFromNodeAndPath(JsonNode node, Object... args) {
        JsonNode resultNode = node;
        for (Object arg : args) {
            if (arg instanceof String) resultNode = resultNode.path((String) arg);
            if (arg instanceof Integer) resultNode = resultNode.path((Integer) arg);
        }
        return resultNode.toString();
    }

    private static void prepareUser() {
        Address address = new Address("green avenue", 10, 23412);
        ExternalNaming naming = new ExternalNaming(Collections.singletonMap("en", "jonny"));
        Name name = new Name("john", "silver", naming);
        user = new User(123, Collections.singletonList(name), address);
    }

    private static class User {
        public long id;
        public List<Name> names;
        public Address address;

        public User(long id, List<Name> names, Address address) {
            this.id = id;
            this.names = names;
            this.address = address;
        }

        @Override
        public String toString() {
            return "User{" +
                    "id=" + id +
                    ", names=" + names +
                    ", address=" + address +
                    '}';
        }
    }

    private static class Name {
        @SuppressWarnings("unused")
        public final String en = "default en value, can be found first if fieldName duplicated";
        public String firstName;
        public String lastName;
        public ExternalNaming naming;

        public Name(String firstName, String lastName, ExternalNaming naming) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.naming = naming;
        }

        @Override
        public String toString() {
            return "Name{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", naming=" + naming +
                    '}';
        }
    }

    private static class ExternalNaming {
        public Map<String, String> countryNaming;

        public ExternalNaming(Map<String, String> countryNaming) {
            this.countryNaming = countryNaming;
        }

        @Override
        public String toString() {
            return "ExternalNaming{" +
                    "countryNaming=" + countryNaming +
                    '}';
        }
    }

    private static class Address {
        public String street;
        public long apartmentNo;
        public int zipCode;

        public Address(String street, long apartmentNo, int zipCode) {
            this.street = street;
            this.apartmentNo = apartmentNo;
            this.zipCode = zipCode;
        }

        @Override
        public String toString() {
            return "Address{" +
                    "street='" + street + '\'' +
                    ", apartmentNo=" + apartmentNo +
                    ", zipCode=" + zipCode +
                    '}';
        }
    }
}

