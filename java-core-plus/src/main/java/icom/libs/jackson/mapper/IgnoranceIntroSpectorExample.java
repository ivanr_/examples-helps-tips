package icom.libs.jackson.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;

import java.util.Arrays;

@SuppressWarnings("unused")
public class IgnoranceIntroSpectorExample {

    public static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        MAPPER.setAnnotationIntrospector(new IgnoranceIntrospector());

        System.out.println("json user without MixIn: " + MAPPER.writeValueAsString(new User()));
    }

    private static class User {
        public final long id = 1;
        public final String firstName = "my first name";
        public final String lastName = "my last name";
        public final String fullName = firstName + ", " + lastName;
        public final UserDetails userDetails = new UserDetails();
    }

    private static class UserDetails {
        public final String address = "some address";
        public final String phone = "phoneNumber";
        public final String dateOfBirth = "dob";
    }

    private static class IgnoranceIntrospector extends JacksonAnnotationIntrospector {
        @Override
        public boolean hasIgnoreMarker(AnnotatedMember m) {
            return Arrays.asList("id", "dateOfBirth", "address").contains(m.getName().toLowerCase());
        }
    }
}
