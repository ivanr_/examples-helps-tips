package icom.libs.jackson.mapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class XmlMapperExample {

    private static final XmlMapper MAPPER = new XmlMapper();

    public static void main(String[] args) throws JsonProcessingException {
        Car car = new Car();
        String xmlCar = MAPPER.writeValueAsString(car);
        System.out.println("xmlCar: " + xmlCar);
        Car carFromXml = MAPPER.readValue(xmlCar, Car.class);
        System.out.println("pojo from xml: " + carFromXml);
    }

    private static class Car {
        public final String color = "grey";
        @JsonProperty("type")
        public final String carType = "ravon";
        @JsonUnwrapped
        public final Parameters parameters = new Parameters();

        @Override
        public String toString() {
            return "Car{" +
                    "color='" + color + '\'' +
                    ", carType='" + carType + '\'' +
                    ", parameters=" + parameters +
                    '}';
        }

        private static class Parameters {
            public final byte litresPer100Miles = 15;
            public final byte length = 4;

            @Override
            public String toString() {
                return "Parameters{" +
                        "litresPer100Miles=" + litresPer100Miles +
                        ", length=" + length +
                        '}';
            }
        }
    }
}
