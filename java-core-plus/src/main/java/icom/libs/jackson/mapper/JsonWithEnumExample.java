package icom.libs.jackson.mapper;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Arrays;

public class JsonWithEnumExample {

    public static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        String jsonEnums = MAPPER.writeValueAsString(Arrays.asList(Color.GREEN, Color.RED));
        System.out.println(jsonEnums);
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    private enum Color {
        RED(234, "red colour"),
        GREEN(333, "red colour");

        public int code;
        public String description;

        Color(int code, String description) {
            this.code = code;
            this.description = description;
        }

        @SuppressWarnings("unused") //used by jackson
        @JsonProperty
        String getColor() {
            return this.name();
        }

    }
}
