package icom.libs.jackson.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonNotFailOnUnknownPropertiesExample {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        String jsonAsString = "{\"stringValue\":\"a\",\"booleanValue\":true,\"nextBooleanValue\":true}";
        MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        ValuesHolder holderFromJson = MAPPER.readValue(jsonAsString, ValuesHolder.class);
        System.out.println(holderFromJson);
        System.out.println("property DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES helps to avoid fail when unknown" +
                " field is present in the input");
    }

    private static class ValuesHolder {
        public String stringValue;
        public Boolean booleanValue;
        public Integer intValue;

        @Override
        public String toString() {
            return "ValuesHolder{" +
                    "stringValue='" + stringValue + '\'' +
                    ", booleanValue=" + booleanValue +
                    ", intValue=" + intValue +
                    '}';
        }
    }
}
