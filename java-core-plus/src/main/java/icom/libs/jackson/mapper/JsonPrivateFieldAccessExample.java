package icom.libs.jackson.mapper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collections;
import java.util.List;

@SuppressWarnings({"UnusedDeclaration", "FieldCanBeLocal"})
public class JsonPrivateFieldAccessExample {

    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static User user;

    public static void main(String[] args) throws JsonProcessingException {
        prepareUser();
        MAPPER.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        String jsonUser = MAPPER.writeValueAsString(user);
        System.out.println("json user: " + jsonUser);


    }

    private static void prepareUser() {
        Name name = new Name("john", "silver");
        user = new User(123, Collections.singletonList(name));
    }

    private static class User {
        private final long id;
        private final List<Name> names;

        public User(long id, List<Name> names) {
            this.id = id;
            this.names = names;
        }
    }

    private static class Name {
        public final String firstName;
        private final String lastName;

        public Name(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }
    }
}

