package icom.libs.jackson.mapper;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings("unused")
public class MixInExample {

    public static final ObjectMapper MAPPER1 = new ObjectMapper();
    public static final ObjectMapper MAPPER2 = new ObjectMapper();

    public static void main(String[] args) throws JsonProcessingException {
        System.out.println("json user without MixIn: " + MAPPER1.writeValueAsString(new User()));

        MAPPER2.addMixIn(User.class, UserMixIn.class);
        System.out.println("json user with MixIn: " + MAPPER2.writeValueAsString(new User()));
    }

    private static class User {
        public final long id = 1;
        public final String firstName = "my first name";
        public final String lastName = "my last name";
        public final String fullName = firstName + ", " + lastName;
    }

    private static class UserMixIn {
        @JsonIgnore
        public final long id = 1;
        @JsonIgnore
        public String firstName;
        @JsonIgnore
        public String lastName;
    }
}
