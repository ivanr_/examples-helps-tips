package icom.google.cloud.platform.abstracts.rest.crud;

import icom.google.cloud.platform.abstracts.querymodel.PathComposer;
import icom.google.cloud.platform.abstracts.querymodel.QueryToGCT;

public interface CreateData {
    void putDataAtRemote(PathComposer path, QueryToGCT query);
}
