package icom.google.cloud.platform.abstracts.rest.crud;

import icom.google.cloud.platform.abstracts.querymodel.PathComposer;
import icom.google.cloud.platform.abstracts.querymodel.QueryToGCT;

public interface DeleteData {
    boolean deleteWholeTableFromRemote(PathComposer path);
    boolean deleteRecordsFromRemoteTable(PathComposer path, QueryToGCT query);
}
