package icom.google.cloud.platform;

import icom.google.cloud.platform.abstracts.querymodel.PathComposer;
import icom.google.cloud.platform.abstracts.querymodel.QueryToGCT;
import icom.google.cloud.platform.abstracts.rest.crud.GetData;
import icom.google.cloud.platform.impl.querymodel.PathComposerImpl;
import icom.google.cloud.platform.impl.querymodel.QueryToGCPImpl;
import icom.google.cloud.platform.impl.rest.crud.CreateNewSourceTable;
import icom.google.cloud.platform.impl.rest.crud.DeleteDataImpl;
import icom.google.cloud.platform.impl.rest.crud.GetDataImpl;

import java.util.ArrayList;
import java.util.List;

public class GCPMainClass {
    static List<String> rows = new ArrayList<>();
    static PathComposer path = new PathComposerImpl("bigquery-public-data",
            "austin_311",
            "311_service_requests");
    static GetData getFromRemote = new GetDataImpl(50);
    static QueryToGCT query = new QueryToGCPImpl(rows);
    static DeleteDataImpl deleteDataImpl = new DeleteDataImpl();


    public static void main(String[] args) throws InterruptedException {
        String fullPath = "`locations.new_table_from_api`";
        exampleTableCreatingRequest(fullPath);
        exampleGetRequest();
        exampleDeleteTableFromRemote(fullPath);

        System.out.println("last breakpoint");
    }

    static void exampleGetRequest() {
//        getting data from remote
        GCPMainClass.path.setPath("bigquery-public-data",
                "austin_311",
                "311_service_requests");
        rows.removeAll(rows);
        rows.add("*");
        System.out.println("Get request example");
        getFromRemote.getDataFromRemote(path, query);
    }

    static void exampleTableCreatingRequest(String inputedPath) {
//        creating new table at remote
        System.out.println("Creating table");
        CreateNewSourceTable createNewSourceTable = new CreateNewSourceTable();
        path.setPath(inputedPath);
        rows.removeAll(rows);
        rows.add("some_name STRING");
        rows.add("some_id INT64");
        query.setRows(rows);
        createNewSourceTable.putDataAtRemote(path, query);
//        getting data from created
        System.out.println("Getting data from created EMPTY remote table");
        getFromRemote.getDataFromRemote(path, query);
    }

    static void exampleDeleteTableFromRemote(String inputedPath) {
//        deleting whole table
        path.setPath(inputedPath);
        deleteDataImpl.deleteWholeTableFromRemote(path);
    }
}

