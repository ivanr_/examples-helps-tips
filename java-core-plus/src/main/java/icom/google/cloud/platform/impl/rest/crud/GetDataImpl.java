package icom.google.cloud.platform.impl.rest.crud;

import icom.google.cloud.platform.abstracts.querymodel.PathComposer;
import icom.google.cloud.platform.abstracts.querymodel.QueryToGCT;
import icom.google.cloud.platform.abstracts.rest.crud.GetData;
import icom.google.cloud.platform.api.DefaultJob;
import icom.google.cloud.platform.api.GCPJobProvider;

public class GetDataImpl implements GetData {
    private int limit = 0;

    public GetDataImpl() {
    }

    public GetDataImpl(int limitOutputRecords) {
        this.limit = limitOutputRecords;
    }

    @Override
    public void getDataFromRemote(PathComposer path, QueryToGCT query) {
        String request = "SELECT " + query.getRows()
                + " FROM " + path.getFullPath()
                +  (limit == 0 ? "" : " LIMIT " + limit);
        executeGCPJob(request);
    }

    private void executeGCPJob(String request){
        new GCPJobProvider(new DefaultJob()).executeGCPJobByRequest(request);
    }
}
