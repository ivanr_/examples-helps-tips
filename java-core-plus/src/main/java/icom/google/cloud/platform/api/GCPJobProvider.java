package icom.google.cloud.platform.api;

public class GCPJobProvider {
    private DefaultJob job;

    public GCPJobProvider(DefaultJob job) {
        this.job = job;
    }

    public void executeGCPJobByRequest(String request){
        job.getDataFromTable(request);
    }
}
