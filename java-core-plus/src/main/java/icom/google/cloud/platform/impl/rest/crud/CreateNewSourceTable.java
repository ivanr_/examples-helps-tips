package icom.google.cloud.platform.impl.rest.crud;

import icom.google.cloud.platform.abstracts.querymodel.PathComposer;
import icom.google.cloud.platform.abstracts.querymodel.QueryToGCT;
import icom.google.cloud.platform.abstracts.rest.crud.CreateData;
import icom.google.cloud.platform.api.DefaultJob;
import icom.google.cloud.platform.api.GCPJobProvider;

public class CreateNewSourceTable implements CreateData {

    private void executeGCPJob(String request) {
        new GCPJobProvider(new DefaultJob()).executeGCPJobByRequest(request);
    }

    @Override
    public void putDataAtRemote(PathComposer path, QueryToGCT query) {
        String request =
                "CREATE TABLE " +
                        path.getFullPath() +
                        " ( " + query.getRows() + " )";
        executeGCPJob(request);
    }
}
