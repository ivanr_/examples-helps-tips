package icom.google.cloud.platform.abstracts.querymodel;

abstract public class PathComposer {
    protected String fullPath;
    protected String pathSeparator = ".";

    protected PathComposer(String fullPath) {
        this.fullPath = fullPath;
    }

    protected PathComposer() {
    }

    abstract public boolean setPath(String tablesLocationPath, String dataSetId);

    abstract public boolean setPath(String tablesLocationPath, String dataSetId, String tableId);

    public abstract boolean setPath(String fullPath);

    public final String getFullPath() {
        return fullPath;
    }
}
