package icom.google.cloud.platform.impl.querymodel;

import icom.google.cloud.platform.abstracts.querymodel.QueryToGCT;

import java.util.List;

public class QueryToGCPImpl implements QueryToGCT {
    private List<String> rows;

    public QueryToGCPImpl(List<String> rowsList) {
        if (rowsList != null) {
            this.rows = rowsList;
        } else {
            throw new IllegalArgumentException("Inputed request is NULL");
        }
    }

    @Override
    public String getRows() {
        return rows.toString().substring(1, rows.toString().length() - 1);
    }

    @Override
    public boolean setRows(List<String> rows) {
        this.rows = rows;
        return true;
    }
}
