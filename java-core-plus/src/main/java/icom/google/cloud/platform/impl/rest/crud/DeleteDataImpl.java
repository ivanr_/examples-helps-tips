package icom.google.cloud.platform.impl.rest.crud;

import icom.google.cloud.platform.abstracts.querymodel.PathComposer;
import icom.google.cloud.platform.abstracts.querymodel.QueryToGCT;
import icom.google.cloud.platform.abstracts.rest.crud.DeleteData;
import icom.google.cloud.platform.api.DefaultJob;
import icom.google.cloud.platform.api.GCPJobProvider;

public class DeleteDataImpl implements DeleteData {
    @Override
    public boolean deleteWholeTableFromRemote(PathComposer path) {
        String request =
                "DROP TABLE " + path.getFullPath();
        executeGCPJob(request);
        return true;
    }

    @Override
    public boolean deleteRecordsFromRemoteTable(PathComposer path, QueryToGCT query) {
        return false;
    }

    private void executeGCPJob(String request) {
        new GCPJobProvider(new DefaultJob()).executeGCPJobByRequest(request);
    }
}
