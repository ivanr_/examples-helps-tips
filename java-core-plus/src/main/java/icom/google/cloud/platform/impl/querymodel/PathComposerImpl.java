package icom.google.cloud.platform.impl.querymodel;

import icom.google.cloud.platform.abstracts.querymodel.PathComposer;

public class PathComposerImpl extends PathComposer {
    public PathComposerImpl(String fullPath) {
        super(fullPath);
    }

    public PathComposerImpl(String tablesLocationPath, String dataSetId, String tableId) {
        setPath(tablesLocationPath, dataSetId, tableId);
    }

    @Override
    public boolean setPath(String tablesLocationPath, String dataSetId, String tableId) {
        if (pathSeparator == null) {
            pathSeparator = ".";
        }
        fullPath = "`" + tablesLocationPath;
        if (!dataSetId.equals("")) {
            fullPath += pathSeparator + dataSetId;
        }
        if (!dataSetId.equals("")) {
            fullPath += pathSeparator + tableId + "`";
        }
        return true;
    }

    @Override
    public boolean setPath(String tablesLocationPath, String dataSetId) {
        if (pathSeparator == null) {
            pathSeparator = ".";
        }
        fullPath = "`" + tablesLocationPath + pathSeparator + dataSetId + "`";
        return true;
    }

    @Override
    public boolean setPath(String fullPath) {
        this.fullPath = fullPath;
        return true;
    }
}
