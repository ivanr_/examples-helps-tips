package icom.google.cloud.platform.abstracts.rest.crud;

import icom.google.cloud.platform.abstracts.querymodel.PathComposer;
import icom.google.cloud.platform.abstracts.querymodel.QueryToGCT;

public interface AddData {
    boolean postDataAtRemote(PathComposer path, QueryToGCT query);
}
