package icom.google.cloud.platform.abstracts.querymodel;

import java.util.List;

public interface QueryToGCT {
    public String getRows();
    public boolean setRows(List<String> rows);
}
