package icom.google.cloud.platform.api;

import com.google.cloud.bigquery.*;

import java.util.UUID;

public class TableBuilderQuery {
    public void create(String datasetName, String tableName, String fieldName) {
        TableId tableId = TableId.of(datasetName, tableName);
// Table field definition
        Field field = Field.of(fieldName, LegacySQLTypeName.STRING);
// Table schema definition
        Schema schema = Schema.of(field);
        TableDefinition tableDefinition = StandardTableDefinition.of(schema);
        TableInfo tableInfo = TableInfo.newBuilder(tableId, tableDefinition).build();
        BigQuery bigquery = BigQueryOptions.getDefaultInstance().getService();
        Table table = bigquery.create(tableInfo);
    }

    public void createTable() {
        BigQuery bigquery = BigQueryOptions.getDefaultInstance().getService();
        QueryJobConfiguration queryConfig =
                QueryJobConfiguration.newBuilder(
                        "CREATE TABLE " +
                                "locations.t2 " +
                                "(column_name STRING," +
                                " description STRING," +
                                "date DATE)"
                )
                        // Use standard SQL syntax for queries.
                        // See: https://cloud.google.com/bigquery/sql-reference/
                        .setUseLegacySql(false)
                        .build();
//        return true;
// Create a job ID so that we can safely retry.
        JobId jobId = JobId.of(UUID.randomUUID().toString());
        Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
//
// Wait for the query to complete.
        try {
            queryJob = queryJob.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BigQueryException e) {
            System.out.println("Found bigquery exception:");
            System.out.println(e.getMessage());
        }

// Check for error
        if (queryJob == null) {
            throw new RuntimeException("Job no longer exists");
        } else if (queryJob.getStatus().getError() != null) {
            // You can also look at queryJob.getStatus().getExecutionErrors() for all
            // errors, not just the latest one.
            throw new RuntimeException(queryJob.getStatus().getError().toString());
        }
        System.out.println("New table created");
    }
}
