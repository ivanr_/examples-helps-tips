package icom.google.cloud.platform.api;

import com.google.cloud.bigquery.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DefaultJob {
    public void getDataFromTable(String request) {
        BigQuery bigquery = BigQueryOptions.getDefaultInstance().getService();
        QueryJobConfiguration queryConfig =
                QueryJobConfiguration.newBuilder(
//                        "SELECT "
//                                + "CONCAT('https://stackoverflow.com/questions/', CAST(id as STRING)) as url, "
//                                + "view_count "
//                                + "FROM `bigquery-public-data.stackoverflow.posts_questions` "
//                                + "WHERE tags like '%google-bigquery%' "
//                                + "ORDER BY favorite_count DESC LIMIT 10")
//                        "SELECT "
//                                + "homeTeamName "
//                                + "FROM `bigquery-public-data.baseball.schedules`"
//                                + "ORDER BY homeTeamName DESC LIMIT 50 "
                        request
                )
                        // Use standard SQL syntax for queries.
                        // See: https://cloud.google.com/bigquery/sql-reference/
                        .setUseLegacySql(false)
                        .build();

// Create a job ID so that we can safely retry.
        JobId jobId = JobId.of(UUID.randomUUID().toString());
        Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());

// Wait for the query to complete.
        try {
            queryJob = queryJob.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BigQueryException e) {
            System.out.println("Completing Query Job:");
            System.out.println(e.getMessage());
        }

// Check for errors
        if (queryJob == null) {
            throw new RuntimeException("Job no longer exists");
        } else if (queryJob.getStatus().getError() != null) {
            // You can also look at queryJob.getStatus().getExecutionErrors() for all
            // errors, not just the latest one.
            throw new RuntimeException(queryJob.getStatus().getError().toString());
        }

        // Get the results.
//        QueryResponse response = bigquery.getQueryResults(jobId);

        TableResult result = null;
        try {
            result = queryJob.getQueryResults();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }catch (BigQueryException e) {
            System.out.println("QueryResultException:");
            System.out.println(e.getMessage());
        }

// Print all pages of the results.

        TableResult finalResult = result;
        System.out.println("Row numbers at table is " + (finalResult != null ? finalResult.getTotalRows() : 0));
        List<String> output = new ArrayList<>();
        if (finalResult != null) {
            for (FieldValueList res : finalResult.getValues()) {
                FieldValue value = res.get(0);
                output.add((String) value.getValue());
                String outputLine = "";
                for (int countValue = 1; countValue < res.size(); countValue++) {
                    try {
                        outputLine += res.get(countValue).getValue().toString() + ",";
                    } catch (NullPointerException npe) {
                        outputLine += "NPE" + ",";
                    }
                }
                outputLine = outputLine.substring(0, outputLine.length() - 1);
                System.out.println(outputLine);
            }
        }
    }
}
