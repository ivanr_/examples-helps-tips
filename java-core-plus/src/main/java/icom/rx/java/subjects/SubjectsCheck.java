package icom.rx.java.subjects;

import io.reactivex.functions.Consumer;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;


public class SubjectsCheck {
    public static void main(String[] args) {
        launchPublishSubject();

        launchBehaviorSubject();

        launchReplaySubject();
    }

    private static void launchBehaviorSubject() {
        System.out.println("\nBehavior subject");
        final StringBuilder result1 = new StringBuilder();
        final StringBuilder result2 = new StringBuilder();
        BehaviorSubject<String> subject =  BehaviorSubject.create();
        subject.subscribe(result1::append);
        subject.onNext("a");
        subject.onNext("b");
        subject.subscribe(result2::append);
        subject.onNext("c");
        subject.onNext("d");
        subject.onComplete();
        System.out.println("result1: " + result1);
        System.out.println("result2:" + result2);
    }

    private static void launchPublishSubject() {
        System.out.println("\nPublish subject");
        final StringBuilder result1 = new StringBuilder();
        final StringBuilder result2 = new StringBuilder();
        final Consumer<String> consumer = result1::append;
        PublishSubject<String> subject = PublishSubject.create();
        subject.subscribe(consumer);
        subject.onNext("a");
        subject.onNext("b");
        subject.subscribe(result2::append);
        subject.onNext("c");
        subject.onNext("d");
        subject.onComplete();

        System.out.println("result1: " + result1);
        System.out.println("result2: " + result2);
    }

    private static void launchReplaySubject(){
        System.out.println("\nReplay subject");

        final StringBuilder result1 = new StringBuilder();
        final StringBuilder result2 = new StringBuilder();

        ReplaySubject<String> subject = ReplaySubject.create();
        subject.subscribe(result1::append);
        subject.onNext("a");
        subject.onNext("b");
        subject.onNext("c");
        subject.subscribe(result2::append);
        subject.onNext("d");
        subject.onComplete();

        System.out.println("result1: " + result1);
        System.out.println("result2: " + result2);
    }
}
