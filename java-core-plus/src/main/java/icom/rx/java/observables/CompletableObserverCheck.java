package icom.rx.java.observables;

import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class CompletableObserverCheck {
    public static void main(String[] args) throws InterruptedException {
        Disposable disposable = Completable.complete()
                .delay(2, TimeUnit.SECONDS, Schedulers.io())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                    @Override
                    public void onStart() {
                        System.out.println("Started!");
                    }
                    @Override
                    public void onComplete() {
                        System.out.println("Done!");
                    }
                });
        System.out.println("check started at " + new Date().toString());
        Thread.sleep(3000);
        System.out.println("observing started at " + new Date().toString());
        disposable.dispose();
        System.out.println("observing finished at " + new Date().toString());
    }
}
