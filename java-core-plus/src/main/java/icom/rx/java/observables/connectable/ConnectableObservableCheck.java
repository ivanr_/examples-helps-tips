package icom.rx.java.observables.connectable;

import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;
import rx.schedulers.TestScheduler;
import rx.subjects.TestSubject;

public class ConnectableObservableCheck {
    public static void main(String[] args) {
        launchConnect();

        launchReplay();


    }

    private static void launchReplay() {
        System.out.println("\nReplay");
        final TestScheduler scheduler = new TestScheduler();
        final TestSubject<Integer> subject = TestSubject.create(scheduler);

        final rx.observables.ConnectableObservable<Integer> sums = subject.scan((a, b) -> {
            System.out.println(a+b);
            return a + b;
        }).replay(0);
        sums.connect();
        System.out.println("before sequence");
        subject.onNext(1);
        subject.onNext(2);
        subject.onNext(3);
        subject.onNext(4);
        scheduler.triggerActions();
    }

    private static void launchConnect() {
        System.out.println("\nConnectable+publish");
        ConnectableObservable<Integer> connectableObservable =
                Observable
                        .just(4, 3, 2, 1)
                        .doOnNext(n -> Thread.sleep(500))
                        .publish();
        connectableObservable.doOnNext(System.out::println);
        System.out.println("before first connect");
        connectableObservable.connect();
        System.out.println("before subscribe");
        connectableObservable.subscribe(n -> System.out.println("next goes " + n));
        System.out.println("before second connect");
        connectableObservable.connect();
    }

}
