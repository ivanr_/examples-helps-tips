package icom.rx.java.observables;

import io.reactivex.Flowable;

public class TouchTest {

    public static void main(String[] args) {
        Flowable
                .just("Hi from reactive java!")
                .subscribe(System.out::println);
    }
}
