package icom.rx.java.observables;

import io.reactivex.Maybe;
import io.reactivex.observers.DisposableMaybeObserver;
import io.reactivex.schedulers.Schedulers;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DisposableMaybeObserverCheck {
    public static void main(String[] args) throws InterruptedException {

        DisposableMaybeObserver<String> disposableMaybeObserver = Maybe.just("Check message")
                .delay(2, TimeUnit.SECONDS, Schedulers.io())
                .subscribeWith(new DisposableMaybeObserver<String>() {
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onSuccess(String value) {
                        System.out.println("value showed in custom order when output because of reactive concurrency:\n" + value);
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Done!");
                    }
                });
        Thread.sleep(3000);
        System.out.println("check started at " + new Date().toString());
        System.out.println("observer is disposed: " + disposableMaybeObserver.isDisposed());
        Thread.sleep(3000);
        System.out.println("observing started at " + new Date().toString());
        disposableMaybeObserver.dispose(); //there will be only one value
        System.out.println("observing finished at " + new Date().toString());
        System.out.println("observer is disposed: " + disposableMaybeObserver.isDisposed());
    }
}
