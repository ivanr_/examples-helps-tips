package icom.rx.java.observables;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class SingleObservableCheck {
    public static void main(String[] args) throws InterruptedException {
        Single<String> check_message = Single.just("Check message");

        DisposableSingleObserver<String> disposableSingleObserver = check_message
                .delay(2, TimeUnit.SECONDS, Schedulers.io())
                .subscribeWith(new DisposableSingleObserver<String>() {
                    @Override
                    public void onSuccess(String s) {
                        System.out.println(s);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                });
        System.out.println("check started at " + new Date().toString());
        Thread.sleep(3000);
        System.out.println("observing started at " + new Date().toString());
        disposableSingleObserver.dispose(); //there will be only one value
        System.out.println("observing finished at " + new Date().toString());
        disposableSingleObserver.dispose(); //there will be no value
    }
}
