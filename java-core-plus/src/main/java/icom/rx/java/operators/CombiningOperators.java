package icom.rx.java.operators;

import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.List;

public class CombiningOperators {
    public static void main(String[] args) {
        launchCombineLatestOperator();

        launchMergeOperator();

        launchZipOperator();
    }

    private static void launchZipOperator() {
        System.out.println("\nZip operator");
        List<String> zippedStrings = new ArrayList<>();

        Observable<String> observable = Observable.zip(
                Observable.fromArray("Simple", "To", "String", "Ignored data"),
                Observable.fromArray("Operation", "Zip", "Array"),
                (str1, str2) -> str1 + " " + str2)
                .doOnNext(zippedStrings::add);
        observable.subscribe();
        System.out.println(zippedStrings);
    }

    private static void launchMergeOperator() {
        System.out.println("\nMerge operator");
        StringBuilder result = new StringBuilder();

        Observable<String> observable = Observable
                .merge(
                        Observable.fromArray("Hello ", "World "),
                        Observable.fromArray("I love ", "RxJava ")
                )
                .doOnNext(result::append);

        observable.subscribe();
        System.out.println(result);
    }

    private static void launchCombineLatestOperator() {
        System.out.println("\nCombineLatest operator");
        Integer[] numbers = {1, 2, 3, 4, 5, 6};
        String[] letters = {"a", "b", "c", "d", "e", "f", "g"};
        final StringBuilder result = new StringBuilder();
        Observable<String> observable1 = Observable.fromArray(letters);
        Observable<Integer> observable2 = Observable.fromArray(numbers);
        Observable<String> observable = Observable.combineLatest(observable2, observable1, (a, b) -> a + b)
                .doOnNext(result::append);
        observable.subscribe();
        System.out.println("result: " + result);
    }
}
