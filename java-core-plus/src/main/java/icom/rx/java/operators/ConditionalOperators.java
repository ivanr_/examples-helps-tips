package icom.rx.java.operators;

import io.reactivex.Observable;
import io.reactivex.Single;

public class ConditionalOperators {
    public static void main(String[] args) {
        launchContains();

        launchAllOperator();

        launchDefaultIfEmpty();

        launchSequenceEqual();

        launchSkipUntil();
    }

    private static void launchSkipUntil() {
        System.out.println("\nSkipUntil operator");
        Observable<String> initObserver = Observable.just("started")
                .doOnNext(System.out::println);
        Observable.just("delayed text")
                .skipUntil(initObserver)
                .doOnNext(System.out::println)
                .subscribe().dispose();
    }

    private static void launchAllOperator() {
        System.out.println("\nAll operator");
        Single<Boolean> contains = Observable.just(1, 2, 3, 4, 5)
                .all(number -> number > 2 && number < 5)
                .doOnSuccess(s -> System.out.println("contains all numbers > 2 and < 5: " + s));
        contains.subscribe();
        Single<Boolean> contains1 = Observable.just(1, 2, 3, 4, 5)
                .all(number -> number > 0 && number < 6)
                .doOnSuccess(s -> System.out.println("contains all numbers > 0 and < 6: " + s));
        contains1.subscribe();
    }

    private static void launchContains() {
        System.out.println("\nContains operator");
        Single<Boolean> contains = Observable.just(1, 2, 3, 4, 5)
                .contains(1)
                .doOnSuccess(s -> System.out.println("sequence contains number 1: " + s));
        contains.subscribe();
    }

    private static void launchDefaultIfEmpty() {
        System.out.println("\nDefaultIfEmpty operator");
        Observable.empty()
                .defaultIfEmpty(123)
                .subscribe(System.out::println).dispose();
    }

    private static void launchSequenceEqual() {
        System.out.println("\nSequenceEqual operator");
        Observable
                .sequenceEqual(Observable.just(1, 3, 4), Observable.just(1, 5, 4))
                .subscribe(s -> System.out.println("sequences are the same: " + s))
                .dispose();
    }
}
