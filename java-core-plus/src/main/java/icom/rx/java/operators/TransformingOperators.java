package icom.rx.java.operators;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.TestScheduler;

import java.util.AbstractMap;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class TransformingOperators {
    public static void main(String[] args) {
        String[] letters = {"a", "b", "c", "d", "e", "f", "g", "a", "b"};

        launchMapOperator(letters);

        launchFlatMapOperator(letters);

        launchGroupByOperator();

        launchScanOperator();
    }

    private static void launchScanOperator() {
        System.out.println("\nScan operator");
        String[] letters = {"a", "b", "c", "d"};
        final String[] result = {""};
        Observable.fromArray(letters)
                .scan(new StringBuilder(), StringBuilder::append)
                .subscribe(total -> result[0] += total.toString());
        System.out.println("result: " + result[0]);
    }

    private static void launchGroupByOperator() {
        System.out.println("\nGroupBy operator");
        int threshold = 300;
        Observable<AbstractMap.SimpleEntry<String, Integer>> data = Observable.just(
                new AbstractMap.SimpleEntry<>("number1", 123),
                new AbstractMap.SimpleEntry<>("number5", 321),
                new AbstractMap.SimpleEntry<>("number3", 345),
                new AbstractMap.SimpleEntry<>("number4", 456),
                new AbstractMap.SimpleEntry<>("number6", 321),
                new AbstractMap.SimpleEntry<>("number2", 234),
                new AbstractMap.SimpleEntry<>("number7", 432)
        );
        data
                .groupBy(groupKey -> groupKey.getValue() > threshold)
                .subscribe(gr -> {
                            if (gr.getKey().equals(Boolean.TRUE)) {
                                System.out.println("true key touched");
                                gr.subscribe(v -> System.out.println("more than " + threshold + ": " + v));
                            } else {
                                if (gr.getKey().equals(Boolean.FALSE)) {
                                    System.out.println("false key touched");
                                    gr.subscribe(v -> System.out.println("less than " + threshold + ": " + v));
                                }
                            }

                        }
                );
        data.subscribe();
    }

    private static void launchFlatMapOperator(String[] letters) {
        System.out.println("\nFlatMap operator");
        TestScheduler scheduler = new TestScheduler();
        Date startTime = new Date();
        Observable.fromArray(letters)
                .flatMap(
                        l -> Observable.just(l + "X").delay(new Random().nextInt(2), TimeUnit.SECONDS, scheduler)
                )
                .toList()
                .doOnSuccess(letter -> System.out.println("from onSuccess: " + letter))
                .subscribe(lettersS -> System.out.println("from subscribe: " + lettersS));
        System.out.println("scheduler launch, time: " + (new Date().getTime() - startTime.getTime()) + " ms");
        scheduler.advanceTimeBy(1, TimeUnit.MINUTES);

        System.out.println("\nConcat map (check output order)");
        startTime = new Date();
        Observable.fromArray(letters)
                .concatMap(
                        l -> Observable.just(l + "X").delay(new Random().nextInt(2), TimeUnit.SECONDS, scheduler)
                )
                .toList()
                .doOnSuccess(letter -> System.out.println("from onSuccess: " + letter))
                .subscribe(lettersS -> System.out.println("from subscribe: " + lettersS));
        System.out.println("scheduler launch, time: " + (new Date().getTime() - startTime.getTime()) + " ms");
        scheduler.advanceTimeBy(1, TimeUnit.MINUTES);
    }

    private static void launchMapOperator(String[] letters) {
        Integer[] numbers = {1, 2, 3, 4, 5};
        final StringBuilder result = new StringBuilder();
        Observable<String> stringObservable = Observable.fromArray(letters);
        Observable<Integer> intObservable = Observable.fromArray(numbers);

        System.out.println("Map operator");
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        compositeDisposable.addAll(
                stringObservable
                        .map(String::toUpperCase)
                        .distinct()
                        .subscribe(result::append),
                intObservable
                        .map(i -> i + 10)
                        .subscribe(System.out::println)
        );
        System.out.println("letters result: " + result);
        System.out.println("composite size: " + compositeDisposable.size() + ", is disposed: " + compositeDisposable.isDisposed());
        compositeDisposable.dispose();
        System.out.println("composite size: " + compositeDisposable.size() + ", is disposed: " + compositeDisposable.isDisposed());
    }
}
