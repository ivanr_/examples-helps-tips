package icom.rx.java.operators;

import rx.Observable;
import rx.observables.MathObservable;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class MathOperators {

    private final static Observable<Integer> INTEGER_OBSERVABLE = Observable.just(1, 2, 3, 4, 5);

    public static void main(String[] args) {
        launchMinOperator();

        launchAverageOperator();

        launchMaxOperator();

        launchSumOperator();

        launchReduceOperator();

        launchCollectOperator();
    }

    private static void launchMaxOperator() {
        System.out.println("\nMax operator");
        MathObservable.max(INTEGER_OBSERVABLE)
                .subscribe(s -> System.out.println("max value: " + s));
    }

    private static void launchAverageOperator() {
        System.out.println("\nAverage operator");
        MathObservable
                .averageInteger(INTEGER_OBSERVABLE)
                .subscribe(s -> System.out.println("an average value: " + s));
    }

    private static void launchMinOperator() {
        System.out.println("\nMin operator");
        MathObservable.min(INTEGER_OBSERVABLE)
                .subscribe(s -> System.out.println("min value: " + s));
    }

    private static void launchSumOperator(){
        System.out.println("\nSum operator");
        MathObservable.sumInteger(INTEGER_OBSERVABLE)
                .subscribe(s -> System.out.println("sum value: " + s));
    }

    private static void launchReduceOperator(){
        System.out.println("\nReduce operator");
        Observable.just(1, 2, 3, 4)
                .reduce((integer1, integer2) -> {
                    System.out.println(integer1 + " " + integer2);
                    return integer1*2 + integer2;
                })
                .subscribe(s -> System.out.println("reduce value: " + s));
    }

    private static void launchCollectOperator(){
        System.out.println("\nCollect operator");

        List<String> list = Arrays.asList("A", "B", "C", "B", "B", "A", "D");
        Observable<HashSet<String>> reduceListObservable = Observable
                .from(list)
                .collect(HashSet::new, HashSet::add);
        reduceListObservable.subscribe(set -> System.out.println("hashSet" + set));
    }
}
