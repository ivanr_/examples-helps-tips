package icom.rx.java.operators;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class FilteringOperators {
    public static void main(String[] args) throws InterruptedException {
        launchFilterOperator();

        launchDebounceOperator();

        launchIgnoreElementOperator();

        launchSkipOperator();

        launchSkipLast();
    }

    private static void launchSkipLast() {
        System.out.println("\nSkipLast operator");
        Observable.range(0, 7)
                .skipLast(3)
                .doOnNext(number -> System.out.println("after skipLast: " + number))
                .subscribe();
        System.out.println("\nSkipLast operator");
    }

    private static void launchSkipOperator() {
        System.out.println("\nSkip operator");
        Observable.range(0, 7)
                .skip(2)
                .doOnNext(number -> System.out.println("after skip: " + number))
                .subscribe();
    }

    private static void launchIgnoreElementOperator() {
        System.out.println("\nIgnoreElement operator");
        Observable<Integer> observable = Observable.just(11, 22, 33)
                .doOnNext(s -> System.out.println("without ignore operator: " + s))
                .doOnComplete(() -> System.out.println("done without ignore"));

        observable
                .doOnComplete(() -> System.out.println("done WITH ignore"))
                .ignoreElements()
                .subscribe();
    }

    private static void launchDebounceOperator() throws InterruptedException {
        System.out.println("\nDebounce operator");

        Observable
                .just("a", "b", "c", "d")
                .doOnNext(s -> System.out.println("before debounce: " + s))
                .debounce(1, TimeUnit.SECONDS)
                .subscribe(s -> System.out.println("after debounce: " + s));

        Observable
                .just("a", "b", "c")
                .delay(2, TimeUnit.SECONDS, Schedulers.io())
                .doOnNext(s -> System.out.println("before debounce: " + s))
                .debounce(1, TimeUnit.SECONDS)
                .subscribe(s -> System.out.println("after debounce: " + s));
        System.out.println("before sleep");
        Thread.sleep(3000);
        System.out.println("after sleep");

        Observable.just(Arrays.asList(11, 22, 33))
                .flatMap(Observable::fromIterable)
                .doOnNext(number -> System.out.println("before debounce: " + number))
                .debounce(1, TimeUnit.MILLISECONDS)
                .subscribe(s -> System.out.println("after debounce: " + s));


    }

    private static void launchFilterOperator() {
        System.out.println("\nFilter operator");

        Observable<Integer> sourceObservable = Observable.just(12, 31, 25, 51);
        sourceObservable
                .doOnNext(v -> System.out.println("doOnNext: " + v))
                .filter(v -> v > 30)
                .subscribe(v -> System.out.println("doOnSubscribe: " + v));
    }
}
