package icom.rx.java.operators;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.observers.DisposableLambdaObserver;
import io.reactivex.internal.operators.maybe.MaybeCallbackObserver;
import io.reactivex.observers.DisposableMaybeObserver;
import io.reactivex.schedulers.Schedulers;
import org.reactivestreams.Subscription;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class CreatingOperators {
    public static void main(String[] args) throws InterruptedException, IOException {
        String[] letters = {"a", "b", "c", "d", "e", "f", "g"};
        final StringBuilder reactiveResult = new StringBuilder();
        Observable<String> observable = Observable.fromArray(letters);

        Date startDate = new Date();
        observable
                .map(String::toUpperCase)
                .subscribe(reactiveResult::append);
        System.out.println("appending time for observers: " + (new Date().getTime() - startDate.getTime()) + " ms");
        System.out.println("observers results: " + reactiveResult);

//        to compare with stream
        startDate = new Date();
        final StringBuilder streamResult = new StringBuilder();
        Arrays.stream(letters)
                .map(String::toUpperCase)
                .forEach(streamResult::append);
        System.out.println("appending time for stream: " + (new Date().getTime() - startDate.getTime()) + " ms");
        System.out.println("streams results: " + streamResult);

        StringBuilder consoleOutput = new StringBuilder();
        Observable.range(5, 4).subscribe(
                s -> {
                    consoleOutput.append(s);
                    System.out.println(s);}
                    );
        System.out.println("appended data; " + consoleOutput);

        System.out.println("\nDefer usage");
        Observable<Long> deferObservable = Observable.defer(
                () -> Observable.just(System.currentTimeMillis())
        );
        deferObservable.subscribe(l -> System.out.println("current time in ms: " + l));
        Thread.sleep(1000);
        deferObservable.subscribe(l -> System.out.println("current time in ms: " + l));

        System.out.println("\nCreate usage");
        Observable<Long> createObservable = Observable.create(
                l -> {
                    l.onNext(System.currentTimeMillis());
                    l.onComplete();
                }
        );
        createObservable.subscribe(
                value -> System.out.println("got value: " + value),
                error -> System.out.println("error occured: " + error),
                () -> System.out.println("complete")
        );
        Thread.sleep(1000);
        createObservable.subscribe(l -> System.out.println("current time in ms: " + l));

        System.out.println("\nInterval usage");
//        Observable<Long> interval = Observable.interval(0, 1000, TimeUnit.MILLISECONDS);
//        interval.subscribe(
//                v -> System.out.println("Received: " + v),
//                e -> System.out.println("Error: " + e),
//                () -> System.out.println("Completed")
//        );
//        System.in.read();
//        interval.blockingSubscribe();

        System.out.println("\nTimer usage");
        Observable<Long> timer = Observable.timer(1, TimeUnit.SECONDS, Schedulers.io());
        timer.subscribe(
                v -> System.out.println("Received: " + v),
                e -> System.out.println("Error: " + e),
                () -> System.out.println("Completed")
        );
        System.in.read();


    }
}
