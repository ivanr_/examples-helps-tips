package icom.testing.junit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Testing {
    public static void checkStandartSortUsing() {
        List<String> toBeSortedString = new ArrayList<>();
        List<Integer> toBeSortedInt = new ArrayList<>();
        toBeSortedString.add("one");
        toBeSortedString.add("two");
        toBeSortedString.add("three");
        toBeSortedString.add("four");

        toBeSortedInt.add(123);
        toBeSortedInt.add(11);
        toBeSortedInt.add(354);
        toBeSortedInt.add(984);

        SortClass sortClass = new SortClass(new SorterFactory() {
            @Override
            public Comparator getComparator() {
                return null;
            }
        });
        sortClass.sortData(toBeSortedString);
        sortClass.sortData(toBeSortedInt);
        System.out.println(toBeSortedString);
        System.out.println(toBeSortedInt);
    }

    public static void main(String[] args) {
        checkStandartSortUsing();
        SortClassTest test = new SortClassTest();
        test.testSortData();
        int a = 1000000;
        System.out.println(a*4000000);
    }
}

class SortClassTest {
    @Test
    public void testSortData() {
        SortClass sortClass = new SortClass(null);

        List<Integer> toBeSortedList = new ArrayList<>();
        List<Integer> trueList = new ArrayList<>();
        toBeSortedList.add(15);
        toBeSortedList.add(377);
        toBeSortedList.add(214);
        trueList.add(15);
        trueList.add(214);
        trueList.add(377);

        //check accordance
        Assertions.assertEquals(trueList, sortClass.sortData(toBeSortedList));
        //check discordance
        trueList.add(0);
        Assertions.assertNotEquals(trueList, sortClass.sortData(toBeSortedList));
        trueList.remove(trueList.size()-1);
        //check bound conditions
        trueList.add(null);
        toBeSortedList.add(null);
        try {
            Assertions.assertEquals(trueList, sortClass.sortData(toBeSortedList));
        } catch (NullPointerException e) {
            System.out.println("got NullPointerException, test done");
        }
        //check exceptions
        try {
            Assertions.assertNotEquals(trueList, sortClass.sortData(null));
        } catch (Exception e) {
            System.out.println("got exception from sorted Null(empty collection)");
        }

    }
}