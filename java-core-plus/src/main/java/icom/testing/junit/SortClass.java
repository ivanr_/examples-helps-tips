package icom.testing.junit;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortClass<T> {
    private List<T> internalData;
    private SorterFactory sorterFactory;

    public SortClass(SorterFactory sorterFactory) {
        this.sorterFactory = sorterFactory;
        initSorter();
    }

    private void initSorter() {
        if (sorterFactory == null) {
            sorterFactory = new SorterFactory() {
                @Override
                public Comparator getComparator() {
                    return Comparator.naturalOrder();
                }
            };
        }
    }

    private List sort(List dataToSort) {
        Collections.sort(dataToSort, sorterFactory.getComparator());
        return dataToSort;
    }

    public List<T> sortData(List<T> dataToSort) {
        internalData = sort(dataToSort);
        return internalData;
    }

    public List<T> getLastSortedData() {
        return internalData;
    }
}
