package icom.testing.junit;

import java.util.Comparator;

public interface SorterFactory {
    default Comparator getComparator() {
        return Comparator.naturalOrder();
    }
}
