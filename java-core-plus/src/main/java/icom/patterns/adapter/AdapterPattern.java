package icom.patterns.adapter;

public class AdapterPattern {
    public static void main(String[] args) {
        ConcreteUnchangableClass originalClass = new ConcreteUnchangableClass();
        FakeConcreteClass fakeClass = new FakeConcreteClass();
        ClientVer1 clientVer1 = new ClientVer1();
        clientVer1.checkMyFavoriteVersion1(originalClass);
        clientVer1.checkMyFavoriteVersion1(fakeClass);
    }
}

class ClientVer1 {
    ClientVer1() {
    }

    void checkMyFavoriteVersion1(ConcreteUnchangableClass classFroCheck) {
        if (classFroCheck.getVersion() == 1) {
            System.out.println("Client answer: it's what i am waiting for");
        } else {
            System.out.println("Client answer: all fails, where my favorite version 1.0?");
        }
    }
}

interface Adapter {
    int getVersion();
}

class ConcreteUnchangableClass implements Adapter{
    private final int version = 1;

    @Override
    public int getVersion() {
        someUnbelievableOperation();
        return version;
    }

    void someUnbelievableOperation() {
        System.out.println("do some magic from original class");
    }
}

class FakeConcreteClass extends ConcreteUnchangableClass{

    @Override
    public int getVersion() {
        someUnbelievableOperation();
        return 1;
    }

    @Override
    void someUnbelievableOperation() {
        System.out.println("do some new features before from fake class");
        super.someUnbelievableOperation();
    }
}