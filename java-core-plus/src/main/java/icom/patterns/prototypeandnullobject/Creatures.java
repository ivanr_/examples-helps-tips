package icom.patterns.prototypeandnullobject;

public class Creatures implements Cloneable {
    protected int weight;
    protected int volume;
    protected String name;

    @Override
    public Creatures clone () throws CloneNotSupportedException {
        Creatures copyOfCreatures = (Creatures) super.clone();
        return copyOfCreatures;
    }
}
