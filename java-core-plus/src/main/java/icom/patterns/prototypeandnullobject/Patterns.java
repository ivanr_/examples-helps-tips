package icom.patterns.prototypeandnullobject;

class MakeCreature {
    private Creatures creature;

    public MakeCreature(Creatures creature) {
        this.creature = creature;
    }

    public Creatures addCreature() throws CloneNotSupportedException {
        return this.creature.clone();
    }
}

abstract class AbstractMakeNewCreature {
    public abstract void makeCreature() throws CloneNotSupportedException;
}

class RealMakeCreature extends AbstractMakeNewCreature {
    private MakeCreature creature;

    @Override
    public void makeCreature() {
        System.out.println("New creature added");
    }
}

class NullMake extends AbstractMakeNewCreature {

    @Override
    public void makeCreature() {
        System.out.println("Nothing to be done");
    }
}

public class Patterns {

    public static void main(String[] args) throws CloneNotSupportedException {
        Creatures someNewCreature = null;
        Creatures prototypeAnimal = new Animals();
        Creatures prototypeInsect = new Insects();
        MakeCreature madeAnimal = new MakeCreature(prototypeAnimal);
        MakeCreature madeInsetc = new MakeCreature(prototypeInsect);

        for (int counter = 0; counter < 2; counter++) {
            madeAnimal.addCreature();
            madeInsetc.addCreature();
        }

        AbstractMakeNewCreature realMakeCreature = new RealMakeCreature();
        AbstractMakeNewCreature nullMakeAction = new NullMake();

        realMakeCreature.makeCreature();
        nullMakeAction.makeCreature();

    }
}
