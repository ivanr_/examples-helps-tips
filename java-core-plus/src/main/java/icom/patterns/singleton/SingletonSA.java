package icom.patterns.singleton;

public class SingletonSA {
    private static SingletonSA instance;

    private SingletonSA() {
    }

    public static synchronized SingletonSA getInstance() {
        if (instance == null) {
            instance = new SingletonSA();
        }
        return instance;
    }
}

