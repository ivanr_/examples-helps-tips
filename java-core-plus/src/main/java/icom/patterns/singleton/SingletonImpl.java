package icom.patterns.singleton;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

public class SingletonImpl {
    public static void main(String[] args) {
//        pattern FACTORY
        UsingMathOperation[] usingFactoryPatterns = {new UsingAddTax(), new UsingSubstractTax()};

        for (UsingMathOperation using : usingFactoryPatterns) {
            MathOperations mathOperations = using.getTaxFromData();
            System.out.printf("Created {%s}\n", mathOperations.getClass());
        }
//        pattern ITERATOR
        Calculator calculator = new Calculator(); //create new calculator
        calculator.setName(null); //get calculator be unnamed
        Iterator iterator = new CalcIterator(calculator);
        System.out.println(iterator.hasNext());// has next properties? - yes
        System.out.println(iterator.next());// properties contains
        System.out.println(iterator.hasNext());// has next? (true)
        System.out.println(iterator.next());// next properties contains(calc data)
        iterator.remove();// if delete
        System.out.println(iterator.hasNext());// has next? (false)

//        pattern SINGLETON
        Date date1 = new Date();
        Date date2 = new Date();
        Date date3 = new Date();
        SimpleDateFormat f = new SimpleDateFormat("ss:SSS");
        int cycleNUmbers = 100000000;
        System.out.println("Singleton Syncronyzed Accessor creating time start: " + f.format(date1));
        for (int counter = 0; counter<cycleNUmbers; counter++) {
            SingletonSA syncAccessor = SingletonSA.getInstance();
        }
        System.out.println("Singleton Syncronyzed Accessor creatimg time end: " + f.format(date2));
        System.out.println("Singleton Double Checked Locking & Volatile creatimg time start: " + f.format(date2));
        for (int counter = 0; counter<cycleNUmbers; counter++) {
            SingletonDCLV DCLV = SingletonDCLV.getInstance();
        }
        System.out.println("Singleton Double Checked Locking & Volatile creatimg time end: " + f.format(date3));
    }
}
