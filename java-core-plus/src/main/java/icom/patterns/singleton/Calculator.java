package icom.patterns.singleton;

import javax.annotation.Nonnull;
import java.util.Iterator;

public class Calculator implements Iterable <Calculator.Properties> {
    private CalcName name = new CalcName();
    private CalcMethod dataInputMethod = new CalcMethod();
    private CalcData calculatedData = new CalcData();

    @Override
    @Nonnull
    public Iterator<Properties> iterator() {
        return new CalcIterator(this);
    }

    public CalcName getName() {
        return name;
    }

    public void setName(CalcName name) {
        this.name = name;
    }

    boolean hasName() {
        return name != null;
    }

    CalcMethod getDataInputMethod() {
        return dataInputMethod;
    }

    void setDataInputMethod(CalcMethod dataInputMethod) {
        this.dataInputMethod = dataInputMethod;
    }

    boolean hasDataInputMethod() {
        return dataInputMethod != null;
    }

    CalcData getCalculatedData() {
        return calculatedData;
    }

    void setCalculatedData(CalcData calculatedData) {
        this.calculatedData = calculatedData;
    }

    boolean hasCalculatedData() {
        return calculatedData != null;
    }

    interface Properties {
    }

    private static class CalcName implements Properties {
    }

    private static class CalcMethod implements Properties {
    }

    private static class CalcData implements Properties {
    }
}

class CalcIterator implements Iterator <Calculator.Properties> {
    private int index = -1;
    private Calculator c;

    CalcIterator(Calculator c) {
        this.c = c;
    }

    @Override
    public boolean hasNext() {
        if (index == -1) return c.hasName() || c.hasDataInputMethod() || c.hasCalculatedData();
        if (index == 0) return c.hasDataInputMethod() || c.hasCalculatedData();
        if (index == 1) return c.hasCalculatedData();
        return false;
    }

    @Override
    public Calculator.Properties next() {
        if (index == -1) {
            if (c.hasName()) { index = 0; return c.getName(); }
            if (c.hasDataInputMethod()) { index = 1; return c.getDataInputMethod(); }
            if (c.hasCalculatedData()) { index = 2; return c.getCalculatedData(); }
        } else if (index == 0) {
            if (c.hasDataInputMethod()) { index = 1; return c.getDataInputMethod(); }
            if (c.hasCalculatedData()) { index = 2; return c.getCalculatedData(); }
        } else if (index == 1) {
            if (c.hasCalculatedData()) { index = 2; return c.getCalculatedData(); }
        }
        return null;
    }

    public void remove() {
        if (index == -1) throw new IllegalStateException();
        if (index == 0)
            if (c.hasName()) c.setName(null);
            else throw new IllegalStateException();
        if (index == 1)
            if (c.hasDataInputMethod()) c.setDataInputMethod(null);
            else throw new IllegalStateException();
        if (index == 2)
            if (c.hasCalculatedData()) c.setCalculatedData(null);
            else throw new IllegalStateException();
    }
}
