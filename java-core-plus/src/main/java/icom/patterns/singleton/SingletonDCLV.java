package icom.patterns.singleton;

public class SingletonDCLV {
    private static volatile SingletonDCLV instance;

    private SingletonDCLV() {
    }

    public static SingletonDCLV getInstance() {
        SingletonDCLV localInstance = instance;
        if (localInstance == null) {
            synchronized (SingletonDCLV.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new SingletonDCLV();
                }
            }
        }
        return localInstance;
    }
}
