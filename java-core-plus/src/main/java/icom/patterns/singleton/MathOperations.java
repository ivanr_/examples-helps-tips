package icom.patterns.singleton;

public class MathOperations {
    private String nameOfOperation;
    private int priorityOperation;

    protected MathOperations(String nameOfOperation) {
        this.nameOfOperation = nameOfOperation;
    }
}

class Add extends MathOperations {
    protected Add(String nameOfOperation) {
        super(nameOfOperation);
    }

    private int result(int variable1, int variable2) {
        return variable1 + variable2;
    }
}

class Substract extends MathOperations {
    protected Substract(String nameOfOperation) {
        super(nameOfOperation);
    }

    private int result(int variable1, int variable2) {
        return variable1 - variable2;
    }
}