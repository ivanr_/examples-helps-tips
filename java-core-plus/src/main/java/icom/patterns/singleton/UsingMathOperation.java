package icom.patterns.singleton;

public abstract class UsingMathOperation {
    public abstract MathOperations getTaxFromData();
}

class UsingAddTax extends UsingMathOperation{

    @Override
    public MathOperations getTaxFromData() {
        return new Add("tax from adding data");
    }
}

class UsingSubstractTax extends UsingMathOperation{

    @Override
    public MathOperations getTaxFromData() {
        return new Substract("tax from sutstracting data");
    }
}