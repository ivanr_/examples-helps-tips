package icom.patterns.template;

public class Template {
    public static void main(String[] args) {

        System.out.println("\n");
        CalculatorsTypes calculatorsTypes = new CalculatorsTypes(TypeOfCalculator.StandardDialogPaneCalculator);
        calculatorsTypes.useCalculator();
    }
}
