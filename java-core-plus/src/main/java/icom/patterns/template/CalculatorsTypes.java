package icom.patterns.template;

enum TypeOfCalculator {
    StandardConsoleCalculator,
    StandardDialogPaneCalculator,
    NetworkCalculator
}

public class CalculatorsTypes {
    TypeOfCalculator calcType = TypeOfCalculator.NetworkCalculator;

    public CalculatorsTypes(TypeOfCalculator type) {
        this.calcType = type;
    }

    public void useCalculator () {
        CalculatorBehaviour calculator;
        switch(calcType){
            case StandardConsoleCalculator:
                calculator = new StandardConsoleCalculator();
                break;
            case StandardDialogPaneCalculator:
                calculator = new StandardDialogPaneCalculator();
                break;
            case NetworkCalculator:
                calculator = new NetworkCalculator();
                break;
            default:
                throw new IllegalStateException();
        }
        calculator.executeCalculation(true);
    }
}

class StandardConsoleCalculator extends CalculatorBehaviour {

    @Override
    protected void initializeCalculator() {
        System.out.println("print input dialog at console to get expressions for calculator");
    }

    @Override
    protected void getDataForCalculations() {
        System.out.println("scan console");
    }

    @Override
    protected void calculate() {
        System.out.println("validate-calculate-getLogData-print results at console");
    }

    @Override
    protected void saveResults() {
        System.out.println("save results and consequence at file");
    }
}

class StandardDialogPaneCalculator extends CalculatorBehaviour {

    @Override
    protected void initializeCalculator() {
        System.out.println("show output dialog at JOptionPane/AWT/etc to get expressions for calculator (allow only strict char to input)");
    }

    @Override
    protected void getDataForCalculations() {
        System.out.println("get data from pane (only after simple validating)");
    }

    @Override
    protected void calculate() {
        System.out.println("validate-calculate-getLogData-output results at dialog pane");
    }

    @Override
    protected void saveResults() {
        System.out.println("save results and consequence at file of define type");
    }
}

class NetworkCalculator extends CalculatorBehaviour {

    @Override
    protected void initializeCalculator() {
        System.out.println("connect with server resources-show output dialog at Pane/Console/etc to get expressions for calculator");
    }

    @Override
    protected void getDataForCalculations() {
        System.out.println("get data from input source");
    }

    @Override
    protected void calculate() {
        System.out.println("validate-calculate-getLogData-output results at output pane/console");
    }

    @Override
    protected void saveResults() {
        System.out.println("save results and consequence of calculation at file of define type");
    }
}