package icom.patterns.template;

public abstract class CalculatorBehaviour {
    private boolean saveResultsAtCSV_orXML;

    protected abstract void initializeCalculator();

    protected abstract void getDataForCalculations();

    protected abstract void calculate();

    protected abstract void saveResults();

    public final void executeCalculation(boolean trueForSaveResultsAtCSV_falseForSaveResultsAtXML) {
        setCalculatorType(trueForSaveResultsAtCSV_falseForSaveResultsAtXML);
        initializeCalculator();
        getDataForCalculations();
        calculate();
        saveResults();
    }

    public void setCalculatorType(boolean trueForSaveResultsAtCSV_falseForSaveResultsAtXML) {
        this.saveResultsAtCSV_orXML = trueForSaveResultsAtCSV_falseForSaveResultsAtXML;
    }
}

