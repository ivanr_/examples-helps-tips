package icom.patterns.decorator.impl;

import icom.patterns.decorator.common.DecoratorAction;
import icom.patterns.decorator.common.InterfaceAction;

public class Configuration extends DecoratorAction {

    public Configuration(InterfaceAction action) {
        super(action);
    }

    @Override
    public void someAction() {
        System.out.println("Do config with app");
        super.someAction();
    }
}
