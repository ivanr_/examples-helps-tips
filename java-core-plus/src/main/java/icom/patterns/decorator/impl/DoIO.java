package icom.patterns.decorator.impl;

import icom.patterns.decorator.common.DecoratorAction;
import icom.patterns.decorator.common.InterfaceAction;

public class DoIO extends DecoratorAction {

    public DoIO(InterfaceAction action) {
        super(action);
    }

    @Override
    public void someAction() {
        System.out.println("Do input-output actions");
        super.someAction();
    }
}
