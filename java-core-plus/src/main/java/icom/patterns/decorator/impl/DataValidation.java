package icom.patterns.decorator.impl;

import icom.patterns.decorator.common.DecoratorAction;
import icom.patterns.decorator.common.InterfaceAction;

public class DataValidation extends DecoratorAction {

    public DataValidation(InterfaceAction action) {
        super(action);
    }

    @Override
    public void someAction() {
        System.out.println("Do validate actions");
        super.someAction();
    }
}
