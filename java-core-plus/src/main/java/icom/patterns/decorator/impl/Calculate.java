package icom.patterns.decorator.impl;

import icom.patterns.decorator.common.InterfaceAction;

public class Calculate implements InterfaceAction {

    @Override
    public void someAction() {
        System.out.println("Do calculations");
    }
}
