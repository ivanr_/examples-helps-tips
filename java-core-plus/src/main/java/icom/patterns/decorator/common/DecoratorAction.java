package icom.patterns.decorator.common;

public abstract class DecoratorAction implements InterfaceAction {
    private InterfaceAction action;

    public DecoratorAction (InterfaceAction action) {
        this.action = action;
    }

    @Override
    public void someAction() {
//        System.out.println("Do some abstract action");
        action.someAction();
    }
}
