package icom.patterns.decorator.common;

public interface InterfaceAction {
    public void someAction();
}
