package icom.patterns.decorator;

import icom.patterns.decorator.common.DecoratorAction;
import icom.patterns.decorator.impl.Calculate;
import icom.patterns.decorator.impl.Configuration;
import icom.patterns.decorator.impl.DataValidation;
import icom.patterns.decorator.impl.DoIO;

public class Decorator {
    public static void main(String[] args) {
        DecoratorAction decorator =
                new Configuration(
                        new DoIO(
                                new DataValidation(
                                        new Calculate()
                                )
                        )
                );
        decorator.someAction();
    }
}

