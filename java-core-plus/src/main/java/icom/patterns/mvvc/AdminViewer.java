package icom.patterns.mvvc;


import icom.patterns.mvvc.common.Observer;
import icom.patterns.mvvc.common.User;

public class AdminViewer implements Observer {
    private User user;

    @Override
    public void update(User user) {
        this.user = user;
        showUserData();
    }

    private void showUserData() {
        System.out.println("Admin viewer result:");
        System.out.println("\tUser " + user.getlName() + " " + user.getName() + " has next tax: " + user.getTax());
        System.out.println("\tUser has ID: " + user.getUserId());
    }
}

