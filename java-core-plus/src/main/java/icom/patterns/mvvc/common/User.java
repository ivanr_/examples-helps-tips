package icom.patterns.mvvc.common;

import java.util.ArrayList;
import java.util.List;

public class User implements Observable {
    private final String name;
    private final String lName;
    private final int userId;
    private int tax;
    private List<Observer> observers = new ArrayList<>();

    public User(String name, String lName, int userId, int tax) {
        this.name = name;
        this.lName = lName;
        this.userId = userId;
        this.tax = tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
        notifyObservers();
    }

    public String getlName() {
        return lName;
    }

    public String getName() {
        return name;
    }

    public int getUserId() {
        return userId;
    }

    public int getTax() {
        return tax;
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer1 : observers) {
            observer1.update(new User(getlName(), getlName(), getUserId(), getTax()));
        }
    }
}
