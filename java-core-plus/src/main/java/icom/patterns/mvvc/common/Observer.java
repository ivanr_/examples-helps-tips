package icom.patterns.mvvc.common;

public interface Observer {
    void update(User user);
}
