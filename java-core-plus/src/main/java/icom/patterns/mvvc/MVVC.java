package icom.patterns.mvvc;

import icom.patterns.mvvc.common.User;

public class MVVC {
    public static void main(String[] args) {
        User user = new User("Mike", "Petrov", 544, 12);
        user.addObserver(new AdminViewer());
        user.addObserver(new UserViewer());
        user.addObserver(new IdUserViewer());
        user.setTax(15);
    }
}

