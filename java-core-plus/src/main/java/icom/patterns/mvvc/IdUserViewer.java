package icom.patterns.mvvc;


import icom.patterns.mvvc.common.Observer;
import icom.patterns.mvvc.common.User;

public class IdUserViewer implements Observer {
    private User user;

    @Override
    public void update(User user) {
        this.user = user;
        showUserData();
    }

    private void showUserData () {
        System.out.println("IdUser viewer result:");
        System.out.println("\tUser " + user.getlName() + " has next tax: " + user.getTax());
    }
}
