package icom.patterns.observer;

import java.util.ArrayList;
import java.util.List;

public class MyObserver {
    public static void main(String[] args) {
        final UserModel userModel = new UserModel(new UserDto());
        Observer<UserDto> userDtoObserver = event ->
                System.out.println("Observer_4; User class name: " + event.getName().getClass());

        userModel.addObservers(event ->
                System.out.println("Observer_1; User name: " + event.getName()));
        userModel.addObservers(event ->
                System.out.println("Observer_2; User name: " + event.getName().toUpperCase()));
        userModel.addObservers(event ->
                System.out.println("Observer_3; User name: " + event.getName().toLowerCase()));
        userModel.addObservers(userDtoObserver);
        userModel.setUserName("Name 1");
        userModel.setUserName("Name 2");
        userModel.setUserName("Name 3");
        userModel.setUserName("Name 4");
    }
}

class UserDto {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class UserModel extends Observable<UserDto> {
    private UserDto userDto;

    public UserModel(UserDto userDto) {
        this.userDto = userDto;
    }

    public void setUserName(String name) {
        userDto.setName(name);
        update(userDto);
    }
}

abstract class Observable<T> {
    private final List<Observer<T>> observers = new ArrayList<>();

    public final void addObservers(Observer<T> observer) {
        this.observers.add(observer);
    }

    public final void removeObserver(Observer<T> observer) {
        this.observers.remove(observer);
    }

    protected void update(T event) {
        for (Observer<T> observer : observers) {
            observer.update(event);
        }
    }
}

interface Observer<T> {
    void update(T event);
}

