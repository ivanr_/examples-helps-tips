package icom.patterns.builder;

public class BuilderPattern {
    public static void main(String[] args) {
        DevOpsTeaching devOpsTeaching = new DevOpsTeaching();
        DevOpsBuilder devOpsBuilder = new ClassicalDevOpsBuilder();

        devOpsTeaching.setDevOpsBuilder(devOpsBuilder);
        devOpsTeaching.constructDevOps();
        DevOps devOps = devOpsTeaching.getDevOps();

        devOpsTeaching.setDevOpsBuilder(new LegPrintingDevOpsBuilder());
        devOpsTeaching.constructDevOps();
        DevOps devOps1 = devOpsTeaching.getDevOps();

        System.out.println(devOps.getName());
        System.out.println(devOps.getMainSkill());
        System.out.println(devOps.getAdditionalSkill());
        System.out.println(devOps.getFunnyFeature());
        System.out.println();
        System.out.println(devOps1.getName());
        System.out.println(devOps1.getMainSkill());
        System.out.println(devOps1.getAdditionalSkill());
        System.out.println(devOps1.getFunnyFeature());
    }
}

class DevOps {
    private String mainSkill = "";
    private String additionalSkill = "";
    private String funnyFeature = "";
    private String name = "";

    public void setMainSkill(String mainSkill) {
        this.mainSkill = mainSkill;
    }

    public void setAdditionalSkill(String additionalSkill) {
        this.additionalSkill = additionalSkill;
    }

    public String getMainSkill() {
        return mainSkill;
    }

    public String getAdditionalSkill() {
        return additionalSkill;
    }

    public String getFunnyFeature() {
        return funnyFeature;
    }

    public void setFunnyFeature(String funnyFeature) {
        this.funnyFeature = funnyFeature;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

abstract class DevOpsBuilder {
    protected DevOps devOps;

    public DevOps getDevOps() {
        return devOps;
    }

    public void createNewDevOps() {
        devOps = new DevOps();
    }

    public abstract void loadMainSkill();

    public abstract void loadAdditSkill();

    public abstract void addFunnyFeature();

    public abstract void setName();
}

class LegPrintingDevOpsBuilder extends DevOpsBuilder {

    @Override
    public void loadMainSkill() {
        devOps.setMainSkill("PHP");
    }

    @Override
    public void loadAdditSkill() {
        devOps.setAdditionalSkill("CSS");
    }

    @Override
    public void addFunnyFeature() {
        devOps.setFunnyFeature("tv dependence");
    }

    @Override
    public void setName() {
        devOps.setName("FrontOrientedDevOps");
    }
}

class ClassicalDevOpsBuilder extends DevOpsBuilder {

    @Override
    public void loadMainSkill() {
        devOps.setMainSkill("Java");
    }

    @Override
    public void loadAdditSkill() {
        devOps.setAdditionalSkill("Js");
    }

    @Override
    public void addFunnyFeature() {
        devOps.setFunnyFeature("love hard coding");
    }

    @Override
    public void setName() {
        devOps.setName("ClassicalDevOps");
    }
}

class DevOpsTeaching {
    private DevOpsBuilder devOpsBuilder;

    public void setDevOpsBuilder(DevOpsBuilder devOpsBuilder) {
        this.devOpsBuilder = devOpsBuilder;
    }

    public DevOps getDevOps() {
        return devOpsBuilder.getDevOps();
    }

    public void constructDevOps() {
        devOpsBuilder.createNewDevOps();
        devOpsBuilder.loadMainSkill();
        devOpsBuilder.loadAdditSkill();
        devOpsBuilder.addFunnyFeature();
        devOpsBuilder.setName();
    }
}