package icom.lambda.simpleinterfaces;

@FunctionalInterface
interface MyLambda1 {
    String execute();
}

@FunctionalInterface
interface MyLambda2 {
    void execute();

    default void addInterfaceNumber() {
        execute();
        System.out.println(" it's MyLambda2 interface");
    }
}

@FunctionalInterface
interface MyDiscount {
    int calcDiscount(int n);

    default void getDiscount(int myCost, int numberOfDiscounts) {
        System.out.println("Your discount is " + calcDiscount(numberOfDiscounts) + "%");
        System.out.println("From " + myCost + " money " + " you get back " + calcDiscount(numberOfDiscounts) * myCost * 0.01);
    }
}

public class Lambda {

    public static void main(String[] args) {
        MyLambda1 temp = () -> "hello method MyLambda1";
        System.out.println(temp.execute());

        MyLambda2 temp2 = () -> System.out.print("Hello World");
        temp2.addInterfaceNumber();

        MyDiscount discount1 = (n) -> 1 * n;
        MyDiscount discount2 = (n) -> 2 * n;
        MyDiscount discount3 = (n) -> 0;
        int cost = 500;
        discount1.getDiscount(cost, 5);
        discount2.getDiscount(cost, 5);
        discount3.getDiscount(cost, 5);

        System.out.println();
    }

}
