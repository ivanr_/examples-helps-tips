package icom.lambda.calculations;

import java.util.HashMap;

public class HashTableCalculations {
    private HashMap<String, LambdaCalculator> interfaceOperationList = new HashMap();

    HashTableCalculations() {
        init();
    }

    private void init() {
        interfaceOperationList.put("+", (a, b) -> a + b);
        interfaceOperationList.put("-", (a, b) -> a - b);
        interfaceOperationList.put("*", (a, b) -> a * b);
        interfaceOperationList.put("/", (a, b) -> {
                    try {
                        return a / b;
                    } catch (ArithmeticException e) {
                        return null;
                    }
                }
        );
    }

    public void addOperation(String operationSymbol, LambdaCalculator operation) {
        interfaceOperationList.put(operationSymbol, operation);
    }

    public LambdaCalculator getOperationBySymbol(String operationSymbol) {
        return interfaceOperationList.get(operationSymbol);
    }

}
