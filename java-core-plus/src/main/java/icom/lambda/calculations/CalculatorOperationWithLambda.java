package icom.lambda.calculations;

public class CalculatorOperationWithLambda {

    public static void main(String[] args) {
        HashTableCalculations operationsStorage = new HashTableCalculations();
        LambdaCalculator mult = operationsStorage.getOperationBySymbol("*");
        System.out.println(mult.getResult(4, 6));

    }
}
