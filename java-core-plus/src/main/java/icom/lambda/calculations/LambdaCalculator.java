package icom.lambda.calculations;

@FunctionalInterface
public interface LambdaCalculator {
    abstract public Integer getResult(int a, int b);

    default public int increment(int input) {
        return input++;
    }

    default public int decrement(int input) {
        return input--;
    }

    default public int reverseInt(int input) {
            return Integer.reverse(input);
    }
}
