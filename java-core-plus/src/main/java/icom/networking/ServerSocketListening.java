package icom.networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSocketListening {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8081);
        while (true) {
            Socket s = serverSocket.accept();
            System.out.println("client connected");

            BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
            while(true) {
                String line = br.readLine();
                if(line == null || line.trim().length() == 0) {
                    break;
                }
            }

            String sr = "<html><body><h1>Hello from Ivan</h1></body></html>";
            String response = "HTTP/1.1 200 OK\r\n" +
                    "Server: YarServer/2009-09-09\r\n" +
                    "Content-Type: text/html\r\n" +
                    "Content-Length: " + sr.length() + "\r\n" +
                    "Connection: close\r\n\r\n";
            String result = response + sr;
            s.getOutputStream().write(result.getBytes());
            s.getOutputStream().flush();

            s.close();
            System.out.println("client disconnected");
        }
    }
}
