package icom.networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Tcp {

    private static String getHTMLRequest(String urlAdressSite) throws IOException {
        URL url = new URL(urlAdressSite);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuilder result = new StringBuilder();
        String readedLine;
        while ((readedLine = reader.readLine()) != null) {
            result.append(readedLine);
        }
        reader.close();
        return result.toString();
    }

    public static void main(String[] args) throws IOException {
        String inpUrl = "https://www.i.ua/";
//        String inpUrl = "https://www.google.com/search?q=hillel+dnepr";
        System.out.println(getHTMLRequest(inpUrl));
    }
}


