package icom.logging.log4j;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Lo4jConsoleFileOutput {
    final static Logger LOGGER = Logger.getLogger(Lo4jConsoleFileOutput.class);

    private static void initLogger() {
        PropertyConfigurator.configure("src/resources/java/icom/Logging/Log4j/log4j.properties");
    }

    public static void main(String[] args) {
        initLogger();
        new AnotherClassForLogs().addLogs();
    }
}

class AnotherClassForLogs {
    void addLogs() {
        Lo4jConsoleFileOutput.LOGGER.error("it's an example of ERROR message");
    }
}