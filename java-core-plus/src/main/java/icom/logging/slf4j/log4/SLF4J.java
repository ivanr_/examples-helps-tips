package icom.logging.slf4j.log4;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SLF4J {
    public static Logger LOGGER = LoggerFactory.getLogger(SLF4J.class);

    private static void configLogger(String pathMayBeEmptyForDefault) {
        if (pathMayBeEmptyForDefault.equals("")) {
            pathMayBeEmptyForDefault = "src\\main\\java\\icom\\Logging\\Slf4jLog4\\lib\\Log4jConsoleAndFile.properties";
        }
        PropertyConfigurator.configure(pathMayBeEmptyForDefault);
    }

    public static void main(String[] args) {
        configLogger("");
        ToBeLogged toBeLogged = new ToBeLogged();
        toBeLogged.init();
        toBeLogged.someLogicExecutions();
    }
}

class ToBeLogged {

    void init() {
//        actions for initialization
        double randomResult = Math.random();
        if (randomResult < 0.5) {
            String log = "failed init, number less than 0.5";
            SLF4J.LOGGER.error(log);
        } else {
            String log = "failed success, number no less than 0.5";
            SLF4J.LOGGER.error(log);
        }
    }

    void someLogicExecutions() {
//        some proceeding
        double randomResult = Math.random();
        if (randomResult < 0.5) {
            String log = "fast execution, number less than 0.5";
            SLF4J.LOGGER.error(log);
        } else {
            String log = "slow execution, number no less than 0.5";
            SLF4J.LOGGER.error(log);
        }
    }
}
