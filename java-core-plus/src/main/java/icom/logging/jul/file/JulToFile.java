package icom.logging.jul.file;

import java.io.IOException;
import java.util.logging.*;

public class JulToFile {
    static Logger LOGGER = Logger.getLogger(JulToFile.class.getName());

    private static void initLogger() throws IOException {
        String pathFile = "src\\main\\java\\icom\\Logging\\JulToFile\\LogFileJUL.xml";
        Handler fileHandler = new FileHandler(pathFile, 3, 2, true);
        fileHandler.setLevel(Level.ALL);
        fileHandler.setFormatter(new XMLFormatter());
        LOGGER.addHandler(fileHandler);
    }

    public static void main(String[] args) throws IOException {
        RandomNumberGenerator logOut = new RandomNumberGenerator();
        initLogger();
        logOut.getLogRecord();
    }
}

class RandomNumberGenerator {
    void getLogRecord() {
        double randomNumber = Math.random();
        System.out.println("Got number: " + randomNumber);
        if (randomNumber < 0.2) {
            JulToFile.LOGGER.finest("FINEST level record example");
        } else {
            if (randomNumber < 0.4) {
                JulToFile.LOGGER.finer("FINER level record example");
            } else {
                if (randomNumber < 0.6) {
                    JulToFile.LOGGER.fine("FINE level record example");
                } else {
                    if (randomNumber < 0.7) {
                        JulToFile.LOGGER.config("CONFIG level record example");
                    } else {
                        if (randomNumber < 0.8) {
                            JulToFile.LOGGER.info("INFO level record example");
                        } else {
                            if (randomNumber < 0.9) {
                                JulToFile.LOGGER.warning("WARNING level record example");
                            } else {
                                JulToFile.LOGGER.severe("SEVERE level record example");
                            }
                        }
                    }
                }
            }
        }
    }
}