package icom.logging.jul.stream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

public class JulToStream {
    static Logger LOGGER = Logger.getLogger(JulToStream.class.getName());
    static ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

    private static void initLogger() {
        Handler streamHandler = new StreamHandler(JulToStream.byteArrayOutputStream, new SimpleFormatter());
        LOGGER.addHandler(streamHandler);
    }

    public static void main(String[] args) throws IOException {
        RandomNumberGenerator logOut = new RandomNumberGenerator();
        initLogger();
        logOut.getLogRecord();
    }
}

class RandomNumberGenerator {
    void getLogRecord() {
        double randomNumber = Math.random();
        System.out.println("Got number: " + randomNumber);
        if (randomNumber < 0.2) {
            JulToStream.LOGGER.fine("FINE level record example");
        } else {
            if (randomNumber < 0.4) {
                JulToStream.LOGGER.config("CONFIG level record example");
            } else {
                if (randomNumber < 0.6) {
                    JulToStream.LOGGER.info("INFO level record example");
                } else {
                    if (randomNumber < 0.8) {
                        JulToStream.LOGGER.warning("WARNING level record example");
                    } else {
                        JulToStream.LOGGER.severe("SEVERE level record example");
                    }
                }
            }
        }
        String result = new String(JulToStream.byteArrayOutputStream.toByteArray(), StandardCharsets.UTF_8);
        System.out.println(result);
    }
}