package icom.logging.jul.console;

import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

public class JulToConsole {
    static Logger LOGGER = Logger.getLogger(JulToConsole.class.getName());

    private static void initLogger() {
        ConsoleHandler handler = new ConsoleHandler();
        LOGGER.addHandler(handler);
    }

    public static void main(String[] args) {
        RandomNumberGenerator logOut = new RandomNumberGenerator();
        initLogger();
        logOut.getLogRecord();
    }
}

class RandomNumberGenerator {
    void getLogRecord() {
        double randomNumber = Math.random();
        System.out.println("Got number: " + randomNumber);
        if (randomNumber < 0.2) {
            JulToConsole.LOGGER.finest("FINEST level record example");
        } else {
            if (randomNumber < 0.4) {
                JulToConsole.LOGGER.finer("FINER level record example");
            } else {
                if (randomNumber < 0.6) {
                    JulToConsole.LOGGER.fine("FINE level record example");
                } else {
                    if (randomNumber < 0.7) {
                        JulToConsole.LOGGER.config("CONFIG level record example");
                    } else {
                        if (randomNumber < 0.8) {
                            JulToConsole.LOGGER.info("INFO level record example");
                        } else {
                            if (randomNumber < 0.9) {
                                JulToConsole.LOGGER.warning("WARNING level record example");
                            } else {
                                JulToConsole.LOGGER.severe("SEVERE level record example");
                            }
                        }
                    }
                }
            }
        }
    }
}