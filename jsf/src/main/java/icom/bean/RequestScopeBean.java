package icom.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.Date;

@ManagedBean
@RequestScoped
public class RequestScopeBean {

    public String getDefaultMessage(){
        System.out.println("jsf bean accessed");
        return "default message from Jsf";
    }

    public String printCurrentData(){
        Date currentDate = new Date();
        System.out.println("Current date must be printed: \n " + currentDate.toString());
        return currentDate.toString();
    }


}
