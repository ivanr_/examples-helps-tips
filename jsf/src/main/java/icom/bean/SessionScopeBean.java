package icom.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class SessionScopeBean {

    public String getDefaultMessage(){
        System.out.println("spring bean accessed");
        return "default message from Spring";
    }
}
