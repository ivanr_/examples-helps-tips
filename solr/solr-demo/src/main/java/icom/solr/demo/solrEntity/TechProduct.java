package icom.solr.demo.solrEntity;

import org.apache.solr.client.solrj.beans.Field;

public class TechProduct {
    @Field
    public String id;

    @Field
    public String name;

    @Field
    public String cat;

    public TechProduct(String id, String name, String cat) {
        this.id = id;
        this.name = name;
        this.cat = cat;
    }
}
