package icom.solr.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import icom.solr.demo.beanProvider.BeanProvider;
import icom.solr.demo.constants.Constants;
import icom.solr.demo.helpers.UrlHelper;
import icom.solr.demo.mappers.Mapper;
import icom.solr.demo.services.SolrRestService;

import java.util.List;

import static icom.solr.demo.mappers.Mapper.getMapFromJson;

public class Application {
    public static void main(String[] args) throws JsonProcessingException {
        BeanProvider beanProvider = new BeanProvider();
        SolrRestService service = beanProvider.getSolrRestService();
        List existedCollections = Mapper
                .getCollectionsFromMapByName(
                        getMapFromJson(service.getForObject(UrlHelper.COLLECTIONS_GET_LIST)), Constants.COLLECTIONS_FIELD_NAME);

        System.out.println("\nExisted collections: " + existedCollections.toString());

        System.out.println("response for two fields: " + service.getForObject(UrlHelper.TWO_FIELDS_URL_RQ));

        System.out.println("response for combined search: " + service.getForObject(UrlHelper.COMBINED_SEARCH_URL));

        System.out.println("\nExisted collections " + getMapFromJson(service.getForObject(UrlHelper.COLLECTIONS_GET_LIST)).get(Constants.COLLECTIONS_FIELD_NAME).toString());

        if (!existedCollections.contains("newCollection")) {
            System.out.println(service.exchangeGet(UrlHelper.COLLECTION_CREATE_URL));
            existedCollections = Mapper
                    .getCollectionsFromMapByName(
                            getMapFromJson(service.getForObject(UrlHelper.COLLECTIONS_GET_LIST)), Constants.COLLECTIONS_FIELD_NAME);
            System.out.println("\nExisted collections: " + existedCollections.toString());
        } else {
            System.out.println("Collection with name: newCollection already exists");
        }

        if (existedCollections.contains("newCollection")) {
            System.out.println(service.exchangeGet(UrlHelper.COLLECTION_REMOVE_URL));
            existedCollections = Mapper
                    .getCollectionsFromMapByName(
                            getMapFromJson(service.getForObject(UrlHelper.COLLECTIONS_GET_LIST)), Constants.COLLECTIONS_FIELD_NAME);
        } else {
            System.out.println("Collection with name: newCollection doesn't exists");
        }

        System.out.println("\nExisted collections" + existedCollections.toString());
    }
}
