package icom.solr.demo;

import icom.solr.demo.beanProvider.BeanProvider;
import icom.solr.demo.constants.Constants;
import icom.solr.demo.helpers.UrlHelper;
import icom.solr.demo.mappers.Mapper;
import icom.solr.demo.services.SolrRestService;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static icom.solr.demo.mappers.Mapper.getMapFromJson;

public class Application2 {
    public static void main(String[] args) throws IOException {
        BeanProvider beanProvider = new BeanProvider();
        SolrRestService service = beanProvider.getSolrRestService();

        List existedCollections = Mapper
                .getCollectionsFromMapByName(
                        getMapFromJson(service.getForObject(UrlHelper.COLLECTIONS_GET_LIST)), Constants.COLLECTIONS_FIELD_NAME);
        System.out.println(existedCollections.toString());

        if (!existedCollections.contains("films")) {
            service.getForObject(UrlHelper.COLLECTION_CREATE_URL.replace("newCollection", "films"));
        }

        String fieldsResponse = service.getForObject(UrlHelper.FIELDS_SEARCH_URL);
        Map fieldsDescription = getMapFromJson(fieldsResponse);
        List fields = Mapper.getCollectionsFromMapByName(fieldsDescription, Constants.FIELDS_NAME);
        List fieldsNames = Mapper.extractFieldsNamesFromList(fields);
        System.out.println("Existed fields: " + fieldsNames);
    }

}
