package icom.solr.demo;

import icom.solr.demo.beanProvider.BeanProvider;
import icom.solr.demo.helpers.TextHelper;
import icom.solr.demo.helpers.UrlHelper;
import icom.solr.demo.solrEntity.TechProduct;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.MapSolrParams;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SolrApiApp {

    public static void main(String[] args) throws IOException, SolrServerException {
        BeanProvider beanProvider = new BeanProvider();

//        queryDirectedByFields(beanProvider);

//        querySortedFields(beanProvider);

//        addDocWithCommit(beanProvider);

        addOrChangeDocAsJavaObject(beanProvider);
    }

    private static void addOrChangeDocAsJavaObject(BeanProvider beanProvider) throws IOException, SolrServerException {
        final SolrClient client = beanProvider.getSolrClient(UrlHelper.SOLR_CLIENT_URL);

        final TechProduct kindle = new TechProduct("kindle-id-4", "Amazon Kindle Paperwhite1", TextHelper.CUSTOM_DATA_SET);
//        if bean exists, client changes fields and DO NOT add data
        client.addBean(TextHelper.TECH_PRODUCT_COLL, kindle);

        client.commit(TextHelper.TECH_PRODUCT_COLL);

        SolrQuery solrQuery = beanProvider.getSolrQuery();
//        getPreparedQuery(solrQuery);
        solrQuery.setQuery("\"" + TextHelper.CUSTOM_DATA_SET + "\"");
        QueryResponse solrQueryResponse = client.query(TextHelper.TECH_PRODUCT_COLL, solrQuery);
        SolrDocumentList queryDocuments = solrQueryResponse.getResults();
        System.out.println(("\nFound " + queryDocuments.getNumFound() + " documents"));
        queryDocuments
                .forEach(document -> System.out.println(document.toString()));
    }

    private static void addDocWithCommit(BeanProvider beanProvider) throws IOException, SolrServerException {
        final SolrClient client = beanProvider.getSolrClient(UrlHelper.SOLR_CLIENT_URL);

        final SolrInputDocument doc = new SolrInputDocument();
        doc.addField("id", UUID.randomUUID().toString());
        doc.addField("name", "Amazon Kindle Paperwhite3");

        SolrQuery solrQuery = beanProvider.getSolrQuery();
        getPreparedQuery(solrQuery);
        QueryResponse solrQueryResponse = client.query(TextHelper.TECH_PRODUCT_COLL, solrQuery);
        SolrDocumentList queryDocuments = solrQueryResponse.getResults();
        System.out.println(("\nFound " + queryDocuments.getNumFound() + " documents"));
        queryDocuments
                .forEach(document -> System.out.println(("id: " + document.getFirstValue("id") + "; name: " + document.getFirstValue("name"))));

//        every time client ADDS NEW entity
        client.add(TextHelper.TECH_PRODUCT_COLL, doc);
//         Indexed documents must be committed
        client.commit(TextHelper.TECH_PRODUCT_COLL);

        System.out.println("\nRetrieving committed docs: ");
        solrQueryResponse = client.query(TextHelper.TECH_PRODUCT_COLL, solrQuery);
        queryDocuments = solrQueryResponse.getResults();

        System.out.println(("\nFound " + queryDocuments.getNumFound() + " documents"));
        queryDocuments
                .forEach(document -> System.out.println(("id: " + document.getFirstValue("id") + "; name: " + document.getFirstValue("name"))));
    }

    private static void querySortedFields(BeanProvider beanProvider) throws IOException, SolrServerException {
        final SolrClient client = beanProvider.getSolrClient(UrlHelper.SOLR_CLIENT_URL);

        final int numResultsToReturn = 5;
        final SolrQuery query =  beanProvider.getSolrQuery();

        getPreparedQuery(query);
        query.setSort("id", SolrQuery.ORDER.asc);
        query.setRows(numResultsToReturn);

        final Map<String, String> queryParamMap = new HashMap<>();
        queryParamMap.put("q", "*:*");
        queryParamMap.put("fl", "id, name");
        queryParamMap.put("sort", "id asc");
        MapSolrParams queryParams = new MapSolrParams(queryParamMap);

        final QueryResponse solrQueryResponse = client.query(TextHelper.TECH_PRODUCT_COLL, query);
        final QueryResponse solrMapResponse = client.query(TextHelper.TECH_PRODUCT_COLL, queryParams);
        final SolrDocumentList queryDocuments = solrQueryResponse.getResults();
        final SolrDocumentList mapDocuments = solrMapResponse.getResults();

        System.out.println(("\nFound " + queryDocuments.getNumFound() + " documents from customized query"));
        for(SolrDocument document : queryDocuments) {
            final String id = (String) document.getFirstValue("id");
            final String name = (String) document.getFirstValue("name");
            System.out.println(("id: " + id + "; name: " + name));
        }

        System.out.println(("\nFound " + mapDocuments.getNumFound() + " documents from mapped params"));
        for(SolrDocument document : mapDocuments) {
            final String id = (String) document.getFirstValue("id");
            final String name = (String) document.getFirstValue("name");
            System.out.println(("id: " + id + "; name: " + name));
        }
    }

    private static void getPreparedQuery(SolrQuery query) {
        query.setQuery("*:*");
        query.addField("id");
        query.addField("name");
    }

    private static void queryDirectedByFields(BeanProvider beanProvider) throws SolrServerException, IOException {
        HttpSolrClient solr = beanProvider.getSolrClient(UrlHelper.QUERY_FIELD_URL);
        SolrQuery query = beanProvider.getSolrQuery();

        query.setQuery("*:*");
        query.setRows(5);
        SolrDocumentList results = solr.query(query).getResults();

        List directed_by = Collections.emptyList();

        try {
            directed_by = results.stream()
                    .map(getDirectedByFields())
                    .flatMap(getListStream())
                    .collect(Collectors.toList());

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Query by field results: " + directed_by.toString());
    }

    private static Function<List, Stream<?>> getListStream() {
        return entryList -> entryList != null ? entryList.stream() : Stream.empty();
    }

    private static Function<SolrDocument, List> getDirectedByFields() {
        return doc -> (List) doc.get("directed_by");
    }
}
