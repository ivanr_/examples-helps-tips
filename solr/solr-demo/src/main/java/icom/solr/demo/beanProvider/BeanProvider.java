package icom.solr.demo.beanProvider;

import icom.solr.demo.services.SolrRestService;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

public class BeanProvider {

    private SolrRestService solrRestService;

    public HttpSolrClient getSolrClient(String url) {
            return new HttpSolrClient.Builder(url).build();
    }

    public SolrQuery getSolrQuery() {
        return new SolrQuery();
    }

    public SolrRestService getSolrRestService() {
        if (solrRestService == null){
            solrRestService = new SolrRestService();
        }
        return solrRestService;
    }
}
