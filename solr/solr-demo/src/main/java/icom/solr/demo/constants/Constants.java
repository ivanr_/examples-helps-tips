package icom.solr.demo.constants;

public class Constants {

    public static final String COLLECTIONS_FIELD_NAME = "collections";

    public static final String FIELDS_NAME = "fields";
}
