package icom.solr.demo.helpers;

public class UrlHelper {
    public static final String TWO_FIELDS_URL_RQ ="http://localhost:8983/solr/techproducts/select?q=foundation&fl=id,name";

    public static final String COMBINED_SEARCH_URL = "http://localhost:8983/solr/techproducts/select?q=%2Belectronics%20%2Bmusic";

    public static final String COLLECTION_CREATE_URL = "http://localhost:8983/solr/admin/collections?action=CREATE&name=newCollection&numShards=2&replicationFactor=1";

    public static final String COLLECTION_REMOVE_URL = "http://localhost:8983/solr/admin/collections?action=DELETE&name=newCollection";

    public static final String COLLECTIONS_GET_LIST = "http://localhost:8983/solr/admin/collections?action=LIST&wt=json";

    public static final String FIELDS_SEARCH_URL = "http://localhost:8983/solr/gettingstarted/schema/fields";

    public static final String QUERY_FIELD_URL = "http://localhost:8983/solr/films";

    public static final String SOLR_CLIENT_URL = "http://localhost:8983/solr";
}
