package icom.solr.demo.services;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class SolrRestService {
    private final RestTemplate restTemplate;

    public SolrRestService() {
        this.restTemplate = new RestTemplate();
    }

    public String getForObject(String url) {
        return restTemplate.getForObject(url, String.class);
    }

    public ResponseEntity<String> exchangeGet(String url) {
        return restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()), String.class);
    }
}
