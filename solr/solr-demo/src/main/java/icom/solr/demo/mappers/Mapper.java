package icom.solr.demo.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Mapper {

    private static ObjectMapper mapper = new ObjectMapper();

    public static Map getMapFromJson(String data) throws JsonProcessingException {
        return mapper.readValue(data, Map.class);
    }

    public static List getCollectionsFromMapByName(Map map, String fieldName) {
        if (map.get(fieldName) instanceof List) {
            List collections = (List) map.get(fieldName);
            if (collections.size() > 0
                    && (collections.get(0) instanceof String || collections.get(0) instanceof LinkedHashMap)) {
                return collections;
            }
        }
        return Collections.emptyList();
    }

    public static List extractFieldsNamesFromList(List fields) {
        return (List) fields.stream()
                .filter(entry -> entry instanceof LinkedHashMap)
                .map(entry -> ((LinkedHashMap)entry).get("name"))
                .collect(Collectors.toList());
    }
}
