Run solr:
1) guide -> https://lucene.apache.org/solr/guide/8_6/solr-tutorial.html
2) cmd from solr root:
    - bin/solr.cmd start -e cloud
    - tune collection name as techproducts and run next to add to collection
    - java -jar -Dc=techproducts -Dauto example\exampledocs\post.jar example\exampledocs\*

Remove collection:
bin\solr delete -c techproducts

Create new collection:
solr create -c <yourCollection> -s 2 -rf 2
ISSUE:

Stop solr nodes:
solr stop -all
solr stop -p 8983

Status:
solr status

Restart solr (nodes are down!), run from bin folder:
solr start -c -p 8983 -s ../example/cloud/node1/solr

Upload all json docs from folder:
java -jar -Dc=films -Dauto example\exampledocs\post.jar example\films\*.json

CollectionAPI:
(https://lucene.apache.org/solr/guide/6_6/collections-api.html)
- http://localhost:8983/solr/admin/collections?action=CREATE&name=newCollection&numShards=2&replicationFactor=1
- http://localhost:8983/solr/admin/collections?action=DELETE&name=newCollection
- http://localhost:8983/solr/admin/collections?action=LIST&wt=json
- http://localhost:8983/solr/gettingstarted/schema/fields
- http://localhost:8983/solr/films/select?q=*:*&rows=0&facet=true&facet.field=genre_str (all values and occurrence of field "genre_str" as the whole phrase or as the single word inside "genre")
- http://localhost:8983/solr/films/select?=&q=*:*&facet.field=genre_str&facet.mincount=200&facet=on&rows=0 (field facet with min count)
