package com;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Date;

class PortScanner {
    private static Mailer mailer = new Mailer();

    private static String password;

    public static void main(String[] args) {
        password = args[0];
        boolean serverIsOn = true;
        while (true) {
            int port = 8080;
            try {
                URL url = new URL("http://localhost:8080/bo-app");
                new BufferedReader(new InputStreamReader(url.openStream()));
                System.out.println("Port " + port + " is open. " + new Date());
                if (!serverIsOn) {
                    sendMessageServerOn();
                    serverIsOn = true;
                }
            } catch (Exception ex) {
                System.out.println("Port " + port + " is not open. " + new Date());
                if (serverIsOn) {
                    sendMessageServerOff();
                    serverIsOn = false;
                }

            }
            new Waiter().run();
        }
    }

    private static void sendMessageServerOn() {
        sendMessage("Server was launched ");
    }

    private static void sendMessageServerOff() {
        sendMessage("Server was shutted down ");
    }

    private static void sendMessage(String message) {
        mailer.send(message, password);
    }
}

