Preconditions to run application by KafkaApplication class:
- kafka installed
- zookeeper server started by run from bin\windows: zookeeper-server-start.bat ..\..\config\zookeeper.properties
- kafka server started: kafka-server-start.bat ..\..\config\server.properties
- kafka consumer started with CURRENT TOPIC NAME: kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic i-topic --from-beginning
    where "i-topic" - topics name