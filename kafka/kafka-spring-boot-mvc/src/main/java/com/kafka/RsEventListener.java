package com.kafka;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.ServletRequestHandledEvent;

@Component
public class RsEventListener implements ApplicationListener {

    public void onApplicationEvent(ApplicationEvent event) {
        System.out.println("caught event: " + event.toString());
        if(event instanceof ServletRequestHandledEvent){
            System.out.println("request url: " + ((ServletRequestHandledEvent) event).getRequestUrl());
        }
    }

    @EventListener
    public void checkEvent(ApplicationEvent event){
        if(event instanceof ServletRequestHandledEvent){
            System.out.println("event caught through custom method" + event.toString());
        }
    }
}
