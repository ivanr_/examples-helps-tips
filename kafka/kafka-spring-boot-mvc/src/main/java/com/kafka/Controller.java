package com.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {
    private static final String TOPIC = "i-topic";

    @GetMapping("/publish/{message}")
    public String publishTextMessage(@PathVariable("message") String message){
        kafkaTemplate.send(TOPIC, message);
        return "published message: " + message;
    }

    @GetMapping("/")
    public String mainPageResponse(){
        return "ok";
    }

    @PostMapping("/publish")
    public String publishPerson(@RequestBody Person person){
        kafkaTemplate.send(TOPIC, person.toString());
        return "published person: " + person;
    }

    @PostMapping("/publishWithParam")
    public String publishPerson(@RequestParam(value = "name") String name, @RequestParam(value = "age") int age){
        Person person = new Person(name, age);
        kafkaTemplate.send(TOPIC, person.toString());
        return "published through parameters person: " + person;
    }

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;
}
