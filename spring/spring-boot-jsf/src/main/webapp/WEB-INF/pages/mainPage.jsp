<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Main application page</title>
</head>
<body>
<h3>Main page of application</h3>
<a href="${pageContext.request.contextPath}/page">1. Link to get returnPage</a><br>
<a href="${pageContext.request.contextPath}/body">2. Link to get body of page</a><br>
<a href="${pageContext.request.contextPath}/jsfPage">3. Link to get JSF example page</a><br>
<a href="${pageContext.request.contextPath}/root">4. Link to get root.xhtml</a><br>
</body>
</html>