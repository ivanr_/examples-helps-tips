<%@ page import="java.util.Calendar" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Page to return data from application</title>
</head>
<body>
<h3>Additional page of application</h3>
<a href="${pageContext.request.contextPath}/">Return</a>
<br>
<% if (Calendar.getInstance().get(Calendar.AM_PM) == Calendar.AM) {%>
Good Morning
<% } else { %>
Good Afternoon
<% } %>
<br>
<%
    int[] internalValues = {1, 4, 6, 1};
    int currentInt;
    for (int internalValue : internalValues) {
        currentInt = internalValue;
        pageContext.setAttribute("intValue", currentInt);
%>
Values of array from jsp page:
<c:out> "${intValue}" </c:out>
<br>
<%
    }
%>
<a href="${pageContext.request.contextPath}/">Return to main page</a>
</body>
</html>