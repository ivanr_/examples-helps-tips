package icom.springboot.jsf.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@ManagedBean
@SessionScoped
public class JsfAnnotationConfiguredBean {
    public String getMessage() {
        return new SimpleDateFormat("HH:mm:ss")
                .format(Calendar
                        .getInstance()
                        .getTime());
    }
}
