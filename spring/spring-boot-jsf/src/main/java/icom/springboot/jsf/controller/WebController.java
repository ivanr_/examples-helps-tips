package icom.springboot.jsf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WebController {

    @GetMapping("/")
    public String getMainPage() {
        System.out.println("main page access");
        return "/WEB-INF/pages/mainPage.jsp";
    }

    @GetMapping("/body")
    @ResponseBody
    public String getResponseBody() {
        System.out.println("body accessed");
        return "hello from root page of spring boot jsf";
    }

    @GetMapping("/page")
    public String getResponsePage() {
        System.out.println("page method accessed");
        return "/WEB-INF/pages/jspExamplePage.jsp";
    }

    @GetMapping("/jsfPage")
    public String getJsfPage() {
        System.out.println("jsf page accessed");
        return "WEB-INF/pages/jsfPageExample.xhtml";
    }

    @GetMapping("/root")
    public String getRootJsf(){
        System.out.println("root.xhtml accessed");
        return "WEB-INF/pages/root.xhtml";
    }
}
