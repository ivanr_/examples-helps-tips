package icom.springboot.jsf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJSF {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootJSF.class, args);
    }
}
