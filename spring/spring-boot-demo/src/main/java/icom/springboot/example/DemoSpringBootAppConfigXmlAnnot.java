package icom.springboot.example;

import icom.springboot.example.beans.HelloMessageProvider;
import icom.springboot.example.beans.HowdyMessageProvider;
import icom.springboot.example.beans.UniqueMessageProvider;
import icom.springboot.example.components.MessageProvider;
import icom.springboot.example.beans.UniqueClassBean;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
class DemoSpringBootAppConfigXmlAnnot {

//BEFORE launch -> create maven built with "mvn compile" to copy file conf.xml as resource to target/classes
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(DemoSpringBootAppConfigXmlAnnot.class);
        app.setBannerMode(Banner.Mode.OFF);

//        ----------------------------------------------------
//        XML configuration from classPath
        String path = "conf.xml";
        ApplicationContext classPathXmlAppContext = new ClassPathXmlApplicationContext(path);
        MessageProvider howdyMessageProvider = (HowdyMessageProvider) classPathXmlAppContext.getBean("howdyMessageProvider");
//        default message
        System.out.println(howdyMessageProvider.get());
//        set and output the new message
        howdyMessageProvider.setMessage("new howdy mes");
        System.out.println(howdyMessageProvider.get());
//        ----------------------------------------------------
//        AUTOCONFIGURATION
        AnnotationConfigApplicationContext annotationConfigAppContext = new AnnotationConfigApplicationContext("icom.springboot.example.beans");
        MessageProvider helloMessageProvider = annotationConfigAppContext.getBean(HelloMessageProvider.class);
//        default message
        System.out.println(helloMessageProvider.get());
//        using unique bean name
        UniqueMessageProvider uniqueMessageProvider = annotationConfigAppContext.getBean(UniqueMessageProvider.class);
//        MessageProvider uniqueMessageProvider = (UniqueMessageProvider) annotationConfigAppContext.getBean("Unique");
        System.out.println("Unique provider name: " + uniqueMessageProvider.getName());
//        UniqueMessage class provider


        UniqueMessageProvider uniqueFromBean = (UniqueMessageProvider) annotationConfigAppContext.getBean("getUniqueMessageClass");
        System.out.println(uniqueFromBean.getName());

//        ----------------------------------------------------
//        DIRECT bean registration to context
//        fake unique object from bean
        annotationConfigAppContext.register(UniqueClassBean.class);
        UniqueClassBean fakeUniqueMessageProvider = annotationConfigAppContext.getBean(UniqueClassBean.class);
        System.out.println(fakeUniqueMessageProvider.getFakeClass("changed text at name").getName());


//        ----------------------------------------------------
        System.out.println("Hello my World!");
    }
}
