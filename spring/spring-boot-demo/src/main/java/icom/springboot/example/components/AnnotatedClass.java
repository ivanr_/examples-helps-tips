package icom.springboot.example.components;

import org.springframework.stereotype.Repository;

@Repository
public class AnnotatedClass {
    private String  field = "Data from field of Annotated class ";

    public String getField() {
        return field;
    }
}
