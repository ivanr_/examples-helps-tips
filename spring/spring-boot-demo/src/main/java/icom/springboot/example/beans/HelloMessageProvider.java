package icom.springboot.example.beans;

import icom.springboot.example.components.MessageProvider;
import org.springframework.stereotype.Service;

@Service
public class HelloMessageProvider extends MessageProvider {

    public HelloMessageProvider() {
        super.setMessage("Hello message!!!");
    }

    @Override
    public String get() {
        System.out.println("example of HelloMessageProvider.class created");
        return super.get();
    }
}
