package icom.springboot.example;

import icom.springboot.example.beans.HiMessageProvider;
import icom.springboot.example.components.AnnotatedClass;
import icom.springboot.example.components.MessageProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

class DemoSpringBootAppConfigTwoPackages {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(DemoSpringBootAppConfigTwoPackages.class);
        ApplicationContext context = new AnnotationConfigApplicationContext("icom.springboot.example.components",
                "icom.springboot.example.beans");
        app.run();
        AnnotatedClass annotatedClass = context.getBean(AnnotatedClass.class);
        System.out.println("\nAnnotated class field value: " + annotatedClass.getField());
        MessageProvider hiMessageProvider = context.getBean(HiMessageProvider.class);
        System.out.println("Default message from hi! provider: " + hiMessageProvider.get());
    }
}
