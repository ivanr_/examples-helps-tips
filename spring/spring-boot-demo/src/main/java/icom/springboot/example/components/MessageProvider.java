package icom.springboot.example.components;

import java.util.function.Supplier;

public class MessageProvider implements Supplier {
    private String message = "Default message";

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String get() {
        return message;
    }
}
