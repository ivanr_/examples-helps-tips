package icom.springboot.example.beans;

import icom.springboot.example.components.MessageProvider;
import org.springframework.stereotype.Component;

@Component
public class HiMessageProvider extends MessageProvider {

    public HiMessageProvider() {
        super.setMessage("Hi to every");
    }

    @Override
    public String get() {
        System.out.println();
        return "Hi there!";
    }
}
