package icom.springboot.example.beans;

import icom.springboot.example.components.MessageProvider;

public class HowdyMessageProvider extends MessageProvider {

    public HowdyMessageProvider() {
        super.setMessage("Howdy, my friend!");
    }

    @Override
    public String get() {
        System.out.println("example of HowdyMessageProvider.class created");
        return super.get();
    }
}
