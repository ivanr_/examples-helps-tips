package icom.springboot.example.beans;

import icom.springboot.example.components.MessageProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service("unique")
public class UniqueMessageProvider extends MessageProvider {
    @Value("some default unique class field name from @Value annotations")
    private String name;

    @Bean("getName")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
