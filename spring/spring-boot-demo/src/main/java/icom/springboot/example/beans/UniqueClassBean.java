package icom.springboot.example.beans;

import icom.springboot.example.beans.UniqueMessageProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UniqueClassBean {
    private final UniqueMessageProvider notUnique;

    @Autowired
    public UniqueClassBean(@Qualifier("getFakeClass") UniqueMessageProvider notUnique) {
        this.notUnique = notUnique;
    }

    @Bean("getFakeClass")
    public UniqueMessageProvider getFakeClass(String text) {
        notUnique.setName(text + " some default data is Here");
        return notUnique;
    }
}
