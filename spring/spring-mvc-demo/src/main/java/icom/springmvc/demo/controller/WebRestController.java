package icom.springmvc.demo.controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class WebRestController {

    @GetMapping
    public String getRootInfo() {
        return "PATH OPTIONS:<br>" +
                "<label href=http://localhost:8080/rest/> /hello</label> <br>" +
                "<label> /modelMap</label> <br>";
    }

    @GetMapping("/hello")
    public String restHello() {
        return "HI from REST controller";
    }

    @GetMapping("/modelMap")
    public ModelMap makeModel(ModelMap modelMap) {
        modelMap.put("message", "some message from ModelMap");
        return modelMap;
    }
}
