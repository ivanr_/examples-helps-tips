package icom.springmvc.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/")
public class WebController {

    @GetMapping
    public String rootPage(){
        return "rootPage";
    }

    @GetMapping("/ret")
    public String returnPage() {
        System.out.println("return method accessed");
        return "simpleCodeJspPage";
    }

    @GetMapping({"/modelMap", "/modelMap/{path}"})
    public String returnModelMap(@PathVariable(required = false) String path,
                                 @RequestParam(required = false) String parameter,
                                 ModelMap modelMap){
        System.out.println("ModelMap path: " + path);
        System.out.println("ModelMap parameter: " + parameter);
        modelMap.addAttribute("message", "hello from MODELMAP!");
        modelMap.addAttribute("path", path);
        modelMap.addAttribute("parameter", parameter);
        return "messageReturnPage";
    }

    @GetMapping({"/modelAndView", "/modelAndView/{path}"})
    public ModelAndView returnModelAndView(@PathVariable(required = false) String path,
                                           @RequestParam(required = false) String parameter){
        ModelAndView modelAndView = new ModelAndView("messageReturnPage");
        System.out.println("ModelAndView path: " + path);
        System.out.println("ModelAndView parameter: " + parameter);
        modelAndView.addObject("message", "hello from ModelAndView");
        modelAndView.addObject("path", path);
        modelAndView.addObject("parameter", parameter);
        return modelAndView;
    }

    @GetMapping({"/page", "/page/{path}"})
    public String hello(@PathVariable(value = "path", required = false) String inputString,
                        @RequestParam (required = false) String parameter,
                        Model model) {
        System.out.println("hi from page request");
        Map<String, String> map = new HashMap<>();
        map.put("path", inputString);
        map.put("parameter", parameter);
        model.addAttribute("message", "hello from Model");
        model.mergeAttributes(map);
        return "messageReturnPage";
    }

    @GetMapping("/hello")
    @ResponseBody
    public String hello1(@RequestParam(value = "name", required = false) String paramName) {
        System.out.println("heeeeelo1 method from /hello path request");
        if (paramName == null) paramName = "incognito";
        return "hello <h1>" + paramName + "</h1> echo from /hello path request";
    }

    @GetMapping("/stop")
    @ResponseBody
    public String stopServer() {
        System.exit(1);
        return "";
    }
}
