<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Title of page</title>
</head>
<body>
<h1>Return Page</h1>
<b>JSP expression example</b>
<button onclick="window.location='/ret'">Push to reload page</button>
<table border="1px">
    <tr>
        <td>Today's date:</td>
        <td><%= Calendar.getInstance().getTime()%>
        </td>
    </tr>
    <tr>
        <td>Time in millis:</td>
        <td><%= new Date().getTime()%>
        </td>
    </tr>
    <tr>
        <td>Random number, max value=1:</td>
        <td><%= Math.random()%>
        </td>
    </tr>
    <tr>
        <td>Random number, max value=10:</td>
        <td><%= Math.random() * 10%>
        </td>
    </tr>
</table>
<b>JSP code fragment</b>
<table border="1px" >
    <tr>
        <td bgcolor="#ffe4c4">
            Declaration of current time presented as long:
            <%!
                private long currentTime = new Date().getTime();
            %>
        </td>
        <td bgcolor="#7fffd4">
            result:
            <%=currentTime%>
        </td>
    </tr>
    <tr>
        <%
            String output;
            long randomNumber = (long) (Math.random()*100);
            if (randomNumber % 2 == 1) {
                output = "odd";
            } else {
                output = "even";
            }
        %>
        <td colspan="2" align="center">
            Value of random number <%=randomNumber%> is <b><%=output%></></td>
    </tr>
</table>
</body>
</html>
