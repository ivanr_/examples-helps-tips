<html>
<head>
    <title>Title of ROOT page</title>
</head>
<body>

<table border="1px">
    <tr>
        <td align="center"><b>Links to get pages from WebController</b><br></td>
    </tr>
    <tr>
        <td><a href="/ret">1. Link to JSP code example page</a></td>
    </tr>
    <tr>
        <td><a onclick=goToModels("/page/")>2. Click here to go to JSP page with <b>Model</b> view to return with</a></td>
    </tr>
    <tr>
        <td><a onclick=goToModels("/modelMap/")>3. Click here to go to JSP page with <b>ModelMap</b> view and included</a><br>
        </td>
    </tr>
    <tr>
        <td>
            <a onclick=goToModels("/modelAndView/")>4. Click here to go to JSP page with <b>ModelAndView</b> view and included</a><br>
        </td>
    </tr>
    <tr>
        <td>Path<input id="path" value="defaultPath">
            Parameter<input id="parameter" value="defaultParameter"></td>
    </tr>
</table>
</body>
</html>

<script>
    function goToModels(input) {
        var path = document.getElementById("path").value;
        var parameter = document.getElementById("parameter").value;
        window.location=input + path + "?parameter=" + parameter;
    }
</script>