package icom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@SpringBootApplication
@RestController
public class ApplicationController {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationController.class, args);
    }

    @RequestMapping("/")
    public String rootResponse(){
        Date currentDate = new Date();
        System.out.println("root method accessed as " + currentDate);
        return "Hello Dockerized World! " +
                "Current system time is: " + currentDate.toString();
    }

    @GetMapping("/stop")
    public String stop(){
        System.out.println("app stopped through endpoint");
        System.exit(0);
        return "unreachable line";
    }
}
