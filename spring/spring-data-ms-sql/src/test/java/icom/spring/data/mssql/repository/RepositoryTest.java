package icom.spring.data.mssql.repository;


import icom.spring.data.mssql.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager entityManager;

    public void addUsersToRepository(){
        userRepository.save(new User("Mike", "Sunny"));
        userRepository.save(new User("Andrew", "Cannon"));
        userRepository.save(new User("Jane", "Mattis"));
    }

    @Test
    public void testRepository(){
        addUsersToRepository();
        Iterable<User> all = userRepository.findAll();
        System.out.println("\nUsers  from db");
        all.forEach(System.out::println);
    }

    @Test
    public void checkUsers(){
        List<User> users = userRepository.findByName("Jane");
        System.out.println("\nFound user by name\n");
        users.forEach(user -> System.out.println(user + "\n"));
    }

    @Test
    public void findUserById(){
        User firstUserFromDb = userRepository.findAll()
                .iterator().next();
        Assert.notNull(firstUserFromDb, "user cannot be null");
        System.out.println("Found first user by id:\n" + userRepository.findById(firstUserFromDb.getId()));
    }

    @Test
    public void customEntityManagerQuery(){
        List<User> resultList = entityManager
                .createQuery("select u from User u where u.name like '%ane'", User.class)
                .getResultList();
        resultList.forEach(System.out::println);
    }

    @Test
    public void saveOrUpdateUser(){
        User userToSave = new User("Manny", "Penny");
        User userFromRepo = userRepository.findByNameAndLastName(userToSave.getName(), userToSave.getLastName());
        if (null == userFromRepo){
            userRepository.save(userToSave);
        }
        userFromRepo = userRepository.findByNameAndLastName(userToSave.getName(), userToSave.getLastName());
        System.out.println(userFromRepo);
    }
}
