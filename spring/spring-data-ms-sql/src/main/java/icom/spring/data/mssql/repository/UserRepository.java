package icom.spring.data.mssql.repository;


import icom.spring.data.mssql.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {

    User findById(int id);

    @Query(name = "select u from User u where u.name = :userName")
    List<User> findByName(@Param("userName") String name);

    User findByNameAndLastName(String name, String lastName);
}
