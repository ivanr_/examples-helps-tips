This project contains:
- personal examples for Java code shared for everyone so I would be appreciated for any proposals and additions
- helps page for solving problems from installing and configuration of software to coding steps
- some tips to improve code constructing

JDK version 1.8.*

Projects contain next:
- JavaCorePlus:
    CORE
    --Csv2Xml
    --CsvSaver
    --Enums
    --Exceptions
    --FileIO (java.io)
    --Generics
    --MyLinkedList/MyMap
    --Iterator
    --String
    PLUS
    --Annotations
    --GCP
    --Lambda
    --Logging(java.util.logging | log4j | slf4j+log4j)
    --Networking(socket | tcp)
    --Patterns(adapter | builder | decorator | mvvc | observer | prototype | nullObject | singleton | template)
    --Reflections
    --Serialization
    --SortAlgorithms(binary | bubble | insertion | merge | selection | MySortAlgorithm)
    --StreamApi(simpleFunctions | optional | math | manySources)
    --Testing(junit | mockito)
    --Threads(concurrent | atomic | countdownLatch | cyclicBarrier | exchanger | fork | semaphore | waiter-notifier)
- SPRING:
    --SpringBootDemo(xml-annotation context) LAUNCH: mainClass
    --SpringMvcDemo(controllers | apacheTomcat | JSPs | Model-ModelMap-ModelAndView | xmlConfigurations) LAUNCH: mvn:install from ide
    --SpringBootJsf(JSF | controllers | WebConfig/WebAppInitializer)
Task to done (CORE):
- multiply flexible inheritance probe (core)
- simple class loading

Task to do:
- error interception
- JMX example
