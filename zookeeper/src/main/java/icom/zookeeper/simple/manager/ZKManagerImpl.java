package icom.zookeeper.simple.manager;

import icom.zookeeper.simple.connection.ZKConnection;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class ZKManagerImpl implements ZKManager {
    private static final String NODE_DO_NOT_EXISTS = "current node do not exists";
    private static ZooKeeper zkeeper;
    private static ZKConnection zkConnection;

    public ZKManagerImpl() throws IOException, InterruptedException {
        initialize();
    }

    private void initialize() throws IOException, InterruptedException {
        zkConnection = new ZKConnection();
        zkeeper = zkConnection.connect("localhost");
    }

    @Override
    public void closeConnection() throws InterruptedException {
        zkConnection.close();
    }

    @Override
    public void create(String path, byte[] data)
            throws KeeperException,
            InterruptedException {

        zkeeper.create(
                path,
                data,
                ZooDefs.Ids.OPEN_ACL_UNSAFE,
                CreateMode.PERSISTENT);
    }

    @Override
    public Object getZNodeData(String path, boolean watchFlag)
            throws KeeperException,
            InterruptedException {

        byte[] b;
        b = zkeeper.getData(path, null, null);
        return new String(b, StandardCharsets.UTF_8);
    }

    @Override
    public void update(String path, byte[] data) throws KeeperException,
            InterruptedException {
        int version = zkeeper.exists(path, true).getVersion();
        System.out.println("Updated data version: " + version);
        zkeeper.setData(path, data, version);
    }

    @Override
    public List<String> getChildren(String nodeName) throws KeeperException, InterruptedException {
        return zkeeper.getChildren("/" + nodeName, true);
    }

    @Override
    public String addChildren(String nodePath, String childName) throws KeeperException, InterruptedException {
        boolean exist = null != zkeeper.exists(nodePath, true);
        if (!exist) {
            return NODE_DO_NOT_EXISTS;
        }
        String childPath = nodePath + "/" + childName;
        try {
            this.create(childPath, "init child data".getBytes());
        } catch (KeeperException e) {
            return "Sub-node already exists";
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (null != zkeeper.exists(childPath, true)) {
            return "true";
        }
        return "false";
    }
}
