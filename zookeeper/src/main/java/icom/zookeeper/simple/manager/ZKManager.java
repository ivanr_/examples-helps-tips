package icom.zookeeper.simple.manager;

import org.apache.zookeeper.KeeperException;

import java.io.UnsupportedEncodingException;
import java.util.List;

public interface ZKManager {
    void closeConnection() throws InterruptedException;

    void create(String path, byte[] data)
            throws KeeperException, InterruptedException;
    Object getZNodeData(String path, boolean watchFlag) throws KeeperException, InterruptedException, UnsupportedEncodingException;
    void update(String path, byte[] data)
            throws KeeperException, InterruptedException;

    List<String> getChildren(String nodeName) throws KeeperException, InterruptedException;

    String addChildren(String path, String childrenName) throws KeeperException, InterruptedException;
}
