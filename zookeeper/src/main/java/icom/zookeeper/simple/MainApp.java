package icom.zookeeper.simple;

import icom.zookeeper.simple.manager.ZKManager;
import icom.zookeeper.simple.manager.ZKManagerImpl;
import org.apache.zookeeper.KeeperException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class MainApp {

    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {
        ZKManager zkManager = new ZKManagerImpl();
        String nodeName = "newNode";
        byte[] initData = "some data".getBytes();
        byte[] newData = "updated node data".getBytes();
        byte[] secondUpdatedData = "lately updated node data".getBytes();
        String childrenName = "sub node as a child";

        createNodeAndPrintResult(zkManager, nodeName, initData);

        printDataFromNodeByNodeName(zkManager, nodeName);

        updateNodeData(zkManager, nodeName, newData);
        printDataFromNodeByNodeName(zkManager, nodeName);

        updateNodeData(zkManager, nodeName, secondUpdatedData);
        printDataFromNodeByNodeName(zkManager, nodeName);

        printChildren(zkManager, nodeName);

        addChild(zkManager, nodeName, childrenName);

        zkManager.closeConnection();
    }

    private static void addChild(ZKManager zkManager, String nodeName, String childrenName) throws KeeperException, InterruptedException {
        System.out.println("Adding child result: " + zkManager.addChildren("/" + nodeName, childrenName));
    }

    private static void printChildren(ZKManager zkManager, String nodeName) throws KeeperException, InterruptedException {
        System.out.println(nodeName + " contains children: " + zkManager.getChildren(nodeName));
    }

    private static void updateNodeData(ZKManager zkManager, String nodeName, byte[] newData) throws KeeperException, InterruptedException {
        zkManager.update("/" + nodeName, newData);
    }

    private static void createNodeAndPrintResult(ZKManager zkManager, final String newNodeName, byte[] data) {
        try {
            zkManager.create("/" + newNodeName, data);
            System.out.println("Node created successfully, name: " + newNodeName);
        } catch (KeeperException e) {
            System.out.println("Node creation result: already exists, name: " + newNodeName);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void printDataFromNodeByNodeName(ZKManager zkManager, String nodeName) throws InterruptedException, UnsupportedEncodingException, KeeperException {
        Object zNodeData = zkManager.getZNodeData("/" + nodeName, true);
        System.out.println("Node data goes next: " + zNodeData);
    }
}
