To run zookeeper,execute from <zookeeper_home>/bin:
zkServer

When run with config it can be issue:
NumberFormatException.forInputString for config file

Connect to client:
zkCli.sh -server 127.0.0.1:2181


RESOLVED:
- install kafka (zookeeper in inside)
- goto bin\windows and launch zookeeper server:
    zookeeper-server-start.bat ..\..\config\zookeeper.properties
- run zookeeper shell as client connection (default port is 2181):
    zookeeper-shell.bat localhost