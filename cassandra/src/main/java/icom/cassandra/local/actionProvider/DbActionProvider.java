package icom.cassandra.local.actionProvider;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.Row;
import com.datastax.oss.driver.api.core.cql.SimpleStatement;

import java.util.List;

public class DbActionProvider {

    private final CqlSession cqlSession;

    public DbActionProvider(CqlSession cqlSession) {
        this.cqlSession = cqlSession;
    }

    public List<Row> executeQueryAndGetAll(String cqlQuery){
        return cqlSession.execute(
                SimpleStatement.builder(cqlQuery).build()).all();
    }

    public void executeQuery(String cqlQuery){
        cqlSession.execute(
                SimpleStatement.builder(cqlQuery).build());
    }

    public void closeSession() {
        cqlSession.close();
    }
}
