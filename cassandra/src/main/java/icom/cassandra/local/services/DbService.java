package icom.cassandra.local.services;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.Row;
import icom.cassandra.local.actionProvider.DbActionProvider;
import icom.cassandra.local.helper.CqlHelper;
import icom.cassandra.local.pojo.Person;
import icom.cassandra.local.resultConverter.PersonConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;

public class DbService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DbService.class);

    private DbActionProvider dbActionProvider;

    private String dbName;

    public DbService(String dbName) {
        this.dbName = dbName;
        initDbActionProvider();
    }

    private void initDbActionProvider() {
        this.dbActionProvider = new DbActionProvider(CqlSession.builder().withKeyspace(dbName).build());
    }


    public List<Person> getPersons() {
        List<Row> allFromTable = dbActionProvider.executeQueryAndGetAll(CqlHelper.QUERY_ALL_PERSONS);

        return allFromTable.stream()
                .map(row -> new PersonConverter(row).get())
                .collect(Collectors.toList());
    }

    public void insertPersonByIdName(String id, String name) {
        dbActionProvider.executeQuery(CqlHelper.INSERT_ID_NAME_INTO_PERSON_QUERY
                .replace("<id>", id)
                .replace("<name>", name));
    }

    public void closeConnection() {
        dbActionProvider.closeSession();
    }

    public void outputPersonsToConsole() {
        LOGGER.warn("all persons requested");
        getPersons().forEach(System.out::println);
    }

    public void removePersonById(String id) {
        dbActionProvider.executeQuery(CqlHelper.REMOVE_PERSON_BY_ID
                .replace("<id>", id));
    }
}
