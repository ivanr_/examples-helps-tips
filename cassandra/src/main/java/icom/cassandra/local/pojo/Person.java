package icom.cassandra.local.pojo;

import java.util.Arrays;
import java.util.List;

public class Person {

    private final long id;

    private final String name;
    private final int age;
    private final List<String> middle_names;

    public Person(long id, String name, int age, List<String> middle_names) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.middle_names = middle_names;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getAge() {
        return age;
    }

    public List<String> getMiddle_names() {
        return middle_names;
    }

    public List<String> getActualFieldsNamesAsList(){
        return Arrays.asList("name", "age", "middle_names");
    }

    @Override
    public String toString() {
        return "Person{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", middle_names=" + middle_names +
                '}';
    }
}