package icom.cassandra.local;

import icom.cassandra.local.pojo.Person;
import icom.cassandra.local.services.DbService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class LocalApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocalApplication.class);

    public static void main(String[] args) {

        LOGGER.warn("cassandra app started");
        DbService personService = new DbService("app_data");

        List<Person> allFromTable = personService.getPersons();
        allFromTable.forEach(System.out::println);

        personService.insertPersonByIdName("1005", "Custom Name");

        personService.outputPersonsToConsole();

        personService.removePersonById("1004");

        personService.outputPersonsToConsole();

        personService.closeConnection();
    }
}
