package icom.cassandra.local.helper;

public class CqlHelper {

    public static final String QUERY_ALL_PERSONS = "select * from app_data.person";

    public static final String INSERT_ID_NAME_INTO_PERSON_QUERY = "insert into app_data.person (id, name) values (<id>, '<name>')";

    public static final String REMOVE_PERSON_BY_ID = "delete from app_data.person where id = <id>;";
}
