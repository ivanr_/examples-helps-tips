package icom.cassandra.local.resultConverter;

import com.datastax.oss.driver.api.core.cql.Row;
import icom.cassandra.local.pojo.Person;
import org.springframework.util.Assert;

import java.util.function.Supplier;

public class PersonConverter implements Supplier<Person> {

    private Person person;

    private Row queryResult;

    public PersonConverter(Person person) {
        this.person = person;
    }

    public PersonConverter(Row results) {
        this.queryResult = results;
    }

    @Override
    public Person get() {
        if (person != null) {
            return person;
        } else {
            Assert.notNull(queryResult, "query result must be not null");
            return person = new Person(
                    queryResult.getLong("id"),
                    queryResult.getString("name"),
                    queryResult.getShort("age"),
                    queryResult.getList("middle_names", String.class)
            );
        }
    }
}
