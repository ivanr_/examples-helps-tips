package icom.cassandra.springJpa;

import com.datastax.oss.driver.api.core.CqlSession;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    /*
     * Use the standard Cassandra driver API to create a com.datastax.oss.driver.api.core.CqlSession instance.
     */
    public @Bean
    CqlSession session() {
        return CqlSession.builder()
                .withLocalDatacenter("datacenter1")
//                .withLocalDatacenter("datacenter2") //TODO tune config to run app properly, current issue- custom conversion class not found
                .withKeyspace("App_data").build();
    }
}
