package icom.cassandra.springJpa.restController;

import icom.cassandra.local.pojo.Person;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/person")
public class PersonController {

    @GetMapping("/")
    public void getAll() {
        System.out.println("get all");
    }

    @GetMapping("/{id}")
    public void getById(@PathVariable String id) {
        System.out.println("get by this id: " + id);
    }

    @PostMapping("/")
    public void postPerson(@RequestBody Person person) {
        System.out.println("posted: " + person);
    }
}
