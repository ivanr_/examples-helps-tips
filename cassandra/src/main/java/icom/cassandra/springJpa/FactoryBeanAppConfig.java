package icom.cassandra.springJpa;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.CqlSessionFactoryBean;

//@Configuration
public class FactoryBeanAppConfig {

    /*
     * Factory bean that creates the com.datastax.oss.driver.api.core.CqlSession instance
     */
    @Bean("cassandraSession")
    public CqlSessionFactoryBean session() {

        CqlSessionFactoryBean session = new CqlSessionFactoryBean();
        session.setContactPoints("localhost");
        session.setKeyspaceName("app_data");

        return session;
    }
}
