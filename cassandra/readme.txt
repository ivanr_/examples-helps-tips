Run cassandra, launch from <cassandra>/bin:
- cassandra.bat -f
- cqlsh 127.0.0.1 -u cassandra -p cassandra

Cassandra status, launch from <cassandra>/bin:
- nodetool status

Db list:
cqlsh> describe keyspaces;


cqlsh> describe App_data;